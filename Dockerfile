FROM node:8.9-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run build
RUN npm install http-server -g
CMD http-server ./dist -p $PORT