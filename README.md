# Procedural Worldbuilding

This project is a complex earth-like simulation set loosely in the early renaissance period with fantasy elements mixed in. The end goal is to create interesting procedural settings|plots|novels that adapt and evolve by the rules of the overarching simulated world to be used as inspiration for authors and game designers. My intent is to make this simulation highly intractable to the point where one could also play it as a game if they wished. Current systems being simulated:
1. Geography with procedural continents and islands built using primarily simplex noise.
2. Earth-like climates based on [köppen climates](https://en.wikipedia.org/wiki/K%C3%B6ppen_climate_classification) used to procedurally generate daily weather conditions. 
3. Procedural ecology complete with procedural fauna (and soon flora) and predator/prey relations.
4. History simulation including wars, rebellions, successions, and plagues across hundreds of procedural nations. Each nation belongs to a culture group. Each culture group has a unique procedural language used to name various elements within its sphere of influence.
5. Procedural cities complete with urban maps, sample populations, and local economies which are affected by the aforementioned history simulation.
6. Procedural actors with detailed descriptions, bios, and ancestry relations.

Demo: https://rayoung788.gitlab.io/project-muse/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Find Circular Dependencies
read more here: https://github.com/pahen/madge
```
npm run deps
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Inspirations
1. http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/
2. https://azgaar.wordpress.com/
3. https://mewo2.com/notes/naming-language/
4. https://heredragonsabound.blogspot.com/
5. https://forhinhexes.blogspot.com/2019/04/history-vi-it-is-well-that-war-is-so.html

### Icons:
1. https://kmalexander.com/free-stuff/fantasy-map-brushes/
2. https://www.deviantart.com/starraven/art/Sketchy-Cartography-Brushes-198264358
3. https://www.deviantart.com/eragon2589/art/Cartography-brushes-498111706
4. https://www.deviantart.com/winstonthebutterfly/art/Oblivion-and-Skyrim-Map-Marker-Brush-Set-387861762
5. https://www.pinterest.com/pin/380343131015862327/
6. https://www.flaticon.com/packs/tree-icons
