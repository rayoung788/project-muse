import { css } from '@emotion/css'
import { css_colors } from './colors'
import { fonts } from './fonts'

export const style__underline = css`
  border-bottom: 1px solid black;
`

export const style__clickable = (color?: string) => css`
  cursor: pointer;
  border-bottom: 1px dotted ${color ?? 'black'};
`

export const style__main_content = css`
  border: 2px solid ${css_colors.primary};
  background-color: ${css_colors.background.cards} !important;
`

export const style__subtitle = css`
  font-family: ${fonts.content};
  font-weight: 400;
  font-size: 0.775rem;
  color: ${css_colors.subtitle} !important;
`
