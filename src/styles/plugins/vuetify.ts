import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import { css_colors } from '../colors'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'mdi'
  },
  theme: {
    themes: {
      light: {
        primary: css_colors.primary,
        secondary: '#424242',
        accent: '#18FFFF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
      }
    }
  }
})
