import { css } from '@emotion/css'
import { css_colors } from './colors'
import { fonts } from './fonts'

export const style__legend_title = css`
  font-family: ${fonts.titles};
  font-weight: 800;
  font-size: 1.5rem;
`

export const style__legend_expansion = css`
  z-index: 2;
  position: absolute;
  width: 300px !important;
`

export const style__legend_panel = css`
  background-color: ${css_colors.background.legend} !important;
  border-color: black;
  border-style: double;
`
