import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

import MapViews from './maps'
import CharGen from './CharGen'
import { TransparentTabs } from '../common/tabs/transparent'

@Component({
  components: {
    MapViews,
    CharGen
  }
})
export default class WorldViews extends tsx.Component<Record<string, unknown>> {
  private active = 'Maps'
  public render() {
    const char_gen = !view_module.avatar
    return (
      <TransparentTabs
        onTabChange={active => (this.active = active)}
        tabs={{
          Maps: {
            element: <MapViews active={this.active === 'Maps'}></MapViews>
          },
          ...(char_gen
            ? {
                Start: {
                  element: <CharGen></CharGen>
                }
              }
            : {})
        }}></TransparentTabs>
    )
  }
}
