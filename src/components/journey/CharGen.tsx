import { BaseButton } from '@/components/common/vuetify/button'
import { Card, CardSubtitle, CardText, CardTitle } from '@/components/common/vuetify/cards'
import { Icon } from '@/components/common/vuetify/icon'
import { AutoComplete, Select, TextField } from '@/components/common/vuetify/input'
import { Column, Container, Row } from '@/components/common/vuetify/layout'
import { actor__spawn } from '@/models/npcs/actors/spawn'
import { Cousin } from '@/models/npcs/actors/spawn/relations/cousins'
import { Sibling } from '@/models/npcs/actors/spawn/relations/sibling'
import { Relation } from '@/models/npcs/actors/spawn/types'
import { genders, npc__opposite_gender } from '@/models/npcs/actors/stats/appearance/gender'
import { Actor } from '@/models/npcs/actors/types'
import { lang__first, lang__last } from '@/models/npcs/species/humanoids/languages/words/actors'
import { region__imperial_name } from '@/models/regions/diplomacy/status'
import { location__demographics } from '@/models/regions/locations/actors/demographics'
import { province__hub } from '@/models/regions/provinces'
import { range } from '@/models/utilities/math'
import { view_module } from '@/store/view'
import { style__subtitle } from '@/styles'
import { Component, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import CodexPage, { style__codex_title } from '../descriptive/codex'
import PrimaryButton from '../common/PrimaryButton'

@Component({
  components: {
    CodexPage
  }
})
export default class CharGen extends tsx.Component<Record<string, unknown>> {
  private first = ''
  private last = ''
  private gender: genders = 'male'
  private genders: genders[] = ['male', 'female']
  private province_idx = 0
  private culture = 0
  private get demographics() {
    const hub = province__hub(window.world.provinces[this.province_idx])
    const { common_cultures } = location__demographics(hub)
    return Object.entries(common_cultures)
      .sort((a, b) => {
        return b[1] - a[1]
      })
      .filter(([_, percent]) => percent > 0.01)
      .map(([culture, percent]) => {
        const { name } = window.world.cultures[parseInt(culture)]
        return {
          name: `${name}: ${(percent * 100).toFixed(2)}%`,
          idx: parseInt(culture)
        }
      })
  }
  private get curr_loc() {
    return view_module.codex.location
  }
  public mounted() {
    this.translate()
  }
  private begin() {
    const culture = window.world.cultures[this.culture]
    const starting_level = 1
    const ages = [20, 40]
    const avatar = actor__spawn({
      occupation: { key: 'mercenary' },
      location: province__hub(window.world.provinces[this.province_idx]),
      culture,
      gender: this.gender,
      first: this.first,
      last: this.last,
      living: true,
      unbound: true,
      venerable: true,
      level: starting_level,
      ages
    })
    const party: Actor[] = [avatar]
    range(4).forEach(() =>
      party.push(
        actor__spawn({
          location: window.world.locations[avatar.location.residence],
          occupation: { key: 'mercenary' },
          living: true,
          unbound: true,
          venerable: true,
          level: starting_level,
          relation: window.dice.weighted_choice<() => Relation>([
            {
              v: () => {
                const sibling = window.dice.choice(party)
                console.log(`spawning sibling of ${sibling.name}`)
                return new Sibling({ ref: sibling, location_locked: true })
              },
              w: 0.05
            },
            {
              v: () => {
                const cousin = window.dice.choice(party)
                console.log(`spawning cousin of ${cousin.name}`)
                return new Cousin({ ref: cousin, location_locked: true })
              },
              w: 0.05
            },
            { v: () => undefined, w: 0.9 }
          ])(),
          ages
        })
      )
    )
    const relations = party.map(({ idx }) => ({ actor: idx, type: 'party' as const }))
    relations.forEach(({ actor }) => {
      const member = window.world.actors[actor]
      member.relations = [...member.relations, ...relations]
    })
    view_module.update_avatar(avatar)
    this.cycle_gender()
  }
  @Watch('gender')
  private random_name() {
    const { language } = window.world.cultures[this.culture]
    this.first = lang__first(language, this.gender)
    this.last = lang__last(language, window.dice.flip)
  }
  @Watch('culture')
  public cycle_gender() {
    this.gender = npc__opposite_gender(this.gender)
  }
  @Watch('curr_loc')
  private translate() {
    const province = window.world.provinces[this.curr_loc.province]
    this.province_idx = province.idx
    this.culture = this.demographics[0].idx
  }
  public get general() {
    const culture = window.world.cultures[this.culture]
    return (
      <Container fluid>
        <Row wrap>
          <Column cols={5}>
            <TextField spellcheck={false} v-model={this.first} label='First:' />
          </Column>
          <Column cols={5}>
            {culture?.language?.surnames?.patronymic ? (
              <TextField disabled label='Patronymic Surname:' />
            ) : (
              <TextField spellcheck={false} v-model={this.last} label='Last:' />
            )}
          </Column>
          <Column cols={5}>
            <Select items={this.genders} v-model={this.gender} label='Gender:' />
          </Column>

          {this.province_idx >= 0 && origin && (
            <Column cols={5}>
              <AutoComplete
                spellcheck={false}
                items={this.demographics}
                item-text='name'
                item-value='idx'
                v-model={this.culture}
                persistent-hint
                hint={`${culture?.species}`}
                label='Culture:'
              />
            </Column>
          )}
          <Column cols={12}>
            <PrimaryButton click={this.begin} text='begin journey' class='mr-2' />
            <BaseButton icon large class='primary--text' onClick={this.random_name}>
              <Icon>mdi-refresh</Icon>
            </BaseButton>
          </Column>
        </Row>
      </Container>
    )
  }
  public render() {
    const province = window.world.provinces[this.curr_loc.province]
    const city = province__hub(province)
    const curr_nation = window.world.regions[province.curr_nation]
    return (
      <Card flat class='transparent ma-3'>
        <CardTitle class='mb-2'>
          <h3 class={style__codex_title}>Character Creation</h3>
        </CardTitle>
        <CardSubtitle>
          <h4 class={style__subtitle}>
            <i>{`Location: ${city.name} (${city.type}), ${region__imperial_name(curr_nation)}`}</i>
          </h4>
        </CardSubtitle>
        <CardText>{this.general}</CardText>
      </Card>
    )
  }
}
