import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

import { RadioButton, RadioGroup } from '@/components/common/vuetify/input'
import { Column, Row } from '@/components/common/vuetify/layout'

import UrbanMap from './urban'
import WorldMap from './world'

const maps = ['world', 'urban'] as const

@Component({
  components: {
    UrbanMap,
    WorldMap
  }
})
export default class MapViews extends tsx.Component<{ active: boolean }> {
  @Prop() active: boolean
  public map: typeof maps[number] = 'world'
  public map_toggle() {
    return (
      <RadioGroup v-model={this.map}>
        <Row>
          {maps.map((map, i) => (
            <Column cols={5} class='py-0'>
              <RadioButton
                color='black'
                key={i}
                label={map === 'urban' ? 'Settlement' : 'World'}
                value={map}
              />
            </Column>
          ))}
        </Row>
      </RadioGroup>
    )
  }
  public render() {
    const toggle = this.map_toggle()
    return (
      <div>
        <WorldMap
          v-show={this.map === 'world'}
          active={this.active && this.map === 'world'}
          toggle={toggle}
        />
        <UrbanMap
          v-show={this.map === 'urban'}
          active={this.active && this.map === 'urban'}
          toggle={toggle}
        />
      </div>
    )
  }
}
