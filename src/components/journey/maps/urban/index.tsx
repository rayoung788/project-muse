import { Column, Row } from '@/components/common/vuetify/layout'
import { districts__cutoff } from '@/models/regions/locations/spawn/blueprints/districts'
import { settlement__dimensions } from '@/models/regions/locations/spawn/blueprints/districts/types'
import { settlement__blueprint } from '@/models/regions/locations/spawn/blueprints'
import { average } from '@/models/utilities/math'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'
import { fonts } from '@/styles/fonts'
import * as d3 from 'd3'
import { Component, Prop, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import UrbanLegend from './legend'
import { location__get_closest_settlement } from '@/models/regions/locations'

const dimensions = settlement__dimensions

const blueprint_id = 'city_blueprint'
const container_id = 'blueprint_container'

const font_family = fonts.maps

@Component({
  components: {
    UrbanLegend
  }
})
export default class UrbanMap extends tsx.Component<{ active: boolean; toggle: JSX.Element }> {
  @Prop() active: boolean
  @Prop() toggle: JSX.Element
  private x = 0
  private y = 0
  private zoom: any
  private transforms = {
    dx: 0,
    dy: 0,
    scale: 1
  }
  private resize() {
    const container = document.getElementById(container_id)
    const canvas = document.getElementById(blueprint_id) as HTMLCanvasElement
    canvas.width = container.clientWidth
    canvas.height = dimensions.h
  }
  public paint_canvas() {
    const canvas = document.getElementById(blueprint_id) as HTMLCanvasElement
    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
    const { blocks } = settlement__blueprint(this.city)
    blocks.forEach(({ path, land }) => {
      const p = new Path2D(path)
      ctx.strokeStyle = !land ? 'rgba(4, 47, 83, 0.04)' : 'rgba(136, 136, 136, 0.1)'
      ctx.fillStyle = !land ? '#95b5b84c' : css_colors.background.cards
      ctx.fill(p)
      ctx.stroke(p)
    })
    const districts = blocks.filter(({ district }) => district)
    // draw buildings
    ctx.strokeStyle = 'white'
    ctx.textAlign = 'center'
    ctx.fillStyle = '#caccbb'
    const average_area = average(blocks.map(({ area }) => area))
    const radius = average_area ** 0.5 / 1.5
    ctx.lineWidth = radius * 0.2
    districts.forEach(({ structures }) => {
      structures.forEach(({ path }) => {
        const p = new Path2D(path)
        ctx.stroke(p)
        ctx.fill(p)
      })
    })
    // draw districts
    const show_districts = districts__cutoff(districts.length) > this.transforms.scale
    ctx.lineWidth = radius * 0.3
    districts.forEach(({ district: { path } }) => {
      const p = new Path2D(path)
      ctx.stroke(p)
    })
    districts.forEach(({ district: { idx }, center: [x, y] }) => {
      ctx.lineWidth = radius * 0.3
      ctx.fillStyle = 'black'
      if (show_districts) {
        ctx.beginPath()
        ctx.arc(x, y, radius, 0, 2 * Math.PI)
        ctx.fill()
        ctx.stroke()
        ctx.fillStyle = 'white'
        ctx.font = `${radius * 1.25}px ${font_family}`
        ctx.fillText(idx.toString(), x, y + radius * 0.3)
      } else {
        ctx.lineWidth = radius * 0.08
        this.city.map.districts[idx].buildings.forEach(({ x, y, idx }) => {
          ctx.fillStyle = 'black'
          ctx.beginPath()
          ctx.arc(x, y, radius * 0.3, 0, 2 * Math.PI)
          ctx.fill()
          ctx.stroke()
          ctx.fillStyle = 'white'
          ctx.font = `${radius * 0.3}px ${font_family}`
          ctx.fillText(idx.toString(), x, y + radius * 0.05)
        })
      }
    })
  }
  private pan_zoom() {
    const canvas = document.getElementById(blueprint_id) as HTMLCanvasElement
    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
    this.zoom = d3
      .zoom()
      .scaleExtent([1, 200])
      .translateExtent([
        [0, 0],
        [dimensions.h, dimensions.w]
      ])
      .on('zoom', () => this.transformer(d3.event.transform))
    const init = d3.select(ctx.canvas)
    init.on('touchmove mousemove', this.mouseover)
    init.call(this.zoom)
  }
  private mouseover() {
    const [x, y] = d3.mouse(document.querySelector(`#${blueprint_id}`))
    const tx = (x - this.transforms.dx) / this.transforms.scale
    const ty = (y - this.transforms.dy) / this.transforms.scale
    if (tx < dimensions.w - 1) this.x = Math.round(tx)
    if (ty < dimensions.h - 1) this.y = Math.round(ty)
  }
  private transformer(transform = { x: 0, y: 0, k: 1 }) {
    this.transforms = { dx: transform.x, dy: transform.y, scale: transform.k }
    this.draw_map()
  }
  public mounted() {
    this.resize()
    this.pan_zoom()
    this.recenter()
  }
  public get city() {
    return location__get_closest_settlement(view_module.codex.location)
  }
  public render() {
    return (
      <Row wrap>
        <Column class='py-0' cols={3}>
          <UrbanLegend x={this.x} y={this.y} scale={this.transforms.scale} toggle={this.toggle} />
        </Column>
        <Column cols={12} id={container_id}>
          <canvas
            id={blueprint_id}
            style={{
              'background-color': css_colors.background.cards,
              height: `${dimensions.h}px`,
              width: `100%`
            }}></canvas>
        </Column>
      </Row>
    )
  }
  public draw_map() {
    const canvas = document.getElementById(blueprint_id) as HTMLCanvasElement
    const context = canvas.getContext('2d') as CanvasRenderingContext2D
    this.resize()
    context.save()
    context.clearRect(0, 0, dimensions.w, dimensions.h)
    context.translate(this.transforms.dx, this.transforms.dy)
    context.scale(this.transforms.scale, this.transforms.scale)
    this.paint_canvas()
    context.restore()
  }
  @Watch('city')
  public recenter() {
    const city = this.city
    const canvas = document.getElementById(blueprint_id) as HTMLCanvasElement
    const context = canvas.getContext('2d') as CanvasRenderingContext2D
    // re-center
    const target = 2
    const { blocks } = settlement__blueprint(city)
    const districts = blocks.filter(({ district }) => district)
    const x = average(districts.map(({ center }) => center[0]))
    const y = average(districts.map(({ center }) => center[1]))
    const init = d3.select(context.canvas)
    this.zoom.scaleTo(init, target)
    this.zoom.translateTo(init, x, y)
  }
  @Watch('active')
  public active_on_change() {
    if (this.active)
      setTimeout(() => {
        this.draw_map()
        this.recenter()
      }, 100)
  }
}
