import StyledText from '@/components/common/text/StyledText'
import { location__get_closest_settlement } from '@/models/regions/locations'
import { settlement__blueprint } from '@/models/regions/locations/spawn/blueprints'
import { building__closest } from '@/models/regions/locations/spawn/blueprints/buildings'
import { districts__cutoff } from '@/models/regions/locations/spawn/blueprints/districts'
import { district__decorated } from '@/models/regions/locations/spawn/blueprints/districts/spawn'
import { view_module } from '@/store/view'
import { style__legend_expansion, style__legend_panel, style__legend_title } from '@/styles/legend'
import { css } from '@emotion/css'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import {
  ExpansionPanel,
  ExpansionPanelContent,
  ExpansionPanelHeader,
  ExpansionPanels
} from '../../../common/vuetify/expansion'
import { Column, Container, Divider, Row } from '../../../common/vuetify/layout'

interface LegendProps {
  x: number
  y: number
  scale: number
  toggle: JSX.Element
}

const styles__list = css`
  list-style-position: inside;
`

@Component
export default class UrbanLegend extends tsx.Component<LegendProps> {
  @Prop() x: number
  @Prop() y: number
  @Prop() scale: number
  @Prop() toggle: JSX.Element
  private panel = 0

  public get city() {
    return location__get_closest_settlement(view_module.codex.location)
  }

  public render() {
    const { miles, blocks, diagram } = settlement__blueprint(this.city)
    const i = diagram.delaunay.find(this.x, this.y)
    const block = blocks[i]
    const neighbors = block.n.map(n => blocks[n])
    const districts = [block, ...neighbors]
      .filter(n => n.district)
      .map(block => this.city.map.districts[block.district.idx])
      .sort((a, b) => a.idx - b.idx)
    const scale = `${(miles / this.scale).toFixed(2)} mi`
    const show_districts =
      districts__cutoff(Object.values(this.city.map.districts).length) > this.scale
    const selected = this.city.map.districts[block?.district?.idx]
    const buildings = selected?.buildings ?? []
    const building = building__closest({
      buildings: buildings,
      point: { x: this.x, y: this.y }
    })
    return (
      <ExpansionPanels
        class={`ma-5 ${style__legend_expansion}`}
        v-model={this.panel}
        flat
        accordion>
        <ExpansionPanel class={style__legend_panel}>
          <ExpansionPanelHeader>
            <Container class='pa-0'>
              <Row wrap class='ma-0'>
                <Column cols={12} class={`${style__legend_title} pa-0`}>
                  Legend
                </Column>
                <Column class='pa-0 pt-1'>
                  <span class='text-caption grey--text'>
                    {this.city.name}, {this.city.type}
                  </span>
                </Column>
              </Row>
            </Container>
          </ExpansionPanelHeader>
          <ExpansionPanelContent>
            <Row class='pb-5 pt-1'>
              <Divider />
            </Row>
            <Row class='pb-5 pt-1 pl-2'>
              <Row wrap class='pt-1 ma-0 text-caption'>
                <Column cols={12} class='pa-0'>
                  {selected
                    ? `District #${selected.idx}: ${selected.name} (${selected.type})`
                    : block.land
                    ? 'Outskirts'
                    : 'Ocean'}
                </Column>
                {building && (
                  <Column cols={12} class='pa-0'>
                    {`Building #${building.idx}: ${building.type}, ${building.quality.desc}`}
                  </Column>
                )}
                <Column cols={12} class='pa-0'>
                  {`Scale: ${scale} x ${scale} (${this.scale.toFixed(2)})`}
                </Column>
              </Row>
            </Row>
            <Row class='pb-5 pt-1'>
              <Divider />
            </Row>
            <Row wrap>
              <ol class={`pl-2 mb-4 text-caption ${styles__list}`}>
                {show_districts
                  ? districts.map(district => (
                      <li value={district.idx}>
                        <StyledText
                          text={`${district.name} (${district__decorated(district)})`}></StyledText>
                      </li>
                    ))
                  : buildings.map(building => {
                      return (
                        <li value={building.idx}>{`${building.type}, ${building.quality.desc}`}</li>
                      )
                    })}
              </ol>
            </Row>
            <Row class='pb-5 pt-1'>
              <Divider />
            </Row>
            <Row class='ml-2'>{this.toggle}</Row>
          </ExpansionPanelContent>
        </ExpansionPanel>
      </ExpansionPanels>
    )
  }
}
