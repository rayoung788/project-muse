import { province__hub, province__neighbors } from '@/models/regions/provinces'
import { point__distance } from '@/models/utilities/math/points'
import { formatters } from '@/models/utilities/text/formatters'
import {
  world__get_feature,
  world__get_mountains,
  world__gps,
  world__h_to_mi
} from '@/models/world'
import { cell__has_roads, cell__nation } from '@/models/world/cells'
import { view_module } from '@/store/view'
import { style__legend_expansion, style__legend_panel, style__legend_title } from '@/styles/legend'
import * as d3 from 'd3'
import { Component, Prop, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import {
  ExpansionPanel,
  ExpansionPanelContent,
  ExpansionPanelHeader,
  ExpansionPanels
} from '../../../common/vuetify/expansion'
import { RadioButton, RadioGroup } from '../../../common/vuetify/input'
import { Column, Container, Divider, Row } from '../../../common/vuetify/layout'
import { canvas__breakpoints, draw_styles } from './canvas/draw_styles'
import { world_canvas_id } from './ids'

type transforms = { dx: number; dy: number; scale: number }

interface MapInfoProps {
  transforms: transforms
  init: boolean
  active: boolean
  toggle: JSX.Element
}

interface MapInfoEvents {
  onTransformer: {
    x: number
    y: number
    k: number
  }
  onStyleChange: draw_styles
}

@Component
export default class MapInfo extends tsx.Component<MapInfoProps, MapInfoEvents> {
  @Prop() transforms: transforms
  @Prop() toggle: JSX.Element
  @Prop(Boolean) init: boolean
  @Prop(Boolean) active: boolean

  private x = 0
  private y = 0
  private region = 0
  private loc = 0
  private nation = 0
  private zoom: any
  private debug: string[] = []
  private panel = 0
  private styles: draw_styles[] = ['Nations', 'Climate', 'Tech', 'Cultures', 'Religions']
  private style: draw_styles = 'Nations'

  public render() {
    return (
      <ExpansionPanels class={`ma-5 ${style__legend_expansion}`} v-model={this.panel} accordion>
        <ExpansionPanel class={style__legend_panel}>
          <ExpansionPanelHeader>
            <Container class='pa-0'>
              <Row wrap class='ma-0'>
                <Column cols={12} class={`${style__legend_title} pa-0`}>
                  Legend
                </Column>
                <Column class='pa-0 pt-1'>
                  <span class='text-caption grey--text'>World</span>
                </Column>
              </Row>
            </Container>
          </ExpansionPanelHeader>
          <ExpansionPanelContent class='text-caption'>
            <Row class='pb-5 pt-1'>
              <Divider />
            </Row>
            <Row class='pt-1 ma-0'>
              {this.debug.map((text, i) => (
                <Column key={i} cols={12} class='pa-0'>
                  {{ text }}
                </Column>
              ))}
            </Row>
            <Row class='pt-5'>
              <Divider />
            </Row>
            <Row align='center' justify='center'>
              <Column>
                <RadioGroup v-model={this.style}>
                  <Row class='pl-5'>
                    {this.styles.map((v, i) => (
                      <Column cols={5} class='pa-0'>
                        <RadioButton color='black' key={i} label={v} value={v} />
                      </Column>
                    ))}
                  </Row>
                </RadioGroup>
              </Column>
            </Row>
            <Divider class='mb-4' />
            <Row class='ml-2'>{this.toggle}</Row>
          </ExpansionPanelContent>
        </ExpansionPanel>
      </ExpansionPanels>
    )
  }

  public mounted() {
    this.panZoom()
  }

  get dim() {
    return window.world.dim
  }

  get len() {
    return this.dim.h / 2
  }

  get gps() {
    return view_module.gps
  }

  private panZoom() {
    const canvas = document.getElementById(world_canvas_id) as HTMLCanvasElement
    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
    this.zoom = d3
      .zoom()
      .scaleExtent([10, 200])
      .translateExtent([
        [0, 0],
        [this.dim.w, this.dim.h]
      ])
      .on('zoom', () => this.$emit('transformer', d3.event.transform))
    const init = d3.select(ctx.canvas)
    init.on('mousedown', this.transition)
    init.on('touchmove mousemove', this.mouseover)
    init.call(this.zoom)
  }
  private mouseover() {
    const [x, y] = d3.mouse(document.querySelector(`#${world_canvas_id}`))
    const tx = (x - this.transforms.dx) / this.transforms.scale
    const ty = (y - this.transforms.dy) / this.transforms.scale
    if (tx < this.dim.w - 1) this.x = tx
    if (ty < this.dim.h - 1) this.y = ty
    this.updateDebug()
  }

  private updateDebug() {
    const cell = window.world.diagram.delaunay.find(this.x, this.y)
    const { longitude, latitude } = world__gps({ x: this.x, y: this.y })
    const poly = window.world.cells[cell]
    const province = window.world.provinces[poly.province]
    if (!province) return
    const hub = province__hub(province)
    const curr_nation = window.world.regions[province.curr_nation]
    const region = window.world.regions[province.region]
    const culture = window.world.cultures[region.culture.native]
    const religion = window.world.religions[culture.religion]
    const feature = world__get_feature(poly)
    const width = (window.world.dim.rw / this.transforms.scale).toFixed(0)
    const height = (window.world.dim.rh / this.transforms.scale).toFixed(0)
    const elevation = world__h_to_mi(poly.h || 0)
    this.debug = [
      `Cell: ${cell} (${this.x.toFixed(2)}, ${this.y.toFixed(2)})`,
      `Scale: ${width} mi x ${height} mi (${this.transforms.scale.toFixed(2)})`,
      `Location: ${latitude.toFixed(2)}°, ${longitude.toFixed(2)}°`,
      `Elevation: ${elevation.toFixed(2)} mi`,
      `Climate: ${region.climate}`,
      `Region: ${region.name} (${region.idx})`,
      `Province: ${hub.name} (${poly.province}) [${province.wealth.toFixed(2)}${
        window.world.cultures[curr_nation.culture.ruling].currency
      }]`,
      `Artery: ${province.artery
        .map(i => province__hub(window.world.provinces[i]).name)
        .join(', ')}`,
      `Culture: ${culture.name} (${culture.idx})`,
      `Religion: ${religion.name} (${religion.idx})`,
      `${feature.name} (${formatters.percent({ value: feature.size, precision: 2 })}) [${
        poly.landmark
      }]`
    ]
    if (cell__has_roads(poly)) {
      const land_routes = poly.roads.land
        .map(road => {
          const { src, dst } = window.world.routes.land[road]
          return [src, dst]
        })
        .flat()
      const sea_routes = poly.roads.sea
        .map(road => {
          const { src, dst } = window.world.routes.sea[road]
          return [src, dst]
        })
        .flat()
      const hubs = Array.from(new Set(land_routes.concat(sea_routes)))
        .sort()
        .map(i => province__hub(window.world.provinces[i]).name)
        .join(', ')
      this.debug.push(`Roads: ${hubs}`)
    }
    const mountain = world__get_mountains(poly.mountain)
    if (mountain) this.debug.push(mountain)
    this.region = poly.region
    this.loc = hub.idx
    this.nation = cell__nation(poly)
  }

  private transition() {
    const local_scale = this.transforms.scale > canvas__breakpoints.regional
    const global_scale = this.transforms.scale <= canvas__breakpoints.global
    const local = (view_module.codex.current === 'location' && !global_scale) || local_scale
    const current = local
      ? 'location'
      : this.style === 'Religions'
      ? 'religion'
      : this.style === 'Cultures'
      ? 'culture'
      : 'nation'
    const region = window.world.regions[this.region]
    const culture = window.world.cultures[region.culture.native]
    const curr = window.world.locations[this.loc]
    const province = window.world.provinces[curr.province]
    const provinces = [province].concat(province__neighbors(province))
    const prospects = provinces
      .map(prov => prov.locations.map(l => window.world.locations[l]))
      .flat()
    const { loc } =
      view_module.codex.current === 'location' &&
      this.transforms.scale > canvas__breakpoints.regional
        ? prospects.reduce(
            (closest, curr) => {
              const dist = point__distance({ points: [curr, { x: this.x, y: this.y }] })
              return dist < closest.dist ? { loc: curr, dist } : closest
            },
            { loc: undefined, dist: Infinity }
          )
        : { loc: curr }
    const target =
      current === 'nation'
        ? window.world.regions[this.nation]
        : current === 'location'
        ? loc
        : current === 'culture'
        ? culture
        : current === 'religion'
        ? window.world.religions[culture.religion]
        : false
    if (target) view_module.update_codex({ target, disable_zoom: true })
    view_module.trigger_redraw()
  }

  @Watch('init')
  public onInit() {
    const nation = view_module.codex.nation
    const capital = window.world.provinces[nation.capital]
    const hub = province__hub(capital)
    view_module.set_gps({
      y: hub.y,
      x: hub.x,
      zoom: 10
    })
  }

  @Watch('gps')
  public zoom_to() {
    if (this.active) {
      const canvas = document.getElementById(world_canvas_id) as HTMLCanvasElement
      const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
      const { x, y, zoom } = this.gps
      const init = d3.select(ctx.canvas)
      this.zoom.scaleTo(init, zoom)
      this.zoom.translateTo(init, x, y)
      this.x = x
      this.y = y
    }
  }
  @Watch('style')
  public style_change() {
    this.$emit('styleChange', this.style)
  }
  @Watch('transforms')
  public watch_transform(o: transforms, n: transforms) {
    this.updateDebug()
    const regional_transition =
      n.scale <= canvas__breakpoints.global && o.scale > canvas__breakpoints.global
    const global_transition =
      o.scale <= canvas__breakpoints.global && n.scale > canvas__breakpoints.global
    const local_transition =
      n.scale <= canvas__breakpoints.regional && o.scale > canvas__breakpoints.regional
    if (regional_transition || local_transition || global_transition) {
      this.transition()
    }
  }
}
