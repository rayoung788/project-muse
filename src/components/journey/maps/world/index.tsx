import { css_colors } from '@/styles/colors'
import MapInfo from '@/components/journey/maps/world/legend'
import { view_module } from '@/store/view'
import { Component, Prop, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { Column, Row } from '../../../common/vuetify/layout'
import { draw_styles } from './canvas/draw_styles'
import { draw_regions } from './canvas/borders'
import { draw_terrain_icons, terrain__icons } from './icons/terrain'
import {
  draw_avatar_location,
  draw_locations,
  draw_locations_regional,
  draw_roads
} from './canvas/infrastructure'
import { world_canvas_id, world_container_id } from './ids'
import { region__neighbors } from '@/models/regions'
import { fonts } from '@/styles/fonts'
import { style__main_content } from '@/styles'
import { draw_lakes, draw_oceans } from './canvas/coasts'
import { icon_path } from './icons'
import { location__icons } from './icons/locations'

const load_image = (path: string): Promise<HTMLImageElement> => {
  return new Promise(resolve => {
    const img = new Image()
    img.onload = () => resolve(img)
    img.onerror = () => resolve(img)
    img.src = path
  })
}
@Component({
  components: {
    MapInfo
  }
})
export default class WorldMap extends tsx.Component<{ active: boolean; toggle: JSX.Element }> {
  @Prop() active: boolean
  @Prop() toggle: JSX.Element
  private cached_images: Record<string, HTMLImageElement> = {}
  private init = false
  private style: draw_styles = 'Nations'
  private transforms = {
    dx: 0,
    dy: 0,
    scale: 1
  }
  private get dim() {
    return window.world.dim
  }
  private get height() {
    return this.dim.h
  }
  get redraw() {
    return view_module.redraw
  }

  private load_font() {
    const canvas = document.createElement('canvas')
    const ctx = canvas.getContext('2d')
    ctx.font = `4px ${fonts.maps}`
    ctx.fillText('text', 0, 8)
  }

  private resize() {
    const canvas = document.getElementById(world_canvas_id) as HTMLCanvasElement
    const container = document.getElementById(world_container_id)
    canvas.width = container.clientWidth
    canvas.height = this.height
  }

  public async mounted() {
    this.load_font()
    this.cached_images = (
      await Promise.all([
        ...Object.entries(terrain__icons).map(async ([k, v]) => ({
          img: await load_image(icon_path + v.path),
          index: k
        })),
        ...Object.entries(location__icons).map(async ([k, v]) => ({
          img: await load_image(icon_path + v.path),
          index: k
        }))
      ])
    ).reduce((dict: Record<string, HTMLImageElement>, { index, img }) => {
      dict[index] = img
      return dict
    }, {})
    this.resize()
    this.paint_canvas()
    this.init = true
  }
  private transformer(transform = { x: 0, y: 0, k: 1 }) {
    this.transforms = { dx: transform.x, dy: transform.y, scale: transform.k }
    this.draw_map()
  }
  private style_change(style: draw_styles) {
    this.style = style
    this.draw_map()
  }
  public async paint_canvas() {
    const canvas = document.getElementById(world_canvas_id) as HTMLCanvasElement
    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
    const { scale } = this.transforms
    const loc = view_module.codex.location
    const province = window.world.provinces[loc.province]
    const nation = window.world.regions[province.curr_nation]
    const borders = region__neighbors(nation).map(r => window.world.regions[r])
    const nations = [nation].concat(borders)
    const nation_set = new Set(nations.map(n => n.idx))
    const expanded = new Set(
      nations
        .map(r =>
          r.regions
            .map(p => {
              const province = window.world.provinces[p]
              const region = window.world.regions[province.region]
              return [region.idx, ...region.borders]
            })
            .flat()
        )
        .flat()
    )
    const lands = draw_oceans({ ctx, scale, nations })
    draw_regions({ ctx, style: this.style, scale, nations })
    draw_lakes({ ctx, scale, nations })
    draw_roads({ ctx, scale, nation_set })
    draw_terrain_icons({ ctx, cached_images: this.cached_images, scale, regions: expanded, lands })
    draw_avatar_location({ ctx, loc, scale })
    draw_locations_regional({
      ctx,
      scale,
      nation_set,
      cached_images: this.cached_images
    })
    draw_locations({ ctx, scale, province, cached_images: this.cached_images })
  }

  public render() {
    return (
      <Row wrap>
        <Column class='py-0' cols={3}>
          <MapInfo
            onTransformer={this.transformer}
            onStyleChange={this.style_change}
            transforms={this.transforms}
            init={this.init}
            active={this.active}
            toggle={this.toggle}
          />
        </Column>
        <Column
          class='py-3'
          cols={12}
          id={world_container_id}
          style={{ 'max-height': `${this.height + 25}px` }}>
          <canvas
            class={style__main_content}
            id={world_canvas_id}
            style={{
              'background-color': css_colors.background.map,
              filter: 'contrast(0.9) sepia(0.3) url(#noiseFilter)',
              height: `${this.height}px`,
              width: `100%`
            }}></canvas>
        </Column>
        {/* https://tympanus.net/codrops/2019/02/19/svg-filter-effects-creating-texture-with-feturbulence/ */}
        <svg height='0'>
          <filter id='noiseFilter'>
            <feTurbulence type='fractalNoise' baseFrequency='0.02' result='noise' numOctaves='5' />
            <feDiffuseLighting in='noise' lighting-color='white' result='paper' surfaceScale='2'>
              <feDistantLight azimuth='45' elevation='60' />
            </feDiffuseLighting>
            <feBlend in='SourceGraphic' in2='paper' mode='multiply' />
          </filter>
        </svg>
      </Row>
    )
  }
  @Watch('redraw')
  private draw_map() {
    if (this.active) {
      const canvas = document.getElementById(world_canvas_id) as HTMLCanvasElement
      const context = canvas.getContext('2d') as CanvasRenderingContext2D
      this.resize()
      context.save()
      context.clearRect(0, 0, this.height, this.height)
      context.translate(this.transforms.dx, this.transforms.dy)
      context.scale(this.transforms.scale, this.transforms.scale)
      this.paint_canvas()
      context.restore()
    }
  }
}
