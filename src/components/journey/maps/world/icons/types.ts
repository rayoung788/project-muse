export interface IconDef {
  height: number
  path: string
  opacity: number
}
