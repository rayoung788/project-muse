import { IconDef } from '../../types'
import { grass_icon } from './types'

const height = 4

export const grass__icons: Record<grass_icon, IconDef> = {
  grass_1: { height, path: 'terrain/grass/1_1.png', opacity: 0.4 },
  grass_2: { height, path: 'terrain/grass/1_2.png', opacity: 0.4 },
  grass_3: { height, path: 'terrain/grass/1_3.png', opacity: 0.4 },
  grass_4: { height, path: 'terrain/grass/1_4.png', opacity: 0.4 },
  grass_5: { height, path: 'terrain/grass/1_5.png', opacity: 0.4 },
  grass_6: { height, path: 'terrain/grass/1_6.png', opacity: 0.4 },
  grass_7: { height, path: 'terrain/grass/1_7.png', opacity: 0.4 },
  grass_8: { height, path: 'terrain/grass/1_8.png', opacity: 0.4 }
}
