export type grass_icon =
  | 'grass_1'
  | 'grass_2'
  | 'grass_3'
  | 'grass_4'
  | 'grass_5'
  | 'grass_6'
  | 'grass_7'
  | 'grass_8'
