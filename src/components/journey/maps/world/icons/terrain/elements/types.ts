import { battle_icon } from './battles/types'
import { ship_icon } from './ships/types'

export type element_icon = battle_icon | ship_icon
