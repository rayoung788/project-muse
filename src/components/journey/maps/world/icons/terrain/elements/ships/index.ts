import { IconDef } from '../../../types'
import { ship_icon } from './types'

const height = 4

export const ship__icons: Record<ship_icon, IconDef> = {
  ship_1: {
    height,
    path: 'terrain/elements/ships/1.png',
    opacity: 0.8
  },
  ship_2: {
    height,
    path: 'terrain/elements/ships/2.png',
    opacity: 0.8
  },
  ship_3: {
    height,
    path: 'terrain/elements/ships/3.png',
    opacity: 0.8
  },
  ship_4: {
    height,
    path: 'terrain/elements/ships/4.png',
    opacity: 0.8
  },
  ship_5: {
    height,
    path: 'terrain/elements/ships/5.png',
    opacity: 0.8
  },
  ship_6: {
    height,
    path: 'terrain/elements/ships/6.png',
    opacity: 0.8
  },
  ship_7: {
    height,
    path: 'terrain/elements/ships/7.png',
    opacity: 0.8
  },
  ship_8: {
    height,
    path: 'terrain/elements/ships/8.png',
    opacity: 0.8
  },
  ship_9: {
    height,
    path: 'terrain/elements/ships/9.png',
    opacity: 0.8
  },
  ship_10: {
    height,
    path: 'terrain/elements/ships/10.png',
    opacity: 0.8
  },
  ship_11: {
    height,
    path: 'terrain/elements/ships/11.png',
    opacity: 0.8
  },
  ship_12: {
    height,
    path: 'terrain/elements/ships/12.png',
    opacity: 0.8
  },
  ship_13: {
    height,
    path: 'terrain/elements/ships/13.png',
    opacity: 0.8
  },
  ship_14: {
    height,
    path: 'terrain/elements/ships/14.png',
    opacity: 0.8
  },
  ship_15: {
    height,
    path: 'terrain/elements/ships/15.png',
    opacity: 0.8
  },
  ship_16: {
    height,
    path: 'terrain/elements/ships/16.png',
    opacity: 1
  }
}
