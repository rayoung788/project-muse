export type swamp_icon =
  | 'swamp_1'
  | 'swamp_2'
  | 'swamp_3'
  | 'swamp_4'
  | 'swamp_5'
  | 'swamp_6'
  | 'swamp_7'
  | 'swamp_8'
