import { IconDef } from '../../../types'
import { swamp_icon } from './types'

const base_height = 4

export const swamp__icons: Record<swamp_icon, IconDef> = {
  swamp_1: {
    height: base_height,
    path: 'terrain/trees/swamp/1/1.png',
    opacity: 0.4
  },
  swamp_2: {
    height: base_height,
    path: 'terrain/trees/swamp/1/2.png',
    opacity: 0.4
  },
  swamp_3: {
    height: base_height,
    path: 'terrain/trees/swamp/1/3.png',
    opacity: 0.4
  },
  swamp_4: {
    height: base_height,
    path: 'terrain/trees/swamp/1/4.png',
    opacity: 0.4
  },
  swamp_5: {
    height: base_height,
    path: 'terrain/trees/swamp/2/1.png',
    opacity: 0.4
  },
  swamp_6: {
    height: base_height,
    path: 'terrain/trees/swamp/2/2.png',
    opacity: 0.4
  },
  swamp_7: {
    height: base_height,
    path: 'terrain/trees/swamp/2/3.png',
    opacity: 0.4
  },
  swamp_8: {
    height: base_height,
    path: 'terrain/trees/swamp/2/4.png',
    opacity: 0.4
  }
}
