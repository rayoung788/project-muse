import { IconDef } from '../../../types'
import { withered_icon } from './types'

const base_height = 4

export const withered__icons: Record<withered_icon, IconDef> = {
  withered_1: {
    height: base_height,
    path: 'terrain/trees/withered/1/1.png',
    opacity: 0.4
  },
  withered_2: {
    height: base_height,
    path: 'terrain/trees/withered/1/2.png',
    opacity: 0.4
  },
  withered_3: {
    height: base_height,
    path: 'terrain/trees/withered/1/3.png',
    opacity: 0.4
  },
  withered_4: {
    height: base_height,
    path: 'terrain/trees/withered/1/4.png',
    opacity: 0.4
  },
  withered_5: {
    height: base_height,
    path: 'terrain/trees/withered/2/1.png',
    opacity: 0.4
  },
  withered_6: {
    height: base_height,
    path: 'terrain/trees/withered/2/2.png',
    opacity: 0.4
  },
  withered_7: {
    height: base_height,
    path: 'terrain/trees/withered/2/3.png',
    opacity: 0.4
  },
  withered_8: {
    height: base_height,
    path: 'terrain/trees/withered/2/4.png',
    opacity: 0.4
  }
}
