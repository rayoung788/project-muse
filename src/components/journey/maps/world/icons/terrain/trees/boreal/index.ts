import { IconDef } from '../../../types'
import { boreal_icon } from './types'

const height = 4

export const boreal__icons: Record<boreal_icon, IconDef> = {
  boreal_1: { height, path: 'terrain/trees/boreal/1_1.png', opacity: 0.35 },
  boreal_2: { height, path: 'terrain/trees/boreal/1_2.png', opacity: 0.35 },
  boreal_3: { height, path: 'terrain/trees/boreal/1_3.png', opacity: 0.35 },
  boreal_4: { height, path: 'terrain/trees/boreal/1_4.png', opacity: 0.35 },
  boreal_5: { height, path: 'terrain/trees/boreal/2_1.png', opacity: 0.4 },
  boreal_6: { height, path: 'terrain/trees/boreal/2_2.png', opacity: 0.4 },
  boreal_7: { height, path: 'terrain/trees/boreal/2_3.png', opacity: 0.4 },
  boreal_8: { height, path: 'terrain/trees/boreal/2_4.png', opacity: 0.4 }
}
