import { IconDef } from '../../../types'
import { tropical_icon } from './types'

const base_height = 4
const hills = 2
const hill_cluster = 2.5

export const tropical__icons: Record<tropical_icon, IconDef> = {
  tropical_1: { height: base_height, path: 'terrain/trees/tropical/1/1.png', opacity: 0.4 },
  tropical_2: { height: base_height, path: 'terrain/trees/tropical/1/2.png', opacity: 0.4 },
  tropical_3: { height: base_height, path: 'terrain/trees/tropical/1/3.png', opacity: 0.4 },
  tropical_4: { height: base_height, path: 'terrain/trees/tropical/1/4.png', opacity: 0.4 },
  tropical_5: { height: base_height, path: 'terrain/trees/tropical/2/1.png', opacity: 0.4 },
  tropical_6: { height: base_height, path: 'terrain/trees/tropical/2/2.png', opacity: 0.4 },
  tropical_7: { height: base_height, path: 'terrain/trees/tropical/2/3.png', opacity: 0.4 },
  tropical_8: { height: base_height, path: 'terrain/trees/tropical/2/4.png', opacity: 0.4 },
  tropical_9: { height: hills, path: 'terrain/trees/tropical/3/1.png', opacity: 0.5 },
  tropical_10: { height: hills, path: 'terrain/trees/tropical/3/2.png', opacity: 0.5 },
  tropical_11: { height: hills, path: 'terrain/trees/tropical/3/3.png', opacity: 0.5 },
  tropical_12: { height: hills, path: 'terrain/trees/tropical/3/4.png', opacity: 0.5 },
  tropical_13: { height: hills, path: 'terrain/trees/tropical/3/5.png', opacity: 0.5 },
  tropical_14: { height: hills, path: 'terrain/trees/tropical/3/6.png', opacity: 0.5 },
  tropical_15: { height: hills, path: 'terrain/trees/tropical/3/7.png', opacity: 0.5 },
  tropical_16: { height: hills, path: 'terrain/trees/tropical/3/8.png', opacity: 0.5 },
  tropical_17: { height: hills, path: 'terrain/trees/tropical/3/9.png', opacity: 0.5 },
  tropical_18: { height: hills, path: 'terrain/trees/tropical/3/10.png', opacity: 0.5 },
  tropical_19: { height: hills, path: 'terrain/trees/tropical/3/11.png', opacity: 0.5 },
  tropical_20: { height: hills, path: 'terrain/trees/tropical/3/12.png', opacity: 0.5 },
  tropical_21: { height: hills, path: 'terrain/trees/tropical/3/13.png', opacity: 0.5 },
  tropical_22: { height: hills, path: 'terrain/trees/tropical/3/14.png', opacity: 0.5 },
  tropical_23: { height: hill_cluster, path: 'terrain/trees/tropical/3/15.png', opacity: 0.5 },
  tropical_24: { height: hill_cluster, path: 'terrain/trees/tropical/3/16.png', opacity: 0.5 },
  tropical_25: { height: hill_cluster, path: 'terrain/trees/tropical/3/17.png', opacity: 0.5 },
  tropical_26: { height: hill_cluster, path: 'terrain/trees/tropical/3/18.png', opacity: 0.5 }
}
