import { boreal_icon } from './boreal/types'
import { savanna_icon } from './savanna/types'
import { swamp_icon } from './swamp/types'
import { temperate_icon } from './temperate/types'
import { tropical_icon } from './tropical/types'
import { withered_icon } from './withered/types'

export type tree_icon =
  | boreal_icon
  | temperate_icon
  | savanna_icon
  | tropical_icon
  | withered_icon
  | swamp_icon
