export type boreal_icon =
  | 'boreal_1'
  | 'boreal_2'
  | 'boreal_3'
  | 'boreal_4'
  | 'boreal_5'
  | 'boreal_6'
  | 'boreal_7'
  | 'boreal_8'
