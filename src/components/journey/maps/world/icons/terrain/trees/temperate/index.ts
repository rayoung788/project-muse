import { IconDef } from '../../../types'
import { temperate_icon } from './types'

const height = 4

export const temperate__icons: Record<temperate_icon, IconDef> = {
  temperate_1: { height, path: 'terrain/trees/temperate/1_1.png', opacity: 0.4 },
  temperate_2: { height, path: 'terrain/trees/temperate/1_2.png', opacity: 0.4 },
  temperate_3: { height, path: 'terrain/trees/temperate/1_3.png', opacity: 0.4 },
  temperate_4: { height, path: 'terrain/trees/temperate/2_1.png', opacity: 0.4 },
  temperate_5: { height, path: 'terrain/trees/temperate/2_2.png', opacity: 0.4 },
  temperate_6: { height, path: 'terrain/trees/temperate/2_3.png', opacity: 0.4 },
  temperate_7: { height, path: 'terrain/trees/temperate/2_4.png', opacity: 0.4 },
  temperate_8: { height, path: 'terrain/trees/temperate/2_5.png', opacity: 0.4 },
  temperate_9: { height, path: 'terrain/trees/temperate/2_6.png', opacity: 0.4 }
}
