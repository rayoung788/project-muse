import { IconDef } from '../../../types'
import { savanna_icon } from './types'

const height = 4

export const savanna__icons: Record<savanna_icon, IconDef> = {
  savanna_1: { height, path: 'terrain/trees/savanna/1.png', opacity: 0.5 },
  savanna_2: { height, path: 'terrain/trees/savanna/2.png', opacity: 0.5 },
  savanna_3: { height, path: 'terrain/trees/savanna/3.png', opacity: 0.5 },
  savanna_4: { height, path: 'terrain/trees/savanna/4.png', opacity: 0.5 }
}
