import { IconDef } from '../../types'
import { mountain_icon } from './types'

const heights = [7, 5, 4.5, 3.5, 2.5, 1.5]

export const mountain__icons: Record<mountain_icon, IconDef> = {
  mountain_1_0_1: {
    height: heights[0],
    path: 'terrain/mountains/style_1/0_1.png',
    opacity: 1
  },
  mountain_1_0_2: {
    height: heights[0],
    path: 'terrain/mountains/style_1/0_2.png',
    opacity: 1
  },
  mountain_1_0_3: {
    height: heights[0],
    path: 'terrain/mountains/style_1/0_3.png',
    opacity: 1
  },
  mountain_1_0_4: {
    height: heights[0],
    path: 'terrain/mountains/style_1/0_4.png',
    opacity: 1
  },
  mountain_1_0_5: {
    height: heights[0],
    path: 'terrain/mountains/style_1/0_5.png',
    opacity: 1
  },
  mountain_1_1_1: {
    height: heights[1],
    path: 'terrain/mountains/style_1/1_1.png',
    opacity: 1
  },
  mountain_1_1_2: {
    height: heights[1],
    path: 'terrain/mountains/style_1/1_2.png',
    opacity: 0.8
  },
  mountain_1_1_3: {
    height: heights[1],
    path: 'terrain/mountains/style_1/1_3.png',
    opacity: 1
  },
  mountain_1_2_1: {
    height: heights[2],
    path: 'terrain/mountains/style_1/2_1.png',
    opacity: 0.8
  },
  mountain_1_2_2: {
    height: heights[2],
    path: 'terrain/mountains/style_1/2_2.png',
    opacity: 0.8
  },
  mountain_1_2_3: {
    height: heights[2],
    path: 'terrain/mountains/style_1/2_3.png',
    opacity: 1
  },
  mountain_1_2_4: {
    height: heights[2],
    path: 'terrain/mountains/style_1/2_4.png',
    opacity: 0.8
  },
  mountain_1_3_1: {
    height: heights[3],
    path: 'terrain/mountains/style_1/3_1.png',
    opacity: 0.8
  },
  mountain_1_3_2: {
    height: heights[3],
    path: 'terrain/mountains/style_1/3_2.png',
    opacity: 1
  },
  mountain_1_3_3: {
    height: heights[3],
    path: 'terrain/mountains/style_1/3_3.png',
    opacity: 0.8
  },
  mountain_1_3_4: {
    height: heights[3],
    path: 'terrain/mountains/style_1/3_4.png',
    opacity: 1
  },
  mountain_1_4_1: {
    height: heights[4],
    path: 'terrain/mountains/style_1/4_1.png',
    opacity: 1
  },
  mountain_1_4_2: {
    height: heights[4],
    path: 'terrain/mountains/style_1/4_2.png',
    opacity: 0.8
  },
  mountain_1_4_3: {
    height: heights[4],
    path: 'terrain/mountains/style_1/4_3.png',
    opacity: 0.8
  },
  mountain_1_4_4: {
    height: heights[4],
    path: 'terrain/mountains/style_1/4_4.png',
    opacity: 0.8
  },
  mountain_1_5_1: {
    height: heights[5],
    path: 'terrain/mountains/style_1/5_1.png',
    opacity: 0.8
  },
  mountain_1_5_2: {
    height: heights[5],
    path: 'terrain/mountains/style_1/5_2.png',
    opacity: 0.8
  },
  mountain_1_5_3: {
    height: heights[5],
    path: 'terrain/mountains/style_1/5_3.png',
    opacity: 0.8
  },
  mountain_1_5_4: {
    height: heights[5],
    path: 'terrain/mountains/style_1/5_4.png',
    opacity: 0.8
  },
  mountain_1_5_5: {
    height: heights[5],
    path: 'terrain/mountains/style_1/5_5.png',
    opacity: 0.8
  },
  mountain_1_5_6: {
    height: heights[5],
    path: 'terrain/mountains/style_1/5_6.png',
    opacity: 0.8
  },
  volcano_0: {
    height: heights[2],
    path: 'terrain/mountains/volcanoes/0.png',
    opacity: 1
  },
  volcano_1: {
    height: heights[2],
    path: 'terrain/mountains/volcanoes/1.png',
    opacity: 1
  },
  volcano_2: {
    height: heights[2],
    path: 'terrain/mountains/volcanoes/2.png',
    opacity: 1
  }
}
