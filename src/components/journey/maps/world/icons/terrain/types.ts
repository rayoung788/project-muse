import { desert_icon } from './desert/types'
import { element_icon } from './elements/types'
import { grass_icon } from './grass/types'
import { mountain_icon } from './mountains/types'
import { tree_icon } from './trees/types'

export type terrain_icon = mountain_icon | grass_icon | desert_icon | tree_icon | element_icon
