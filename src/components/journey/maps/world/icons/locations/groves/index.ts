import { location_icon__size } from '../common'
import { LocationIcon } from '../types'
import { grove__icon } from './types'

const generic = location_icon__size

export const grove__icons: Record<grove__icon, LocationIcon> = {
  grove_1: {
    height: generic.height * 1.3,
    path: 'locations/groves/dead/1.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_2: {
    height: generic.height * 1.3,
    path: 'locations/groves/dead/2.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_3: {
    height: generic.height * 1.3,
    path: 'locations/groves/dead/3.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_4: {
    height: generic.height * 1.3,
    path: 'locations/groves/dead/4.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_5: {
    height: generic.height * 1.3,
    path: 'locations/groves/dead/5.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_6: {
    height: generic.height * 1.3,
    path: 'locations/groves/dead/6.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_7: {
    height: generic.height * 1.3,
    path: 'locations/groves/dead/7.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_8: {
    height: generic.height * 1.3,
    path: 'locations/groves/generic/1.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_9: {
    height: generic.height * 1.3,
    path: 'locations/groves/generic/2.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_10: {
    height: generic.height * 1.3,
    path: 'locations/groves/generic/3.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_11: {
    height: generic.height * 1.3,
    path: 'locations/groves/generic/4.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_12: {
    height: generic.height * 1.3,
    path: 'locations/groves/generic/5.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_13: {
    height: generic.height * 1.3,
    path: 'locations/groves/generic/6.png',
    opacity: 1,
    font_scale: generic.font
  },
  grove_14: {
    height: generic.height * 1.3,
    path: 'locations/groves/generic/7.png',
    opacity: 1,
    font_scale: generic.font
  }
}
