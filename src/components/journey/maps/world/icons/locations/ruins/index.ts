import { location_icon__size } from '../common'
import { LocationIcon } from '../types'
import { ruins__icon } from './types'

const generic = location_icon__size

export const ruins__icons: Record<ruins__icon, LocationIcon> = {
  ruins_1: {
    height: generic.height,
    path: 'locations/ruins/1.png',
    opacity: 1,
    font_scale: generic.font
  },
  ruins_2: {
    height: generic.height,
    path: 'locations/ruins/2.png',
    opacity: 1,
    font_scale: generic.font
  },
  ruins_3: {
    height: generic.height,
    path: 'locations/ruins/3.png',
    opacity: 1,
    font_scale: generic.font
  },
  ruins_4: {
    height: generic.height,
    path: 'locations/ruins/4.png',
    opacity: 1,
    font_scale: generic.font
  },
  ruins_5: {
    height: generic.height,
    path: 'locations/ruins/5.png',
    opacity: 1,
    font_scale: generic.font
  },
  ruins_6: {
    height: generic.height,
    path: 'locations/ruins/6.png',
    opacity: 1,
    font_scale: generic.font
  }
}
