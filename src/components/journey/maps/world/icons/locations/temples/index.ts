import { location_icon__size } from '../common'
import { LocationIcon } from '../types'
import { temple__icon } from './types'

const generic = location_icon__size

export const temple__icons: Record<temple__icon, LocationIcon> = {
  temple_1: {
    height: generic.height * 1.5,
    path: 'locations/temples/1.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_2: {
    height: generic.height * 1.5,
    path: 'locations/temples/2.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_3: {
    height: generic.height * 1.5,
    path: 'locations/temples/3.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_4: {
    height: generic.height * 1.5,
    path: 'locations/temples/4.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_5: {
    height: generic.height,
    path: 'locations/temples/5.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_6: {
    height: generic.height,
    path: 'locations/temples/6.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_7: {
    height: generic.height,
    path: 'locations/temples/7.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_8: {
    height: generic.height,
    path: 'locations/temples/8.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_9: {
    height: generic.height,
    path: 'locations/temples/9.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_10: {
    height: generic.height,
    path: 'locations/temples/10.png',
    opacity: 1,
    font_scale: generic.font
  },
  temple_11: {
    height: generic.height,
    path: 'locations/temples/11.png',
    opacity: 1,
    font_scale: generic.font
  }
}
