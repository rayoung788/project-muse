import { location_icon__size } from '../common'
import { LocationIcon } from '../types'
import { shrine__icon } from './types'

const generic = location_icon__size

export const shrine__icons: Record<shrine__icon, LocationIcon> = {
  shrine_1: {
    height: generic.height,
    path: 'locations/shrines/1.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_2: {
    height: generic.height,
    path: 'locations/shrines/2.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_3: {
    height: generic.height * 1.5,
    path: 'locations/shrines/3.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_4: {
    height: generic.height * 1.5,
    path: 'locations/shrines/4.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_5: {
    height: generic.height,
    path: 'locations/shrines/5.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_6: {
    height: generic.height,
    path: 'locations/shrines/6.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_7: {
    height: generic.height * 1.5,
    path: 'locations/shrines/7.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_8: {
    height: generic.height * 1.5,
    path: 'locations/shrines/8.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_9: {
    height: generic.height * 1.5,
    path: 'locations/shrines/9.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_10: {
    height: generic.height * 1.5,
    path: 'locations/shrines/10.png',
    opacity: 1,
    font_scale: generic.font
  },
  shrine_11: {
    height: generic.height * 1.5,
    path: 'locations/shrines/11.png',
    opacity: 1,
    font_scale: generic.font
  }
}
