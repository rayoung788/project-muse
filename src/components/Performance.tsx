import { ProfileNode, Profiles } from '@/models/utilities/performance'
import { componentFactory } from 'vue-tsx-support'
import NestedPieChart from './common/charts/pie/NestedPieChart'
import { CustomPieChartTooltip, NestedPieData } from './common/charts/pie/types'
import { TabbedContent } from './common/tabs/button_group'

/**
 * returns the final tree structure of profile node used for display
 * @param param - profile node
 * @returns - tree structure
 */
const profile__get_nodes = ({ value, label, color, children }: ProfileNode): NestedPieData => {
  const node: NestedPieData = {
    label,
    value: value,
    color,
    children: Object.values(children).map(child => profile__get_nodes(child))
  }
  const total = node.children.reduce((sum, child) => sum + child.value, 0)
  const diff = node.value - total
  if (diff < 0) node.value = total
  if (node.children.length === 0) delete node.children
  if (node.children && diff > 0) {
    node.children.push({
      label: 'Other',
      value: diff,
      color: 'black',
      children: []
    })
  }
  return node
}

const pie_chart__tooltips: CustomPieChartTooltip = (tooltipItems, data) => {
  const label = data.labels[tooltipItems.index]
  const value = data.datasets[0].data[tooltipItems.index]
  return `${label}: ${value.toFixed(2)} ms`
}

const PerformanceChart = componentFactory.create({
  components: {
    TabbedContent,
    NestedPieChart
  },
  render() {
    const modes = Object.keys(window.profiles)
    return (
      <TabbedContent
        selection={modes}
        default_idx={modes.findIndex(mode => mode === 'history')}
        content={(mode: string) => {
          const data = window.profiles[mode as keyof Profiles]
          const parsed = profile__get_nodes(data)
          return (
            <NestedPieChart
              type='doughnut'
              chart_data={parsed}
              tooltips={pie_chart__tooltips}
              title={node => `${node.label}: ${node.value.toFixed(2)} ms`}></NestedPieChart>
          )
        }}></TabbedContent>
    )
  }
})

export default PerformanceChart
