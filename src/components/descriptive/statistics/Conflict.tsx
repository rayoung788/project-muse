import LineChart from '@/components/common/charts/LineChart'
import NestedPieChart from '@/components/common/charts/pie/NestedPieChart'
import { pie_chart__basic_tooltips } from '@/components/common/charts/pie/PieChart'
import { NestedPieData } from '@/components/common/charts/pie/types'
import { histogram } from 'd3'
import { Rebellion } from '@/models/history/events/rebellion/types'
import { War } from '@/models/history/events/war/types'
import { color__random_preset } from '@/models/utilities/colors'
import { title_case } from '@/models/utilities/text'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { average } from '@/models/utilities/math'
import { describe_coarse_duration } from '@/models/utilities/math/time'
import BarChart from '@/components/common/charts/BarChart'
import { SimpleTabbedContent } from '@/components/common/tabs/button_group'

@Component({
  components: {
    LineChart
  }
})
export default class Conflicts extends tsx.Component<Record<string, unknown>> {
  private construct(params: { events: (War | Rebellion)[]; seed: string }): NestedPieData[] {
    const { events, seed } = params
    const event_types = events.reduce((agg: Record<string, number>, event) => {
      if (!agg[event.background.type]) agg[event.background.type] = 0
      agg[event.background.type] += 1
      return agg
    }, {})
    const colors = color__random_preset({ tags: Object.keys(event_types), seed })
    return Object.entries(event_types)
      .map(([label, value]) => {
        return {
          label,
          value,
          color: colors[label],
          children: []
        }
      })
      .sort((a, b) => b.value - a.value)
  }
  public render() {
    const colors = color__random_preset({ tags: ['rebellions', 'wars'], seed: 'conflicts' })
    const past_wars = window.world.wars
      .filter(war => Number.isFinite(war.end))
      .map(war => war.end - war.start)
    const past_rebels = window.world.rebellions
      .filter(war => Number.isFinite(war.end))
      .map(war => war.end - war.start)
    const past = past_wars.concat(past_rebels)
    const max = Math.max(...past)
    const min = Math.min(...past)
    const hist = histogram().domain([min, max]).thresholds(10)(past)
    const bins = hist.map(b => ({
      min: b.x0,
      max: b.x1,
      label: describe_coarse_duration(average([b.x0, b.x1]))
    }))
    console.log(hist)
    const bar = {
      labels: bins.map(({ label }) => label),
      datasets: [
        {
          label: `wars`,
          data: bins.map(
            b => past_wars.filter(duration => duration < b.max && duration >= b.min).length
          ),
          backgroundColor: colors['wars']
        },
        {
          label: `rebellions`,
          data: bins.map(
            b => past_rebels.filter(duration => duration < b.max && duration >= b.min).length
          ),
          backgroundColor: colors['rebellions']
        }
      ]
    }
    const conflicts: NestedPieData = {
      label: '',
      value: 0,
      color: '',
      children: [
        {
          label: 'wars',
          value: window.world.wars.length,
          color: colors['wars'],
          children: this.construct({ events: window.world.wars, seed: 'wars' })
        },
        {
          label: 'rebellions',
          value: window.world.rebellions.length,
          color: colors['rebellions'],
          children: this.construct({ events: window.world.rebellions, seed: 'rebellions' })
        }
      ]
    }
    return (
      <SimpleTabbedContent
        class='py-5'
        content={{
          types: (
            <NestedPieChart
              chart_data={conflicts}
              tooltips={pie_chart__basic_tooltips}
              title={node => `${title_case(node.label)} (${node.value})`}
              type='pie'></NestedPieChart>
          ),
          durations: <BarChart class='chart' chartData={bar}></BarChart>
        }}></SimpleTabbedContent>
    )
  }
}
