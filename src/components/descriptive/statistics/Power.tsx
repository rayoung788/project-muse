import { region__imperial_name } from '@/models/regions/diplomacy/status'
import { region__population } from '@/models/regions'
import { world__nations } from '@/models/regions'
import { climate_lookup } from '@/models/world/climate/types'
import { world__gps } from '@/models/world'
import { view_module } from '@/store/view'
import { Component, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import BubbleChart from '../../common/charts/BubbleChart'
import { Column, Row } from '../../common/vuetify/layout'

@Component({
  components: {
    BubbleChart
  }
})
export default class Power extends tsx.Component<Record<string, unknown>> {
  private chart_data = {
    backgroundColor: '',
    datasets: [] as {
      label: string
      backgroundColor: string
      borderColor: string
      data: [{ x: number; y: number; r: number; p: string }]
    }[]
  }
  private scale = 1
  get power_shift() {
    return view_module.power_shift
  }
  public mounted() {
    this.compute_chart()
  }
  @Watch('power_shift')
  private compute_chart() {
    const radius = 50 / this.scale
    const regions = world__nations()
    const max = regions.reduce((m, r) => Math.max(m, region__population(r)), -Infinity)
    const formatter = new Intl.NumberFormat('en-US', {
      notation: 'compact',
      compactDisplay: 'short'
    } as any)
    this.chart_data = {
      backgroundColor: 'transparent',
      datasets: regions
        .sort((a, b) => region__population(a) - region__population(b))
        .map(r => {
          const capital = window.world.provinces[r.capital]
          const { latitude } = world__gps(window.world.locations[capital.hub])
          const pop = region__population(r)
          const climate = climate_lookup[r.climate]
          return {
            label: `${region__imperial_name(r)} [${climate.zone}]`,
            backgroundColor: r.colors.replace('%)', '%, 0.5)'),
            borderColor: r.colors,
            fill: false,
            ridx: r.idx,
            data: [
              {
                x: latitude,
                y: r.wealth,
                r: (pop / max) * radius,
                p: formatter.format(pop)
              }
            ]
          }
        })
    }
  }
  @Watch('scale')
  public scale_change() {
    this.compute_chart()
  }
  private tooltipLabels(tooltipItems: any, data: any) {
    const label = data.datasets[tooltipItems.datasetIndex].label
    const x = data.datasets[tooltipItems.datasetIndex].data[0].x.toFixed(2)
    const y = data.datasets[tooltipItems.datasetIndex].data[0].y.toFixed(2)
    const r = data.datasets[tooltipItems.datasetIndex].data[0].p
    return `${label}: (${x}, ${y}, ${r})`
  }

  public render() {
    const speed = 0.1
    const options = {
      legend: {
        display: false
      },
      scales: {
        yAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: 'Wealth'
            }
          }
        ],
        xAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: 'Latitude'
            }
          }
        ]
      },
      layout: {
        padding: 20
      },
      title: {
        text: 'Wealth of Nations',
        display: true
      },
      tooltips: {
        callbacks: {
          label: this.tooltipLabels
        }
      },
      plugins: {
        zoom: {
          pan: {
            enabled: true
          },
          zoom: {
            enabled: true,
            drag: false,
            speed
          }
        }
      },
      onClick: (_: Event, data: any[]) => {
        const [d] = data
        const idx = d?._datasetIndex
        const ridx = d?._chart?.data?.datasets[idx]?.ridx
        const region = window.world.regions[ridx]
        if (region) view_module.update_codex({ target: region })
      }
    }
    return (
      <div
        onWheel={event => {
          const diff = event.deltaY > 0 ? 1 : -1
          this.scale *= 1 + diff * speed
        }}>
        <Row wrap>
          <Column>
            <BubbleChart height={250} chartData={this.chart_data} options={options} />
          </Column>
        </Row>
      </div>
    )
  }
}
