import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

import Nation from '@/components/descriptive/codex/nations'
import Rebellion from '@/components/descriptive/codex/nations/history/RebellionView'
import War from '@/components/descriptive/codex/nations/history/WarView'
import Actor from '@/components/descriptive/codex/npcs/actors'
import Culture from '@/components/descriptive/codex/npcs/culture'
import { view_module } from '@/store/view'
import { style__main_content } from '@/styles'

import { TransparentTabs } from '../common/tabs/transparent'
import LocationView from './codex/locations'
import ReligionView from './codex/npcs/culture/ReligionView'
import BeastView from './codex/npcs/species/BeastView'
import PrimordialView from './codex/npcs/species/PrimordialView'
import Power from './statistics/Power'
import Conflicts from './statistics/Conflict'

@Component({
  components: {
    Nation,
    Culture,
    ReligionView,
    LocationView,
    Actor,
    War,
    Rebellion,
    BeastView,
    PrimordialView,
    Conflicts,
    Power
  }
})
export default class Codex extends tsx.Component<Record<string, unknown>> {
  public render() {
    const { current } = view_module.codex
    return (
      <TransparentTabs
        tabs={{
          Codex: {
            element: (
              <div class={style__main_content}>
                {<Nation v-show={current === 'nation'}></Nation>}
                {<LocationView v-show={current === 'location'}></LocationView>}
                {<Culture v-show={current === 'culture'}></Culture>}
                {<ReligionView v-show={current === 'religion'}></ReligionView>}
                {<Actor v-show={current === 'actor'}></Actor>}
                {<War v-show={current === 'war'}></War>}
                {<Rebellion v-show={current === 'rebellion'}></Rebellion>}
                {<BeastView v-show={current === 'beast'}></BeastView>}
                {<PrimordialView v-show={current === 'primordial'}></PrimordialView>}
              </div>
            )
          },
          Conflicts: {
            element: <Conflicts class={style__main_content}></Conflicts>
          },
          Nations: {
            element: <Power class={style__main_content} />
          }
        }}></TransparentTabs>
    )
  }
}
