import { interpolateBuPu, interpolateSpectral, interpolateViridis } from 'd3-scale-chromatic'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

import BarChart from '@/components/common/charts/BarChart'
import { Icon } from '@/components/common/vuetify/icon'
import { Column, Row } from '@/components/common/vuetify/layout'
import { location__conditions } from '@/models/regions/locations/environment/conditions'
import { decorate_text } from '@/models/utilities/text/decoration'
import { range, scale } from '@/models/utilities/math'
import { format_hours, months } from '@/models/utilities/math/time'
import { world__day_length, world__gps } from '@/models/world'
import { climate_lookup } from '@/models/world/climate/types'
import { compute_heat, compute_rain, freezing_point } from '@/models/world/climate/weather'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'

import { SimpleTabbedContent } from '../../../common/tabs/button_group'
import SectionList from '../../../common/text/SectionList'
import StyledText from '../../../common/text/StyledText'
import { formatters } from '@/models/utilities/text/formatters'
import { css } from '@emotion/css'

const visible_css = (visible: boolean) => css`
  color: ${visible ? css_colors.black : css_colors.subtitle};
`

const enum CLIMATE_CHARTS {
  temperature = 'temperature',
  precipitation = 'rain',
  sunlight = 'sunlight'
}
@Component({
  components: {
    SimpleTabbedContent,
    BarChart
  }
})
export default class Weather extends tsx.Component<Record<string, never>> {
  private get climate_data() {
    const cell = window.world.cells[view_module.codex.location.cell]
    const province = window.world.provinces[view_module.codex.location.province]
    const climate = climate_lookup[window.world.regions[province.region].climate]
    const temp = range(12).map(month => {
      return compute_heat({ cell, month, climate })
    })
    return {
      labels: months,
      datasets: [
        {
          label: `Monthly Average Temperatures (°F)`,
          data: temp.map(t => t.toFixed(2)),
          backgroundColor: temp.map(t => interpolateSpectral(scale([-30, 90], [1, 0], t)))
        }
      ]
    }
  }

  private get sunlight_data() {
    const { cell } = view_module.codex.location
    const province = window.world.cells[cell]
    const { latitude } = world__gps(province)
    const hours = range(12).map(m => world__day_length(latitude, (m / 12) * 365))
    return {
      labels: months,
      datasets: [
        {
          label: 'Monthly Day Length (Hours)',
          data: hours.map(h => h.toFixed(2)),
          backgroundColor: hours.map(t => interpolateBuPu(scale([0, 30], [1, 0], t)))
        }
      ]
    }
  }
  private get rain_data() {
    const loc = view_module.codex.location
    const province = window.world.provinces[loc.province]
    const cell = window.world.cells[loc.cell]
    const climate = climate_lookup[window.world.regions[province.region].climate]
    const rain = range(12).map(month => {
      const rain = compute_rain({ climate, month, cell })
      return rain * 100
    })
    return {
      labels: months,
      datasets: [
        {
          label: `Monthly Rain Chance (%)`,
          data: rain.map(h => h.toFixed(2)),
          backgroundColor: rain.map(t => interpolateViridis(scale([0, 100], [1, 0], t)))
        }
      ]
    }
  }
  private weather_chart(data: {
    labels: string[]
    datasets: {
      label: string
      data: string[]
      backgroundColor: string[]
    }[]
  }) {
    return <BarChart class='chart' height={125} chartData={data} />
  }
  private get climate() {
    return (
      <SimpleTabbedContent
        content={{
          [CLIMATE_CHARTS.temperature]: this.weather_chart(this.climate_data),
          [CLIMATE_CHARTS.precipitation]: this.weather_chart(this.rain_data),
          [CLIMATE_CHARTS.sunlight]: this.weather_chart(this.sunlight_data)
        }}
        color={(selected: string) =>
          selected === CLIMATE_CHARTS.temperature
            ? 'deep-orange'
            : selected === CLIMATE_CHARTS.precipitation
            ? 'blue'
            : 'deep-purple'
        }></SimpleTabbedContent>
    )
  }
  public render() {
    const location = view_module.codex.location
    const { conditions, visible, icon } = location__conditions(location)
    const { sun, moon, day, night, rain_chance } = conditions
    const is_day = visible.sun
    const current = is_day ? day : night
    const { heat, wind } = current
    const sunrise = format_hours(sun.rise)
    const sunset = format_hours(sun.set)
    const moonrise = format_hours(moon.rise)
    const moonset = format_hours(moon.set)
    const precipitation = heat.degrees <= freezing_point ? 'snow' : 'rain'
    return (
      <div>
        <Row align='end' justify='center'>
          <Column cols={2}>
            <Icon size={50} color={css_colors.primary} class='pa-2'>
              mdi-weather-{icon}
            </Icon>
          </Column>
          <Column cols={4}>
            <SectionList
              list={[
                [
                  'Conditions',
                  <StyledText
                    text={`${decorate_text({
                      label: current.conditions,
                      tooltip: `${formatters.percent({
                        value: rain_chance
                      })} chance of ${precipitation}`
                    })}`}></StyledText>
                ],
                [
                  'Temperature',
                  <StyledText
                    text={decorate_text({
                      label: heat.desc,
                      tooltip: `${heat.degrees.toFixed(0)}°F`
                    })}></StyledText>
                ],
                [
                  'Wind Speed',
                  <StyledText
                    text={decorate_text({
                      label: wind.desc,
                      tooltip: `${wind.speed.toFixed(0)} mph`
                    })}></StyledText>
                ]
              ]}></SectionList>
          </Column>
          <Column cols={4}>
            <SectionList
              list={[
                ['Clouds', current.clouds],
                [
                  'Sunlight',
                  <span class={visible_css(visible.sun)}>{`${sunrise} - ${sunset}`}</span>
                ],
                [
                  'Moonlight',
                  <span class={visible_css(visible.moon)}>{`${moonrise} - ${moonset}`}</span>
                ]
              ]}></SectionList>
          </Column>
          <Column cols={2}>
            {
              <Icon size={50} color={css_colors.primary} class='pb-2'>
                mdi-moon-{moon.icon}
              </Icon>
            }
          </Column>
        </Row>
        <Row class='mt-5'>{this.climate}</Row>
      </div>
    )
  }
}
