import Weather from '@/components/descriptive/codex/locations/Weather'
import TraitDisplay from '@/components/common/Traits'
import { Badge } from '@/components/common/vuetify/badge'
import { Column, Row } from '@/components/common/vuetify/layout'
import { TabProps } from '@/components/common/vuetify/tabs'
import { actor__location } from '@/models/npcs/actors'
import { location__is_settlement } from '@/models/regions/locations'
import { location__terrain } from '@/models/regions/locations/environment'
import { location__conditions } from '@/models/regions/locations/environment/conditions'
import { location__threads } from '@/models/regions/locations/spawn/threads'
import { location__trait_colors } from '@/models/regions/locations/spawn/traits'
import { thread__collect } from '@/models/threads'
import { decorate_text } from '@/models/utilities/text/decoration'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import SectionList from '../../../common/text/SectionList'
import StyledText from '../../../common/text/StyledText'
import CodexPage from '..'
import Society from './society'
import ThreadLists from './threads'
import Travel from './traveling/Travel'

@Component({
  components: {
    CodexPage,
    Weather,
    Society,
    Travel,
    ThreadLists
  }
})
export default class LocationView extends tsx.Component<Record<string, unknown>> {
  private get location() {
    return view_module.codex.location
  }
  private get general() {
    const loc = this.location
    const province = window.world.provinces[loc.province]
    const { key } = location__terrain(loc)
    const climate = window.world.regions[province.region].climate
    return (
      <Row wrap>
        <Column cols={6}>
          <SectionList
            list={[
              [
                'Terrain',
                <StyledText
                  text={decorate_text({
                    label: key,
                    tooltip: climate
                  })}></StyledText>
              ]
            ]}></SectionList>
        </Column>
        {loc.traits.length > 0 && (
          <Column cols={12}>
            <TraitDisplay traits={loc.traits} colors={location__trait_colors}></TraitDisplay>
          </Column>
        )}
      </Row>
    )
  }
  public render() {
    const loc = view_module.codex.location
    const settlement = location__is_settlement(loc)
    const { icon } = location__conditions(loc)
    const tabs: Record<string, TabProps> = {
      Weather: {
        element: <Weather></Weather>,
        title: (
          <Badge color='primary' icon={`mdi-weather-${icon}`}>
            Weather
          </Badge>
        )
      }
    }
    if (settlement) {
      tabs.Society = { element: <Society></Society> }
      const { avatar } = view_module
      if (avatar && actor__location(avatar) === loc) {
        const available = location__threads({ loc, avatar })
        const { active, closed } = thread__collect(avatar)
        tabs.Quests = {
          element: (
            <ThreadLists
              available={available}
              active={active.sort((a, b) => {
                const a_loc = a.location === avatar.location.curr ? 1 : 0
                const b_loc = b.location === avatar.location.curr ? 1 : 0
                return b_loc - a_loc
              })}
              closed={closed}></ThreadLists>
          )
        }
      }
    }
    tabs.Travel = { element: <Travel></Travel> }
    const province = window.world.provinces[loc.province]
    return (
      <CodexPage
        title={loc.name}
        subtitle={
          <StyledText
            color={css_colors.subtitle}
            text={`(${loc.idx}) ${loc.subtype ?? loc.type}, ${decorate_text({
              link: window.world.regions[province.curr_nation]
            })}`}></StyledText>
        }
        content={this.general}
        tabs={tabs}></CodexPage>
    )
  }
}
