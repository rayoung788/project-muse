import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { SimpleTabbedContent } from '../../../../common/tabs/button_group'
import Cultures from './Cultures'
import Markets from './Markets'
import Occupations from './Occupations'

@Component({
  components: {
    SimpleTabbedContent,
    Cultures,
    Occupations,
    Markets
  }
})
export default class Society extends tsx.Component<Record<string, never>> {
  public render() {
    return (
      <SimpleTabbedContent
        content={{
          cultures: <Cultures></Cultures>,
          occupations: <Occupations></Occupations>,
          economy: <Markets></Markets>
        }}></SimpleTabbedContent>
    )
  }
}
