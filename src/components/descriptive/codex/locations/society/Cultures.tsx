import {
  PieChart,
  pie_chart__construct,
  pie_chart__percent_tooltips
} from '@/components/common/charts/pie/PieChart'
import { PieData } from '@/components/common/charts/pie/types'
import { species__by_culture } from '@/models/npcs/species/humanoids/taxonomy'
import { location__demographics } from '@/models/regions/locations/actors/demographics'
import { Loc } from '@/models/regions/locations/types'
import { view_module } from '@/store/view'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

const cultural_demographic = (loc: Loc): PieData[] => {
  const { common_cultures } = location__demographics(loc)
  const cultures = Object.entries(common_cultures).sort((a, b) => b[1] - a[1])
  const other = cultures
    .map(([, percent]) => percent)
    .filter(percent => percent < 0.01)
    .reduce((total, percent) => total + percent, 0)
  const other_demo: [string, number] = ['-1', other]
  return cultures
    .filter(([, percent]) => percent >= 0.01)
    .concat([other_demo])
    .map(([culture, percent]) => {
      const details = window.world.cultures[parseInt(culture)]
      return {
        label: details ? `${details.name} (${species__by_culture(details).name})` : 'Other',
        value: percent,
        color: details?.display || 'black'
      }
    })
}

type OnClickAction = (name: string) => void

@Component({
  components: {
    PieChart
  }
})
export class DemographicPie extends tsx.Component<{
  action: OnClickAction
  chart_data: PieData[]
}> {
  @Prop() private action?: OnClickAction
  @Prop() private chart_data: PieData[]
  public render() {
    return (
      <PieChart
        height={200}
        class='chart'
        chartData={pie_chart__construct(this.chart_data)}
        options={{
          tooltips: {
            callbacks: {
              label: pie_chart__percent_tooltips
            }
          },
          onClick: (_: Event, data: { _model: { label: string } }[]) => {
            const [d] = data
            const name = d?._model?.label?.replace(/ \(.*/, '')
            this.action?.(name)
          }
        }}
      />
    )
  }
}

@Component({
  components: {
    DemographicPie
  }
})
export default class Cultures extends tsx.Component<Record<string, unknown>> {
  private demographic_chart(params: { data: PieData[]; action?: OnClickAction }) {
    const { data, action } = params
    return <DemographicPie chart_data={data} action={action}></DemographicPie>
  }
  public render() {
    const loc = view_module.codex.location
    return this.demographic_chart({
      data: cultural_demographic(loc),
      action: name => {
        const culture = window.world.cultures.find(c => c.name === name)
        if (culture) view_module.update_codex({ target: culture })
      }
    })
  }
}
