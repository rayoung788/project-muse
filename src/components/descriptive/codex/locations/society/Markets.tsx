import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

import { Column, Container, Row } from '@/components/common/vuetify/layout'
import { DataTable } from '@/components/common/vuetify/table'
import { province__markets } from '@/models/regions/provinces/networks/trade_goods'
import { scarcity } from '@/models/utilities/quality'
import { proper_list, title_case } from '@/models/utilities/text'
import { formatters } from '@/models/utilities/text/formatters'
import { view_module } from '@/store/view'

import SectionList from '../../../../common/text/SectionList'

interface TradeRecord {
  name: string
  rarity: number
  demand: number
  price: number
}

const TradeTable = DataTable<
  TradeRecord,
  {
    'item.rarity': { item: TradeRecord }
    'item.demand': { item: TradeRecord }
    'item.price': { item: TradeRecord }
  }
>()
@Component({
  components: {
    TradeTable
  }
})
export default class Markets extends tsx.Component<Record<string, unknown>> {
  private rarity(value: number): keyof typeof scarcity {
    if (value < 0) return 'abundant'
    else if (value < 0.2) return 'common'
    else if (value < 0.5) return 'uncommon'
    else if (value < 1) return 'rare'
    return 'exceptional'
  }
  private demand(value: number) {
    if (value < 0) return 'Surplus'
    else if (value < 0.2) return 'Normal'
    else if (value < 0.35) return 'Popular'
    else if (value < 0.5) return 'Needed'
    return 'Desperate'
  }
  public render() {
    const province = window.world.provinces[view_module.codex.location.province]
    const trade_goods = province__markets(province)
    const items: TradeRecord[] = Object.entries(trade_goods).map(([k, v]) => {
      const price = v.rarity + v.demand
      return {
        name: `${k}${v.supply > 0 ? '**' : ''}`,
        rarity: v.rarity,
        demand: v.demand,
        price
      }
    })
    const headers = [
      { text: 'Trade Good', value: 'name' },
      { text: 'Supply', value: 'rarity' },
      { text: 'Demand', value: 'demand' },
      { text: 'Price', value: 'price' }
    ]
    const exports = Object.entries(trade_goods)
      .filter(([_, v]) => v.supply)
      .sort((a, b) => b[1].supply - a[1].supply)
      .map(([k], i) => (i === 0 ? k : k.toLowerCase()))
    const imports = Object.entries(trade_goods)
      .filter(([_, v]) => !v.supply)
      .sort((a, b) => b[1].demand - a[1].demand)
      .slice(0, 5)
      .map(([k], i) => (i === 0 ? k : k.toLowerCase()))
    return (
      <Container fluid>
        <Row>
          <SectionList
            class='ml-4 mb-4'
            list={[
              ['Imports', proper_list(imports, 'and')],
              ['Exports', proper_list(exports, 'and')]
            ]}></SectionList>
        </Row>
        <Row>
          <Column cols={12}>
            <TradeTable
              dense
              headers={headers}
              items={items}
              items-per-page={5}
              scopedSlots={{
                'item.rarity': ({ item }: { item: TradeRecord }) =>
                  `${formatters.percent({ value: item.rarity })} (${title_case(
                    this.rarity(item.rarity)
                  )})`,
                'item.demand': ({ item }: { item: TradeRecord }) =>
                  `${formatters.percent({ value: item.demand })} (${this.demand(item.demand)})`,
                'item.price': ({ item }: { item: TradeRecord }) =>
                  formatters.percent({ value: item.price })
              }}></TradeTable>
          </Column>
        </Row>
      </Container>
    )
  }
}
