import { Task } from '@/models/threads/types'
import { css_colors } from '@/styles/colors'
import { css } from '@emotion/css'

export const style__thread_panel = css`
  font-size: 0.88rem !important;
  background-color: #faf7ea !important;
  box-shadow: inset 0px 4px 8px -5px rgb(50 50 50 / 75%),
    inset 0px -4px 8px -5px rgb(50 50 50 / 75%);
  .v-label {
    font-size: 0.88rem !important;
  }
`
export const style__thread_failures = css`
  color: ${css_colors.primary};
`
export const style__disabled_thread = css`
  background-color: #e5e5e5 !important;
`

export const thread__icons: Record<Task['status'], { icon: string; color: string }> = {
  perfection: { icon: 'check-circle', color: css_colors.difficulty.easy },
  success: { icon: 'check-bold', color: css_colors.difficulty.easy },
  pyrrhic: { icon: 'close-thick', color: css_colors.difficulty.hard },
  failure: { icon: 'close-circle', color: css_colors.difficulty.hard },
  abandoned: { icon: 'close-circle', color: 'gray' },
  blocked: { icon: 'skull', color: 'gray' },
  fresh: { icon: 'alert-circle', color: css_colors.difficulty.medium },
  'in progress': { icon: 'exclamation-thick', color: css_colors.difficulty.medium },
  paused: { icon: 'pause-circle', color: 'gray' }
}
