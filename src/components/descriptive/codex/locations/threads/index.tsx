import { TabbedContent } from '@/components/common/tabs/button_group'
import TooltipText from '@/components/common/text/TooltipText'
import { Icon } from '@/components/common/vuetify/icon'
import { Row } from '@/components/common/vuetify/layout'
import DetailedTableRow, { DataTable, TableHeaders } from '@/components/common/vuetify/table'
import { actor__location } from '@/models/npcs/actors'
import { profession__title } from '@/models/npcs/actors/stats/professions'
import { difficulties } from '@/models/npcs/stats/difficulty'
import {
  thread__describe_complexity,
  thread__progress,
  thread__status,
  thread__task_odds
} from '@/models/threads'
import { thread__close } from '@/models/threads/actions'
import { thread__spawn_children } from '@/models/threads/spawn'
import { Thread } from '@/models/threads/types'
import { title_case } from '@/models/utilities/text'
import { decorate_text } from '@/models/utilities/text/decoration'
import { formatters } from '@/models/utilities/text/formatters'
import { view_module } from '@/store/view'
import { style__clickable } from '@/styles'
import { css_colors } from '@/styles/colors'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import StyledText from '../../../../common/text/StyledText'
import AdventuringParty from './Adventurers'
import { style__disabled_thread, style__thread_failures, thread__icons } from './styles'
import ThreadDetails from './ThreadDetails'

export const ThreadTable = DataTable<
  Thread,
  {
    'item.name': { item: Thread }
    'item.patron'?: { item: Thread }
    'item.location'?: { item: Thread }
    'item.difficulty': { item: Thread }
    'item.complexity': { item: Thread }
    'item.status': { item: Thread }
    'item.actions': { item: Thread }
  }
>()
const items_per_page = 6

const selection = ['available', 'active', 'closed', 'party'] as const

@Component({
  components: {
    ThreadTable,
    ThreadDetails,
    AdventuringParty
  }
})
export default class ThreadLists extends tsx.Component<{
  active: Thread[]
  closed: Thread[]
  available: Thread[]
}> {
  @Prop() private active: Thread[]
  @Prop() private closed: Thread[]
  @Prop() private available: Thread[]
  private page = 1
  private expanded: Thread[] = []
  private selected = 'active'
  private find_page(params: { idx: number; threads: Thread[] }) {
    const { idx, threads } = params
    const i = threads.findIndex(thread => thread.idx === idx)
    return Math.floor(i / items_per_page) + 1
  }
  private go_to_thread(thread: Thread) {
    const threads = thread.closed ? this.closed : this.active
    this.page = this.find_page({ idx: thread.idx, threads })
    this.expanded = [thread]
    const thread_type = thread.closed ? 'closed' : 'active'
    if (this.selected !== thread_type) this.selected = thread_type
  }
  public render() {
    return (
      <TabbedContent
        class='pa-0 pt-2'
        selection={[...selection]}
        selected={this.selected}
        on={{ 'update:selected': (val: string) => (this.selected = val) }}
        content={(_selected: string) => {
          const selected = _selected as typeof selection[number]
          if (selected === 'party') return <AdventuringParty></AdventuringParty>
          const threads =
            selected === 'active'
              ? this.active
              : selected === 'closed'
              ? this.closed
              : this.available
          const available = selected === 'available'
          return (
            <ThreadTable
              class='mt-3'
              headers={
                [
                  { text: '', value: 'status', sortable: false },
                  { text: 'Quest', value: 'name', sortable: false },
                  { text: 'Patron', value: 'patron', sortable: false },
                  !available && { text: 'Location', value: 'location', sortable: false },
                  !available && { text: 'Progress', value: 'complexity', sortable: false },
                  { text: 'Difficulty', value: 'difficulty', sortable: true },
                  available && { text: 'Actions', value: 'actions', sortable: false },
                  { text: '', value: 'data-table-expand' }
                ].filter(s => s) as TableHeaders[]
              }
              dense
              items={threads}
              items-per-page={items_per_page}
              hide-default-footer={threads.length <= items_per_page}
              item-class={item =>
                selected === 'active' && item.location !== actor__location(view_module.avatar).idx
                  ? style__disabled_thread
                  : undefined
              }
              item-key='idx'
              page={this.page}
              expanded={this.expanded}
              single-expand
              on={{
                'update:page': (v: number) => (this.page = v),
                'update:expanded': (v: Thread[]) => {
                  if (selected === 'active') {
                    this.expanded = v
                    this.expanded.forEach(thread =>
                      thread__spawn_children({
                        thread,
                        avatar: view_module.avatar
                      })
                    )
                  }
                }
              }}
              show-expand={!available}
              scopedSlots={{
                'item.status': ({ item }) => {
                  const { status } = thread__progress({ thread: item, avatar: view_module.avatar })
                  const { icon, color } = thread__icons[status]
                  return <Icon color={color}>mdi-{icon}</Icon>
                },
                'item.name': ({ item }) => {
                  const parent = window.world.threads[item.parent]
                  return (
                    <DetailedTableRow
                      title={
                        <span>
                          {title_case(item.goal)} (#{item.idx})
                        </span>
                      }
                      subtitle={
                        <span>
                          {item.hook}
                          {parent ? (
                            <span>
                              {' '}
                              (
                              <span
                                class={style__clickable(css_colors.subtitle)}
                                onClick={() => this.go_to_thread(parent)}>
                                #{parent.idx}
                              </span>
                              )
                            </span>
                          ) : (
                            ''
                          )}
                        </span>
                      }></DetailedTableRow>
                  )
                },
                'item.patron': ({ item }) => {
                  const patron = window.world.actors[item.patron]
                  return (
                    <DetailedTableRow
                      title={
                        <StyledText
                          text={decorate_text({
                            link: patron
                          })}></StyledText>
                      }
                      subtitle={
                        <StyledText
                          color={css_colors.subtitle}
                          text={profession__title({
                            actor: patron
                          }).toLocaleLowerCase()}></StyledText>
                      }
                      link></DetailedTableRow>
                  )
                },
                'item.location': ({ item }) => {
                  const loc = window.world.locations[item.location]
                  return (
                    <DetailedTableRow
                      title={
                        <StyledText
                          text={decorate_text({
                            link: loc
                          })}></StyledText>
                      }
                      subtitle={loc.type}
                      link></DetailedTableRow>
                  )
                },
                'item.difficulty': ({ item }) => {
                  const desc = thread__describe_complexity(item)
                  const { odds, tier } = thread__task_odds({
                    difficulty: item.difficulty,
                    actor: view_module.avatar
                  })
                  return (
                    <DetailedTableRow
                      title={
                        <StyledText
                          text={decorate_text({
                            label: title_case(tier),
                            color: difficulties[tier].color,
                            tooltip: `${formatters.percent({ value: odds, precision: 2 })}`
                          })}></StyledText>
                      }
                      subtitle={desc}
                      link></DetailedTableRow>
                  )
                },
                'item.complexity': ({ item }) => {
                  const desc =
                    selected === 'closed'
                      ? thread__progress({ thread: item, avatar: view_module.avatar }).status
                      : thread__status(item)
                  return (
                    <DetailedTableRow
                      title={`${item.progress}/${item.complexity}`}
                      subtitle={
                        <span>
                          {desc} <span class={style__thread_failures}>({item.failures})</span>
                        </span>
                      }></DetailedTableRow>
                  )
                },
                'item.actions': ({ item }) => {
                  return (
                    <Row>
                      <TooltipText
                        link
                        tooltip={<span>Accept</span>}
                        text={
                          <Icon
                            color={css_colors.difficulty.easy}
                            class='ml-1'
                            onClick={() => {
                              view_module.avatar.threads = [...view_module.avatar.threads, item.idx]
                              view_module.codex.location.threads =
                                view_module.codex.location.threads.filter(idx => idx !== item.idx)
                            }}>
                            mdi-check-circle-outline
                          </Icon>
                        }></TooltipText>
                      <TooltipText
                        link
                        tooltip={<span>Decline</span>}
                        text={
                          <Icon
                            color='primary'
                            class='ml-1'
                            onClick={() =>
                              thread__close({
                                thread: item,
                                ref: view_module.codex.location,
                                avatar: view_module.avatar
                              })
                            }>
                            mdi-close-circle-outline
                          </Icon>
                        }></TooltipText>
                    </Row>
                  )
                },
                'expanded-item': ({
                  item: thread,
                  headers
                }: {
                  item: Thread
                  headers: string[]
                }) => {
                  return (
                    <ThreadDetails
                      thread={thread}
                      headers={headers}
                      go_to_thread={this.go_to_thread}></ThreadDetails>
                  )
                }
              }}
            />
          )
        }}></TabbedContent>
    )
  }
}
