import { Component, Prop } from 'vue-property-decorator'
import StyledText from '../../../../common/text/StyledText'
import * as tsx from 'vue-tsx-support'
import { DataTable } from '@/components/common/vuetify/table'
import { npc__is_actor } from '@/models/npcs/actors'
import NPCHeathView from '../../npcs/actors/stats/Health'
import { decorate_text } from '@/models/utilities/text/decoration'
import { NPC } from '@/models/npcs/types'
import { npc__health } from '@/models/npcs/stats'
import { Column, Container, Row } from '@/components/common/vuetify/layout'
import { species__by_culture } from '@/models/npcs/species/humanoids/taxonomy'
import { species__size_rank } from '@/models/npcs/species/size'
import { title_case } from '@/models/utilities/text'
import { actor__details } from '@/models/utilities/text/entities/actor'
import { css } from '@emotion/css'

interface EncounterSlots {
  'item.name': { item: NPC }
}

const EncounterActorTable = DataTable<NPC, EncounterSlots>()

const items_per_page = 5

const table_style = css`
  table tbody tr:nth-child(odd) {
    background-color: #f1e5d6 !important;
  }
  .v-data-footer,
  th,
  table tbody tr:nth-child(even) {
    background-color: #faf7ea !important;
  }
  .v-data-footer__select {
    display: none;
  }
`

const info_block = (npc: NPC) => {
  const { type, idx, genus, size } = npc.species
  if (type === 'humanoid' && npc__is_actor(npc)) {
    const culture = window.world.cultures[npc.culture]
    const species = species__by_culture(culture)
    const spec = ` !@!@!@!@!@!@!`
    return `${species__size_rank(species.size).toLowerCase()} humanoid (${actor__details.species({
      actor: npc
    })}${spec})`
  } else if (type === 'beast') {
    const species = window.world.beasts[idx]
    return `${species__size_rank(species.size).toLowerCase()} beast (${decorate_text({
      label: species.family,
      link: species,
      tooltip: species.name
    })})`
  } else if (type === 'spirit') {
    return `${size} spirit (${genus})`
  }
  return ''
}

export const npc_name = (npc: NPC) => {
  const { type, idx, family } = npc.species
  if (type === 'humanoid' && npc__is_actor(npc)) {
    return decorate_text({
      label: npc.alias ?? npc.name,
      link: npc
    })
  } else if (type === 'beast') {
    const species = window.world.beasts[idx]
    return title_case(species.genus.name)
  } else if (type === 'spirit') {
    return family
  }
  return ''
}

@Component({
  components: {
    EncounterActorTable,
    NPCHeathView
  }
})
export default class EncounterTable extends tsx.Component<{ npcs: NPC[] }> {
  @Prop() private npcs: NPC[]
  public render() {
    const headers = [{ text: 'name', value: 'name', sortable: false }]
    return (
      <EncounterActorTable
        dense
        class={table_style}
        items={this.npcs}
        headers={headers}
        items-per-page={items_per_page}
        hide-default-footer={this.npcs.length <= items_per_page}
        hide-default-header
        scopedSlots={{
          'item.name': ({ item }) => {
            const name = npc_name(item)
            const health = npc__health(item)
            return (
              <Container class='ma-0'>
                <Row class='pt-1'>
                  <Column class='pa-0'>
                    <StyledText text={`${name}`} />
                    <sup class='px-1'>{item.level.toFixed(2)}</sup>
                  </Column>
                  <Column class='pa-0 text-right'>
                    <span class='text-caption'>
                      <NPCHeathView
                        max={health.max}
                        current={health.current}
                        percent={health.percent}></NPCHeathView>
                    </span>
                  </Column>
                </Row>
                <Row class='pt-3'>
                  <span class='text-caption'>
                    <StyledText text={info_block(item)}></StyledText>
                  </span>
                </Row>
              </Container>
            )
          }
        }}></EncounterActorTable>
    )
  }
}
