import PrimaryButton from '@/components/common/PrimaryButton'
import { Icon } from '@/components/common/vuetify/icon'
import { RadioButton, RadioGroup } from '@/components/common/vuetify/input'
import { Column, Divider, Row } from '@/components/common/vuetify/layout'
import { actor__location } from '@/models/npcs/actors'
import { difficulties } from '@/models/npcs/stats/difficulty'
import {
  task__in_progress,
  thread__progress,
  thread__status,
  thread__tasks,
  thread__task_odds
} from '@/models/threads'
import { thread__advance, thread__close, thread__exp, thread__fork } from '@/models/threads/actions'
import { thread__spawn_children } from '@/models/threads/spawn'
import { Task, Thread } from '@/models/threads/types'
import { describe_duration } from '@/models/utilities/math/time'
import { title_case } from '@/models/utilities/text'
import { decorate_text } from '@/models/utilities/text/decoration'
import { formatters } from '@/models/utilities/text/formatters'
import { view_module } from '@/store/view'
import { style__clickable } from '@/styles'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import SectionList from '../../../../common/text/SectionList'
import StyledText from '../../../../common/text/StyledText'
import { style__thread_panel, thread__icons } from './styles'

const thread_xp = (xp: number) => `${(xp * 1000).toFixed(0)} xp`

@Component
export default class ThreadDetails extends tsx.Component<{
  thread: Thread
  headers: string[]
  go_to_thread: (thread: Thread) => void
}> {
  @Prop() private thread: Thread
  @Prop() private headers: string[]
  @Prop() private go_to_thread: (thread: Thread) => void
  private selected_fork = -1
  private close_thread(thread: Thread) {
    const { avatar } = view_module
    thread__close({ thread, ref: avatar, avatar })
    const parent = window.world.threads[thread.parent]
    if (parent) this.go_to_thread(parent)
  }
  private describe_task(task: Task) {
    const { odds, tier } = thread__task_odds({
      difficulty: task.difficulty,
      actor: view_module.avatar
    })
    const ref = window.world.threads[task.thread]
    return (
      <Row wrap>
        <Column cols={12} class='pb-0'>
          {ref ? (
            <span>
              <i>{title_case(ref.goal)}</i> (
              <span class={style__clickable()} onClick={() => this.go_to_thread(ref)}>
                #{ref.idx}
              </span>
              )
            </span>
          ) : (
            <i>{title_case(task.goal)}</i>
          )}
          <StyledText
            text={` (${decorate_text({
              label: formatters.percent({
                value: odds,
                precision: 2
              }),
              color: difficulties[tier].color
            })}): `}></StyledText>
          <StyledText text={task.text}></StyledText>
        </Column>
        <Column cols={12} class={task.exp !== undefined ? 'py-0' : 'pt-0'}>
          <i>Duration:</i>
          {` ${describe_duration(task.duration)}.`}
        </Column>
        {task.exp !== undefined && (
          <Column cols={12} class='pt-0'>
            <i>Rewards:</i>
            {` ${thread_xp(task.exp)}.`}
          </Column>
        )}
      </Row>
    )
  }
  public render() {
    const { avatar } = view_module
    const thread = this.thread
    const { goal, tasks, fork, closed } = thread
    const has_tasks = tasks.length > 0
    const { completed, failed, status } = thread__progress({ thread, avatar })
    const ended = failed || completed
    const core_tasks = thread__tasks({ tasks, avatar })
    const forked_tasks = thread__tasks({ tasks: fork?.tasks ?? [], avatar })
    const no_fork = fork && !fork?.tasks[this.selected_fork]
    const latest_task = core_tasks.find(task__in_progress)
    const child_required = latest_task?.thread !== undefined
    const loc = window.world.locations[thread.location]
    const avatar_at_loc = view_module.avatar && actor__location(view_module.avatar) === loc
    return (
      <td class={`py-2 ${style__thread_panel}`} colspan={this.headers.length}>
        <Row class='ma-2'>
          <SectionList
            list={[
              [
                'Goal',
                <span>
                  <i>{title_case(goal)}. </i>
                  {thread.text}
                </span>
              ]
            ]}></SectionList>
        </Row>
        {has_tasks && <Divider></Divider>}
        {has_tasks &&
          core_tasks.map(task => {
            const { tier } = thread__task_odds({
              difficulty: task.difficulty,
              actor: avatar
            })
            const { icon, color } = thread__icons[tier === 'insanity' ? 'blocked' : task.status]
            return (
              <Row class='ma-1' align='center'>
                <Column cols={1} class='pa-1'>
                  <Icon color={color}>mdi-{icon}</Icon>
                </Column>
                <Column cols={11} class='pa-1'>
                  {this.describe_task(task)}
                </Column>
              </Row>
            )
          })}
        {fork && <Divider></Divider>}
        {fork && (
          <Row class='mt-3 mx-3 mb-1'>
            <SectionList list={[['Fork', <span>{fork.text}</span>]]}></SectionList>
          </Row>
        )}
        {fork && (
          <Row class='ma-1'>
            <RadioGroup v-model={this.selected_fork}>
              <Row class='pl-4'>
                {forked_tasks.map((task, i) => {
                  return (
                    <Column cols={12} class='pa-1'>
                      <RadioButton
                        color='primary'
                        key={i}
                        label={i.toString()}
                        scopedSlots={{
                          label: () => this.describe_task(task)
                        }}
                      />
                    </Column>
                  )
                })}
              </Row>
            </RadioGroup>
          </Row>
        )}
        {ended && <Divider></Divider>}
        {ended && (
          <Row class='ma-3'>
            <SectionList
              list={[
                ['Outcome', <span>Quest {failed ? 'failed' : 'completed'}.</span>],
                [
                  'Rewards',
                  <span>
                    {thread_xp(
                      thread.exp ??
                        thread__exp({
                          status: thread__status(thread),
                          difficulty: thread.difficulty.cr,
                          complexity: thread.complexity,
                          avatar
                        })
                    )}
                    .
                  </span>
                ]
              ]}></SectionList>
          </Row>
        )}
        {!closed && <Divider></Divider>}
        {!closed && (
          <Row class='ma-1'>
            <PrimaryButton
              text='Continue'
              disabled={no_fork || child_required || !avatar_at_loc || status === 'blocked'}
              click={() => {
                ended
                  ? this.close_thread(thread)
                  : fork
                  ? thread__fork({
                      thread,
                      decision: forked_tasks[this.selected_fork],
                      ref: view_module.avatar,
                      avatar
                    })
                  : thread__advance({ thread, ref: view_module.avatar, avatar })
                thread__spawn_children({ thread, avatar })
                this.selected_fork = -1
              }}
              class='ma-2'></PrimaryButton>
            <PrimaryButton
              text='Abandon'
              disabled={ended}
              click={() => this.close_thread(thread)}
              class='ma-2'></PrimaryButton>
          </Row>
        )}
      </td>
    )
  }
}
