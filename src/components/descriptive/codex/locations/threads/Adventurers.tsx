import { Column, Row } from '@/components/common/vuetify/layout'
import { actor__relation } from '@/models/npcs/actors'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import ActorTable, { ActorHeaders, construct_actor_record } from '../../npcs/actors/ActorTable'

const headers: ActorHeaders = [
  { text: 'Name', value: 'name' },
  { text: 'Age', value: 'age' },
  { text: 'Culture', value: 'culture' },
  { text: 'Health', value: 'level' }
]

@Component({
  components: {
    ActorTable
  }
})
export default class AdventuringParty extends tsx.Component<Record<string, unknown>> {
  public render() {
    const actor = view_module.avatar
    const party = actor__relation({ actor, type: 'party' }).map(actor =>
      construct_actor_record(actor)
    )
    return (
      <Row>
        <Column cols={12} v-show={party.length > 0}>
          <ActorTable headers={headers} actors={party}></ActorTable>
        </Column>
      </Row>
    )
  }
}
