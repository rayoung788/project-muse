import { TabbedContent } from '@/components/common/tabs/button_group'
import { Icon } from '@/components/common/vuetify/icon'
import { Column, Container, Row } from '@/components/common/vuetify/layout'
import DetailedTableRow, { DataTable } from '@/components/common/vuetify/table'
import { actor__location } from '@/models/npcs/actors'
import { location__travel } from '@/models/regions/locations'
import { location__templates } from '@/models/regions/locations/spawn/taxonomy'
import { LocationTemplate } from '@/models/regions/locations/spawn/taxonomy/types'
import { province__neighborhood } from '@/models/regions/provinces'
import { deconstruct_hours } from '@/models/utilities/math/time'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import Wait from './Wait'

interface Destination {
  idx: number
  name: string
  type: string
  hours: number
  miles: string
  time: { minutes: number; hours: number; days: number }
}

const items_per_page = 5

const TravelBaseTable = DataTable<
  Destination,
  {
    'item.name': { item: Destination }
    'item.hours': { item: Destination }
    'item.travel': { item: Destination }
  }
>()
@Component({
  components: {
    TravelBaseTable,
    Wait
  }
})
export default class Travel extends tsx.Component<Record<string, unknown>> {
  public render() {
    const avatar_loc = view_module.avatar ? actor__location(view_module.avatar) : null
    const src = view_module.codex.location
    const province = window.world.provinces[src.province]
    return (
      <Container fluid>
        <Row justify='center'>
          <Column>
            <TabbedContent
              selection={['settlement', 'wilderness']}
              content={(_selected: string) => {
                const selected = _selected as LocationTemplate['group']
                const destinations = province__neighborhood(province).filter(
                  n => n !== src && location__templates[n.type].group === selected
                )
                return (
                  <TravelBaseTable
                    headers={[
                      { text: 'Location', value: 'name' },
                      { text: 'Distance', value: 'hours' },
                      { text: 'Travel', value: 'travel', sortable: false }
                    ]}
                    items-per-page={items_per_page}
                    hide-default-footer={destinations.length < items_per_page}
                    items={destinations
                      .map(dst => {
                        const { idx, name, type, subtype } = dst
                        const { miles, hours } = location__travel({ src, dst })
                        return {
                          idx,
                          name,
                          type: subtype ?? type,
                          time: deconstruct_hours(hours),
                          miles: `${miles.toFixed(1)} miles`,
                          hours
                        }
                      })
                      .sort((a, b) => a.hours - b.hours)}
                    dense
                    scopedSlots={{
                      'item.name': ({ item }) => (
                        <DetailedTableRow title={item.name} subtitle={item.type}></DetailedTableRow>
                      ),
                      'item.hours': ({ item }) => (
                        <DetailedTableRow
                          title={`${item.time.days.toFixed(0)} day(s), ${item.time.hours.toFixed(
                            0
                          )} hour(s), ${item.time.minutes.toFixed(0)} minute(s)`}
                          subtitle={item.miles}></DetailedTableRow>
                      ),
                      'item.travel': ({ item }) => {
                        const dest = window.world.locations[item.idx]
                        return (
                          <Icon
                            small
                            color={css_colors.primary}
                            disabled={avatar_loc !== src}
                            onClick={() =>
                              view_module.update_avatar_location({ dest, hours: item.hours })
                            }>
                            mdi-update
                          </Icon>
                        )
                      }
                    }}></TravelBaseTable>
                )
              }}></TabbedContent>
          </Column>
        </Row>
        <Row>
          <Wait></Wait>
        </Row>
      </Container>
    )
  }
}
