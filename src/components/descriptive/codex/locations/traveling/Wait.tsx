import PrimaryButton from '@/components/common/PrimaryButton'
import { TextField } from '@/components/common/vuetify/input'
import { Column, Row } from '@/components/common/vuetify/layout'
import { day_ms, hour_ms, minute_ms, year_ms } from '@/models/utilities/math/time'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

@Component
export default class Wait extends tsx.Component<Record<string, unknown>> {
  public minutes = 0
  public hours = 0
  public days = 0
  public years = 0
  public render() {
    return (
      <Row justify='center' align='center' class='text-center'>
        <Column cols={2} class='mt-5'>
          <TextField v-model={this.years} type='number' label='years:' />
        </Column>
        <Column cols={2} class='mt-5'>
          <TextField v-model={this.days} type='number' label='days:' />
        </Column>
        <Column cols={2} class='mt-5'>
          <TextField v-model={this.hours} type='number' label='hours:' />
        </Column>
        <Column cols={2} class='mt-5'>
          <TextField v-model={this.minutes} type='number' label='minutes:' />
        </Column>
        <Column cols={1} class='mt-5'>
          <PrimaryButton
            text='Wait'
            click={() => {
              view_module.tick(
                this.minutes * minute_ms +
                  this.hours * hour_ms +
                  this.days * day_ms +
                  this.years * year_ms
              )
            }}
          />
        </Column>
      </Row>
    )
  }
}
