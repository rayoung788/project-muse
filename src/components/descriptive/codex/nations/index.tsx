import Geography from '@/components/descriptive/codex/nations/Geography'
import History from '@/components/descriptive/codex/nations/history'
import SectionList from '@/components/common/text/SectionList'
import { region__war_rivals } from '@/models/regions/diplomacy/relations'
import { region__imperial_name } from '@/models/regions/diplomacy/status'
import { decorated_culture } from '@/models/npcs/species/humanoids/cultures'
import { region__population } from '@/models/regions'
import { location__is_village } from '@/models/regions/locations/spawn/taxonomy/settlements'
import { province__hub } from '@/models/regions/provinces'
import { decorate_text } from '@/models/utilities/text/decoration'
import { formatters } from '@/models/utilities/text/formatters'
import { climate_lookup } from '@/models/world/climate/types'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import StyledText from '../../../common/text/StyledText'
import { Column, Row } from '../../../common/vuetify/layout'
import CodexPage from '..'
import Ecology from './Ecology'
import Politics from './politics'

@Component({
  components: {
    CodexPage,
    History,
    Geography,
    Ecology,
    Politics
  }
})
export default class NationView extends tsx.Component<Record<string, never>> {
  private get general() {
    const nation = view_module.codex.nation
    const { regions, religion } = nation
    const rebellions = regions
      .map(p => {
        const province = window.world.provinces[p]
        return window.world.regions[province.region]
      })
      .filter(region => region.rebellions.current !== -1)
    const ruling = window.world.cultures[nation.culture.ruling]
    const native = window.world.cultures[nation.culture.native]
    const current_wars = region__war_rivals(nation).map(rival => {
      const war = rival.wars.current
        .map(i => window.world.wars[i])
        .find(w => {
          return w.invader.idx === nation.idx || w.defender.idx === nation.idx
        })
      return decorate_text({
        link: war,
        label: rival.name,
        tooltip: `${war.name}`
      })
    })
    const current_rebellions = rebellions.map(subject => {
      const rebellion = window.world.rebellions[subject.rebellions.current]
      return decorate_text({
        label: subject.name,
        link: rebellion,
        tooltip: rebellion.name
      })
    })
    const conflicts = current_wars.concat(current_rebellions)
    const total_pop = region__population(nation)
    const urban_pop = nation.provinces
      .map(i => province__hub(window.world.provinces[i]))
      .filter(hub => !location__is_village(hub))
      .reduce((sum, hub) => sum + hub.population, 0)

    return (
      <Row>
        <Column cols={6}>
          <SectionList
            list={[
              [
                `Culture`,
                <StyledText
                  text={`${decorated_culture({ culture: ruling, title: true })}${
                    ruling !== native
                      ? `, ${decorated_culture({
                          culture: native,
                          title: true,
                          color: css_colors.subtitle
                        })}`
                      : ''
                  }`}></StyledText>
              ],
              [
                'Religion',
                <StyledText
                  text={decorate_text({
                    link: window.world.religions[religion.state]
                  })}></StyledText>
              ]
            ]}></SectionList>
        </Column>
        <Column cols={6}>
          <SectionList
            list={[
              [
                `Population`,
                `${formatters.compact(total_pop)} (${((urban_pop / total_pop) * 100).toFixed(
                  0
                )}% Urban)`
              ],
              [
                `Conflicts`,
                <StyledText
                  text={conflicts.length > 0 ? conflicts.join(', ') : 'None'}></StyledText>
              ]
            ]}></SectionList>
        </Column>
      </Row>
    )
  }
  public render() {
    const nation = view_module.codex.nation
    const climate = climate_lookup[nation.climate]
    const overlord = window.world.regions[nation.overlord.idx]
    return (
      <CodexPage
        title={region__imperial_name(nation)}
        subtitle={
          <StyledText
            color={css_colors.subtitle}
            text={`(${nation.idx}) ${
              overlord
                ? decorate_text({
                    label: 'Vassal',
                    link: overlord,
                    tooltip: overlord?.name ?? undefined,
                    color: css_colors.subtitle
                  })
                : 'Nation'
            } (${climate.zone.toLowerCase()}, ${nation.development})`}></StyledText>
        }
        content={this.general}
        tabs={{
          Geography: { element: <Geography></Geography> },
          Ecology: { element: <Ecology></Ecology> },
          Politics: { element: <Politics></Politics> },
          History: { element: <History></History> }
        }}></CodexPage>
    )
  }
}
