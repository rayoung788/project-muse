import { Column, Row } from '@/components/common/vuetify/layout'
import { DataTable } from '@/components/common/vuetify/table'
import { Region } from '@/models/regions/types'
import { title_case } from '@/models/utilities/text'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

const government_structure: Record<Region['government']['structure'], string> = {
  autocratic:
    'With established discrete borders and efficient taxation, the central executive is strong enough to overpower all other social actors.',
  republic:
    'With established discrete borders and efficient taxation, the central executive and citizens have reached a compromise: citizens are taxed but have government representatives who decide how taxes are used.',
  oligarchic:
    'With established discrete borders and efficient taxation, the central executive is overpowered by citizens with money.',
  confederation:
    'The executive is unable to organize centrally and is divided into regional lordships, each with the ability to ignore central decrees.',
  autonomous:
    'Regions essentially have their own governments, with optional loyalty to the central seat, who holds primarily symbolic power.'
}
const government_succession: Record<Region['government']['succession'], string> = {
  hereditary: 'The throne is inherited by members of the royal family.',
  elected: 'Elections are held to decide the next ruler.'
}
const government_entry: Record<Region['government']['entry'], string> = {
  merit:
    'Government offices are given to the citizen with the highest exam score regardless of upbringing.',
  hereditary: 'Government offices are inherited by the children of noble birth.',
  wealth: 'Government offices are sold by the executive authority to wealthy citizens.',
  eunuchs:
    'Government offices are restricted to eunuchs who are trained for their positions from birth.',
  assigned:
    'Top government offices are assigned by the executive based on whatever merits they choose.'
}

const rule_of_law: Record<Region['law']['rule_of_law'], string> = {
  ubiquitous: 'Nobody is above the law.',
  selective: 'Only the executive is above the law.',
  none: 'Law is selectively applied and rarely to those with power and money. If a judicial system exists, it is entirely unstable.'
}
const accountability: Record<Region['law']['accountability'], string> = {
  judicial:
    'Powerful citizens are held accountable through a system of courts. The powerless may appeal to the judiciary if wronged.',
  'top-down':
    'Powerful citizens are only accountable to those more powerful than them. The powerless can appeal to the top of the hierarchy, for whatever good that might do.',
  informal:
    'Powerful citizens are accountable to cultural mores, philosophies, religion, and taboo. The powerless have no means of appeal besides admonishment and disapproval.'
}
const magic: Record<Region['law']['magic'], string> = {
  banned: 'Spellcasting is banned except under strict supervision by the executive authority.',
  restricted: 'Only permitted schools of magic are allowed in an effort to keep civilians safe.',
  unrestricted: 'There are no spellcasting restrictions.'
}

const religion_authority: Record<Region['religion']['authority'], string> = {
  protest:
    'The popular religion works independently as a major destabilizing force against executive legitimacy.',
  theocratic:
    'The head of government is the head of the religion. The executive has full control over religious legitimacy and clerical appointments.',
  aristocratic:
    'The popular religion is run by aristocratic elites, independent of the government. Aristocrats bestow religious legitimacy and clerical appointments.',
  independent:
    'The popular religion acts independently of all other social actors. It bestows religious legitimacy and clerical appointments.'
}

const religion_tolerance: Record<Region['religion']['tolerance'], string> = {
  traditional:
    'It is considered morally wrong to worship any religion outside of the recognized state religion. Minority religions are suppressed.',
  selective:
    'There exists a group of "accepted" religions to which worship is permitted, but all others are considered "evil".',
  cosmopolitan: 'The worship of any religion is permitted and minority religions are protected.'
}

const military_type: Record<Region['military'][number], string> = {
  'slave army':
    'Soldiers are selected as children and trained to be loyal soldiers. Slave soldiers may hold high positions in society, with slave generals even becoming aristocracy.',
  'centralized draft':
    'Military regiments are drafted by whatever power the central authority has, united under the banner of centralized leadership.',
  'regional draft':
    'Military regiments are organized region-by-region, united in a segmented fashion with strong regional loyalties.',
  'tribal militias':
    'Military regiments consist of forces gathered from regional tribes. They are very independent and difficult for the executive to command.',
  mercenaries: 'Military regiments consist of expensive, but professional armies for hire.',
  'aristocratic mounted':
    'Military regiments consist of small units of professional aristocratic knights.'
}

const economic_management: Record<Region['economy']['management'], string> = {
  market: 'Resource allocation is driven by the collective actions of individuals and businesses.',
  mixed: 'Resource allocation is controlled or regulated by the government in some sectors.',
  planned: 'Resource allocation is mostly controlled by the government.',
  barter:
    'Only primitive resource allocation exists. Goods and services are exchanged without currency.'
}
const trade_policy: Record<Region['economy']['trade'], string> = {
  free: 'Goods are allowed to flow freely across borders without undue interference by government officials, laws, or taxes.',
  protectionism: 'Foreign imports are taxed and discriminated in favor of local products.'
}

const economic_status_text: Record<Region['economy']['status'], string> = {
  struggling:
    'A mixture of unfavorable trade agreements and local misfortune (natural disasters, famines, plagues) have created poorer than average living conditions.',
  healthy:
    'Living conditions are standard. Most have everything they need to survive, but some are able to thrive.',
  prosperous:
    'A mixture of favorable trade agreements and local good fortune (innovation, new industries) have created richer than average living conditions.'
}

interface PoliticsRecord {
  name: string
  description: JSX.Element
}

const PoliticsTable = DataTable<
  PoliticsRecord,
  {
    'item.description': { item: PoliticsRecord }
  }
>()
@Component({
  components: {
    PoliticsTable
  }
})
export default class Politics extends tsx.Component<Record<string, unknown>> {
  public render() {
    const { government, law: legal_system, religion, military, economy } = view_module.codex.nation
    const data = [
      {
        name: `Government`,
        description: (
          <ul>
            <li>
              <i>
                <b>Structure: </b>
                {title_case(government.structure)}.
              </i>{' '}
              {government_structure[government.structure]}
            </li>{' '}
            <li>
              <i>
                <b>Succession: </b>
                {title_case(government.succession)}.
              </i>{' '}
              {government_succession[government.succession]}
            </li>
            <li>
              <i>
                <b>Officials: </b>
                {title_case(government.entry)}.
              </i>{' '}
              {government_entry[government.entry]}
            </li>
          </ul>
        )
      },
      {
        name: `Law`,
        description: (
          <ul>
            <li>
              <i>
                <b>Rule of law: </b>
                {title_case(legal_system.rule_of_law)}.
              </i>{' '}
              {rule_of_law[legal_system.rule_of_law]}
            </li>
            <li>
              <i>
                <b>Accountability: </b>
                {title_case(legal_system.accountability)}.
              </i>{' '}
              {accountability[legal_system.accountability]}
            </li>
            <li>
              <i>
                <b>Arcana: </b>
                {title_case(legal_system.magic)}.
              </i>{' '}
              {magic[legal_system.magic]}
            </li>
          </ul>
        )
      },
      {
        name: `Religion`,
        description: (
          <ul>
            <li>
              <i>
                <b>Authority: </b>
                {title_case(religion.authority)}.
              </i>{' '}
              {religion_authority[religion.authority]}
            </li>
            <li>
              <i>
                <b>Tolerance: </b>
                {title_case(religion.tolerance)}.
              </i>{' '}
              {religion_tolerance[religion.tolerance]}
            </li>
          </ul>
        )
      },
      {
        name: `Military`,
        description: (
          <ul>
            {military.map((type, i) => (
              <li>
                <i>
                  <b>{i === 0 ? 'Primary' : 'Secondary'}: </b>
                  {title_case(type)}.
                </i>{' '}
                {military_type[type]}
              </li>
            ))}
          </ul>
        )
      },
      {
        name: `Economy`,
        description: (
          <ul>
            <li>
              <i>
                <b>Status: </b>
                {title_case(economy.status)}.
              </i>{' '}
              {economic_status_text[economy.status]}
            </li>
            <li>
              <i>
                <b>Allocation: </b>
                {title_case(economy.management)}.
              </i>{' '}
              {economic_management[economy.management]}
            </li>
            <li>
              <i>
                <b>Trade: </b>
                {title_case(economy.trade)}.
              </i>{' '}
              {trade_policy[economy.trade]}
            </li>
          </ul>
        )
      }
    ]
    return (
      <Row wrap>
        <Column cols={12} class='pa-0'>
          <PoliticsTable
            class='ma-0'
            headers={[
              { text: 'Category', value: 'name' },
              { text: 'Description', value: 'description' }
            ]}
            dense
            items={data}
            disable-pagination
            hide-default-footer
            scopedSlots={{
              'item.description': ({ item }) => {
                return <span>{item.description}</span>
              }
            }}
          />
        </Column>
      </Row>
    )
  }
}
