import { Column, Row } from '@/components/common/vuetify/layout'
import { region__neighbors, region__population } from '@/models/regions'
import { region__migrations } from '@/models/regions/diplomacy/migrations'
import { formatters } from '@/models/utilities/text/formatters'
import { view_module } from '@/store/view'
import { css } from '@emotion/css'
import { descending, arc } from 'd3'
import { Chord, chord, ribbon } from 'd3-chord'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

interface Tooltip {
  x: number
  y: number
  display: boolean
  text: string | JSX.Element
}

const cursor_style = css`
  .group:hover {
    cursor: pointer;
  }
`

const tooltip_style = ({ x, y, display }: Tooltip) => {
  return css`
    position: fixed;
    display: ${display ? 'block' : 'none'};
    top: ${y - 60}px;
    left: ${x - 60}px;
    background-color: black;
    opacity: 0.7;
    border-radius: 4px;
    color: white;
    padding: 5px;
    font-size: 12px;
    line-height: 18px;
    animation: fadein 0.5s;
    @keyframes fadein {
      from {
        opacity: 0;
      }
      to {
        opacity: 0.7;
      }
    }
  `
}

@Component
export default class Migrations extends tsx.Component<Record<string, unknown>> {
  private selected_group = -1
  private selected_chord = ''
  private tooltip: Tooltip = { x: 0, y: 0, display: false, text: '' }
  private show_tooltip(params: { event: MouseEvent; text: JSX.Element }) {
    const { event, text } = params
    this.tooltip.display = true
    this.tooltip.x = event.clientX
    this.tooltip.y = event.clientY
    this.tooltip.text = text
  }
  public render() {
    const nation = view_module.codex.nation
    const area = [...region__neighbors(nation).map(i => window.world.regions[i]), nation]
    const height = 190
    const width = 300
    const innerRadius = Math.min(width, height) * 0.35
    const outerRadius = innerRadius * 1.1
    const names = area.map(region => region.name)
    const colors = area.map(region => region.colors)
    area.forEach(source => region__migrations(source))
    const matrix = area.map(source =>
      area.map(target => (source.idx === target.idx ? 0 : source.immigration[target.idx] ?? 0))
    )
    const layout = chord().padAngle(0.15).sortChords(descending)(matrix)
    const arc_generator = arc()
    const ribbon_generator = ribbon()
    const chord_id = (data: Chord) => `chord-${data.source.index}-${data.target.index}`
    return (
      <Row wrap>
        <Column cols={12} class={`pa-0 ${cursor_style}`}>
          <div class='tooltips'>
            <div class='tooltip'></div>
          </div>
          <svg
            viewBox={`0 0 ${width} ${height}`}
            width={'100%'}
            height={height}
            class='my-4'
            id='migrations'>
            <g transform={`translate(${width / 2},${height / 2})`}>
              <defs>
                {layout.map(chord => {
                  const source_start =
                    (chord.source.endAngle - chord.source.startAngle) / 2 +
                    chord.source.startAngle -
                    Math.PI / 2
                  const target_start =
                    (chord.target.endAngle - chord.target.startAngle) / 2 +
                    chord.target.startAngle -
                    Math.PI / 2
                  return (
                    <linearGradient
                      id={chord_id(chord)}
                      gradientUnits='userSpaceOnUse'
                      x1={innerRadius * Math.cos(source_start)}
                      y1={innerRadius * Math.sin(source_start)}
                      x2={innerRadius * Math.cos(target_start)}
                      y2={innerRadius * Math.sin(target_start)}>
                      <stop offset='0%' stop-color={colors[chord.source.index]}></stop>
                      <stop offset='100%' stop-color={colors[chord.target.index]}></stop>
                    </linearGradient>
                  )
                })}
              </defs>
              {layout.groups.map(({ startAngle, endAngle, index }) => {
                const angle = (startAngle + endAngle) / 2
                const region = area[index]
                const pop = region__population(region)
                const local_im = matrix[index].reduce((sum, im) => sum + im, 0)
                const local_em = matrix.reduce((sum, im) => sum + im[index], 0)
                return (
                  <g class='group'>
                    <path
                      fill={colors[index]}
                      onclick={() =>
                        view_module.update_codex({
                          target: { tag: 'nation', idx: region.idx }
                        })
                      }
                      onmousemove={(event: MouseEvent) => {
                        this.show_tooltip({
                          event,
                          text: (
                            <span>
                              <span>
                                Immigrants: {formatters.compact(local_im)} (
                                {formatters.percent({
                                  value: local_im / pop
                                })}
                              </span>
                              )<br />
                              <span>
                                Emigrants: {formatters.compact(local_em)} (
                                {formatters.percent({
                                  value: local_em / pop
                                })}
                                )
                              </span>
                            </span>
                          )
                        })
                        this.selected_group = index
                      }}
                      onmouseout={() => {
                        this.tooltip.display = false
                        this.selected_group = -1
                      }}
                      d={arc_generator({
                        startAngle,
                        endAngle,
                        innerRadius: innerRadius * 1.01,
                        outerRadius
                      })}></path>
                    <text
                      dy='0.5em'
                      font-size={5}
                      text-anchor={angle < Math.PI ? null : 'end'}
                      transform={`rotate(${(angle * 180) / Math.PI - 90})translate(${
                        outerRadius + 5
                      })${angle > Math.PI ? 'rotate(180)' : ''}`}>
                      {names[index]}
                    </text>
                  </g>
                )
              })}
              {layout.map(chord => {
                const { source, target } = chord
                const group_selected =
                  source.index === this.selected_group ||
                  target.index === this.selected_group ||
                  this.selected_group < 0
                const id = chord_id(chord)
                const chord_selected = !this.selected_chord || this.selected_chord === id
                const source_pop = Object.values(area[target.index].immigration).reduce(
                  (sum, count) => sum + count,
                  0
                )
                const source_immigrants = matrix[target.index][source.index]
                const target_pop = Object.values(area[source.index].immigration).reduce(
                  (sum, count) => sum + count,
                  0
                )
                const target_immigrants = matrix[source.index][target.index]
                return (
                  <g class='chord'>
                    <path
                      opacity={group_selected !== chord_selected ? 0.1 : 0.8}
                      onmousemove={(event: MouseEvent) => {
                        this.selected_chord = id
                        this.show_tooltip({
                          event,
                          text: (
                            <span>
                              {names[target.index]} → {names[source.index]}:{' '}
                              {formatters.compact(target_immigrants)} (
                              {formatters.percent({
                                value: target_immigrants / target_pop
                              })}
                              )<br />
                              {names[source.index]} → {names[target.index]}:{' '}
                              {formatters.compact(source_immigrants)} (
                              {formatters.percent({
                                value: source_immigrants / source_pop
                              })}
                              )
                            </span>
                          )
                        })
                      }}
                      onmouseout={() => {
                        this.tooltip.display = false
                        this.selected_chord = ''
                      }}
                      fill={`url(#${chord_id(chord)})`}
                      d={ribbon_generator({
                        source: { ...source, radius: innerRadius },
                        target: { ...target, radius: innerRadius }
                      })}></path>
                  </g>
                )
              })}
            </g>
          </svg>
          <div class={tooltip_style(this.tooltip)}>{this.tooltip.text}</div>
        </Column>
      </Row>
    )
  }
}
