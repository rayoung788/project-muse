import { SimpleTabbedContent } from '@/components/common/tabs/button_group'
import { Column, Row } from '@/components/common/vuetify/layout'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import Society from './Society'
import Migrations from './Migrations'

@Component({ components: { Society, Migrations } })
export default class Politics extends tsx.Component<Record<string, unknown>> {
  public render() {
    return (
      <Row wrap>
        <Column cols={12} class='pa-0'>
          <SimpleTabbedContent
            content={{
              society: <Society></Society>,
              immigration: <Migrations></Migrations>
            }}></SimpleTabbedContent>
        </Column>
      </Row>
    )
  }
}
