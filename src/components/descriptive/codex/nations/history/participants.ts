import { WarActorRecord } from '@/models/history/events/war/types'
import { decorate_text } from '@/models/utilities/text/decoration'

export const ally_color = '#7f6868'

export const allied_participants = (allies: WarActorRecord['allies']) =>
  allies.map(ally =>
    decorate_text({
      link: window.world.regions[ally.idx],
      tooltip: ally.relation,
      color: ally.neutral ? 'gray' : ally_color
    })
  )
