import SectionList from '@/components/common/text/SectionList'
import { Rebellion, RebellionRecord } from '@/models/history/events/rebellion/types'
import { formatters, format__date_range } from '@/models/utilities/text/formatters'
import { decorate_text } from '@/models/utilities/text/decoration'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { Column, Row } from '../../../../common/vuetify/layout'
import { DataTable } from '../../../../common/vuetify/table'
import CodexPage from '../..'
import StyledText from '../../../../common/text/StyledText'
import { allied_participants } from './participants'
import { title_case } from '@/models/utilities/text'

type rebellion_status = 'fading' | 'stalemated' | 'peaked'
const status_text: Record<rebellion_status, string> = {
  fading: 'The rebel movement fades in strength as heavy causalities are taken',
  stalemated: 'Both sides are evenly matched. The rebellion will be long and bloody',
  peaked: 'The rebels are emboldened as they quickly approach their goals'
}

const ItemsTable = DataTable<
  RebellionRecord,
  {
    'item.time': { item: RebellionRecord }
    'item.odds': { item: RebellionRecord }
    'item.victory': { item: RebellionRecord }
  }
>()

@Component({
  components: {
    ItemsTable,
    CodexPage
  }
})
export default class RebellionView extends tsx.Component<Record<string, unknown>> {
  private loyalists(rebellion: Rebellion) {
    const { loyalists } = rebellion
    const { idx, allies } = loyalists
    const participants = [decorate_text({ link: window.world.regions[idx] })]
      .concat(allied_participants(allies))
      .join(', ')
    return (
      <SectionList
        list={[['Loyalists', <StyledText text={participants}></StyledText>]]}></SectionList>
    )
  }
  private rebels(rebellion: Rebellion) {
    const { rebels } = rebellion
    const { idx, allies } = rebels
    const region = window.world.regions[idx]
    const participants = [`${decorate_text({ link: region })}`]
      .concat(allied_participants(allies))
      .join(', ')
    return (
      <SectionList list={[[`Rebels`, <StyledText text={participants}></StyledText>]]}></SectionList>
    )
  }
  private get general() {
    const rebellion = view_module.codex.rebellion
    const { events, background, result, next_battle } = rebellion
    const status: rebellion_status =
      next_battle.odds > 0.6 ? 'peaked' : next_battle.odds > 0.4 ? 'stalemated' : 'fading'
    return (
      <Row>
        <Column cols={6}>{this.loyalists(rebellion)}</Column>
        <Column cols={6}>{this.rebels(rebellion)}</Column>
        <Column cols={12}>
          <SectionList
            list={[
              [
                'Background',
                <span>
                  <i>{title_case(background.type)}. </i>
                  <StyledText text={background.text}></StyledText>
                </span>
              ],
              [
                'Status',
                <span>
                  <i>{title_case(result ? 'concluded' : status)}. </i>
                  {result ? (
                    <StyledText text={result}></StyledText>
                  ) : (
                    `${status_text[status]}. (${formatters.percent({ value: next_battle.odds })})`
                  )}
                </span>
              ]
            ]}></SectionList>
        </Column>
        <Column cols={12} class='subtab-content'>
          <ItemsTable
            headers={[
              { text: 'Date', value: 'time', sortable: false },
              { text: 'Event', value: 'title', sortable: false },
              { text: 'Attacker', value: 'attacker', sortable: false },
              { text: 'Strength', value: 'odds', sortable: false },
              { text: 'Result', value: 'victory', sortable: false },
              { text: '', value: 'data-table-expand' }
            ]}
            items={events}
            items-per-page={10}
            dense
            show-expand
            item-key='time'
            scopedSlots={{
              'item.time': ({ item }) => formatters.date(item.time),
              'item.odds': ({ item }) => formatters.percent({ value: item.odds }),
              'item.victory': ({ item }) => (item.victory ? 'Victory' : 'Defeat'),
              'expanded-item': ({
                item,
                headers
              }: {
                item: RebellionRecord
                headers: string[]
              }) => (
                <td class='py-2' colspan={headers.length}>
                  <StyledText text={item.text}></StyledText>
                </td>
              )
            }}></ItemsTable>
        </Column>
      </Row>
    )
  }
  public render() {
    const event = view_module.codex.rebellion
    if (!event) return <span>none</span>
    return (
      <CodexPage
        title={event.name}
        subtitle={`(${event.idx}) Event (rebellion), ${format__date_range(event)}`}
        content={this.general}
        tabs={{}}></CodexPage>
    )
  }
}
