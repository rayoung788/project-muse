import LineChart from '@/components/common/charts/LineChart'
import { TooltipItem } from '@/components/common/charts/TooltipItem'
import { Column, Row } from '@/components/common/vuetify/layout'
import { DataTable } from '@/components/common/vuetify/table'
import { region__politics, status_splitter } from '@/models/history/events/health'
import { event_type, event_types, LogRecord } from '@/models/history/types'
import { year_ms } from '@/models/utilities/math/time'
import { formatters } from '@/models/utilities/text/formatters'
import { clean_decoration } from '@/models/utilities/text/decoration'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import StyledText from '../../../../common/text/StyledText'
import { TabbedContent } from '../../../../common/tabs/button_group'
import { triangular_number } from '@/models/utilities/math'
interface PastEvent {
  event: event_type
  title: string
  text: string
  time: number
  wealth: number
  max_wealth: number
  idx: number
}
const EventTable = DataTable<
  LogRecord,
  {
    'item.text': { item: LogRecord }
    'item.time': { item: LogRecord }
    'item.title': { item: LogRecord }
  }
>()

const year = year_ms - 1

const valid_events = event_types.filter(e => e !== 'health_check')

@Component({
  components: {
    EventTable,
    LineChart,
    TabbedContent
  }
})
export default class History extends tsx.Component<Record<string, unknown>> {
  protected selected_event = -1
  private tooltipLabels(tooltipItem: TooltipItem, data: { datasets: { raw: PastEvent[] }[] }) {
    const { index, datasetIndex } = tooltipItem
    return data.datasets[datasetIndex].raw[index].text.split(status_splitter).map(clean_decoration)
  }
  private title(
    tooltipItems: TooltipItem[],
    data: { datasets: { raw: PastEvent[]; label: string }[] }
  ) {
    const [item] = tooltipItems
    const { index, label, datasetIndex } = item
    const past = data.datasets[datasetIndex].raw[index]
    return `${data.datasets[datasetIndex].label} [${formatters.date(
      parseInt(label)
    )}]: ${clean_decoration(past.title)} [${past.idx}]`
  }
  private footer(tooltipItems: TooltipItem[], data: { datasets: { raw: PastEvent[] }[] }) {
    const [item] = tooltipItems
    const { index, datasetIndex } = item
    const { wealth, max_wealth } = data.datasets[datasetIndex].raw[index]
    const percent = (max_wealth <= 0 ? 0 : wealth / max_wealth) * 100
    return `Wealth: ${wealth.toFixed(2)} (${percent.toFixed(2)}%) [${max_wealth.toFixed(2)}]`
  }
  public render() {
    const overlord = view_module.codex.nation
    const nations = overlord.regions
      .map(t => window.world.provinces[t])
      .map(p => window.world.regions[p.region])
    return (
      <TabbedContent
        selection={valid_events}
        default_idx={valid_events.findIndex(cls => cls === 'war')}
        content={(selected: string) => {
          const event_filter = selected as event_type
          const datasets = nations.map(nation => {
            let records: PastEvent[] = nation.past
              .map(i => window.world.past[i])
              .concat([
                {
                  ...region__politics(nation),
                  idx: -1
                }
              ])
              .filter(e => event_filter === e.type || e.type === 'health_check')
              .map(({ actors, time, title, text, type, idx }) => {
                const n = actors.find(a => a.idx === nation.idx)
                return {
                  wealth: n.wealth,
                  max_wealth: n.max_wealth,
                  time,
                  title,
                  text,
                  event: type,
                  idx
                }
              })
              .sort((a, b) => a.time - b.time)
            records = records.filter((p, i) => {
              if (p.event !== 'health_check' || p.idx === -1) return triangular_number
              const next = records[i + 1]
              const upper_window = !next || p.time + year < next.time
              const previous = records[i - 1]
              const lower_window = !previous || p.time - year > previous.time
              return upper_window && lower_window
            })
            return {
              label: nation.name,
              data: records.map(({ wealth, time, idx }) => ({
                x: time,
                y: wealth,
                i: idx
              })),
              fill: false,
              showLine: true,
              hidden: nation !== overlord,
              backgroundColor: nation.colors,
              pointRadius: records.map(({ event, idx }) =>
                event === 'health_check' ? 1 : idx !== this.selected_event ? 4 : 6
              ),
              pointBorderWidth: records.map(({ event, idx }) =>
                event === 'health_check' ? 1 : idx !== this.selected_event ? 3 : 5
              ),
              pointBackgroundColor: records.map(({ event }) =>
                event !== 'health_check' ? 'white' : nation.colors
              ),
              pointBorderColor: nation.colors,
              raw: records
            }
          })
          const dates = Array.from(new Set(datasets.map(({ data }) => data.map(p => p.x)).flat()))
          const chart_data = {
            labels: dates.map(time => formatters.date(time)),
            datasets
          }
          return (
            <Row>
              <Column cols={12}>
                <LineChart
                  chartData={chart_data}
                  options={{
                    tooltips: {
                      displayColors: false,
                      callbacks: {
                        title: this.title,
                        label: this.tooltipLabels,
                        footer: this.footer
                      }
                    },
                    onClick: (_: Event, data: { _datasetIndex: number; _index: number }[]) => {
                      const [d] = data
                      const event_idx = datasets[d?._datasetIndex]?.data[d?._index]?.i
                      if (window.world.past[event_idx]) {
                        this.selected_event = event_idx
                      }
                    },
                    scales: {
                      xAxes: [
                        {
                          ticks: {
                            callback: (time: string) => {
                              return formatters.date(parseInt(time))
                            }
                          }
                        }
                      ]
                    },
                    plugins: {
                      zoom: {
                        pan: {
                          enabled: true
                        },
                        zoom: {
                          enabled: true,
                          drag: false,
                          speed: 0.1
                        }
                      }
                    }
                  }}
                  height={200}
                />
              </Column>
              {window.world.past[this.selected_event] && (
                <Column cols={12}>
                  <EventTable
                    dense
                    disable-pagination
                    hide-default-footer
                    headers={[
                      { text: 'Date', value: 'time', sortable: false },
                      { text: 'Event', value: 'title', sortable: false, width: 275 },
                      { text: 'Description', value: 'text', sortable: false }
                    ]}
                    items={[window.world.past[this.selected_event]]}
                    scopedSlots={{
                      'item.text': ({ item }) => <StyledText text={item.text}></StyledText>,
                      'item.title': ({ item }) => <StyledText text={item.title}></StyledText>,
                      'item.time': ({ item }) => formatters.date(item.time)
                    }}
                  />
                </Column>
              )}
            </Row>
          )
        }}></TabbedContent>
    )
  }
}
