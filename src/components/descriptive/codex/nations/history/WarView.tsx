import SectionList from '@/components/common/text/SectionList'
import { region__formatted_wealth } from '@/models/regions/diplomacy/status'
import { WarRecord, WarActorRecord } from '@/models/history/events/war/types'
import { scale } from '@/models/utilities/math'
import { formatters, format__date_range } from '@/models/utilities/text/formatters'
import { decorate_text } from '@/models/utilities/text/decoration'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { Column, Row } from '../../../../common/vuetify/layout'
import { DataTable } from '../../../../common/vuetify/table'
import CodexPage from '../..'
import StyledText from '../../../../common/text/StyledText'
import { allied_participants } from './participants'
import { title_case } from '@/models/utilities/text'
import { war__victory_odds } from '@/models/history/events/war/battles'

type war_status = 'decisive' | 'stalemated' | 'struggling'
const status_text: Record<war_status, string> = {
  decisive: 'The invaders are optimistic that the war will end swiftly',
  stalemated: 'Both sides are evenly matched. The war will be long and bloody',
  struggling: 'The defenders were stronger than expected. The invasion is being repulsed'
}

const ItemsTable = DataTable<
  WarRecord,
  {
    'item.time': { item: WarRecord }
    'item.attacker': { item: WarRecord }
    'item.odds': { item: WarRecord }
    'item.victory': { item: WarRecord }
  }
>()

const armySize = (population: number, wealth_percent: number) => {
  const size_mod = scale([0, 1000000, 100000000], [0.1, 0.015, 0.01], population)
  const wealth_mod = Math.max(0.1, wealth_percent)
  return population * size_mod * wealth_mod
}

@Component({
  components: {
    ItemsTable,
    CodexPage
  }
})
export default class WarView extends tsx.Component<Record<string, unknown>> {
  private actor(agent: WarActorRecord, label: string) {
    const { idx, allies, pop, wealth_percent, wealth } = agent
    const troops = armySize(pop, wealth_percent)
    const participants = [
      decorate_text({
        link: window.world.regions[idx],
        tooltip: region__formatted_wealth(window.world.regions[idx], wealth)
      })
    ]
      .concat(allied_participants(allies))
      .join(', ')
    return (
      <SectionList
        list={[
          [
            `${label}${participants.length > 1 ? 's' : ''}`,
            <StyledText text={participants}></StyledText>
          ],
          [
            'Troops',
            `${formatters.compact(troops)} (${formatters.percent({
              value: troops / agent.pop,
              precision: 2
            })})`
          ]
        ]}></SectionList>
    )
  }
  private get general() {
    const war_record = view_module.codex.war
    const { invader, defender, background, result } = war_record
    const victory_odds = war__victory_odds(
      window.world.regions[invader.idx],
      window.world.regions[defender.idx]
    )
    const status: war_status =
      victory_odds > 0.7 ? 'decisive' : victory_odds > 0.3 ? 'stalemated' : 'struggling'
    return (
      <Row>
        <Column cols={6}>{this.actor(invader, 'Invader')}</Column>
        <Column cols={6}>{this.actor(defender, 'Defender')}</Column>
        <Column cols={12}>
          <SectionList
            list={[
              [
                'Background',
                <span>
                  <i>{title_case(background.type)}. </i>
                  <StyledText text={background.text}></StyledText>
                </span>
              ],
              [
                'Status',
                <span>
                  <i>{title_case(result ? 'concluded' : status)}.</i>{' '}
                  {result ? (
                    <StyledText text={result}></StyledText>
                  ) : (
                    `${status_text[status]}. (${formatters.percent({ value: victory_odds })})`
                  )}
                </span>
              ]
            ]}></SectionList>
        </Column>
        <Column cols={12} class='subtab-content'>
          <ItemsTable
            headers={[
              { text: 'Date', value: 'time', sortable: false },
              { text: 'Event', value: 'title', sortable: false },
              { text: 'Attacker', value: 'attacker', sortable: false },
              { text: 'Odds', value: 'odds', sortable: false },
              { text: 'Result', value: 'victory', sortable: false },
              { text: '', value: 'data-table-expand' }
            ]}
            items={war_record.events}
            items-per-page={10}
            dense
            show-expand
            item-key='time'
            scopedSlots={{
              'item.time': ({ item }) => formatters.date(item.time),
              'item.attacker': ({ item }) => (
                <StyledText
                  text={decorate_text({
                    link: window.world.regions[item.attacker]
                  })}></StyledText>
              ),
              'item.odds': ({ item }) => formatters.percent({ value: item.odds }),
              'item.victory': ({ item }) => (item.victory ? 'Victory' : 'Defeat'),
              'expanded-item': ({ item, headers }: { item: WarRecord; headers: string[] }) => (
                <td class='py-2' colspan={headers.length}>
                  <StyledText text={item.text}></StyledText>
                </td>
              )
            }}></ItemsTable>
        </Column>
      </Row>
    )
  }
  public render() {
    const event = view_module.codex.war
    if (!event) return <span>none</span>
    return (
      <CodexPage
        title={event.name}
        subtitle={`(${event.idx}) Event (war), ${format__date_range(event)}`}
        content={this.general}
        tabs={{}}></CodexPage>
    )
  }
}
