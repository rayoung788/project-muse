import { AutoComplete } from '@/components/common/vuetify/input'
import { Column, Container, Row } from '@/components/common/vuetify/layout'
import { css_colors } from '@/styles/colors'
import { Creature } from '@/models/npcs/species'
import { primordial__tooltip } from '@/models/npcs/species/primordials'
import { decorate_text } from '@/models/utilities/text/decoration'
import { scarcity, rarity__rank } from '@/models/utilities/quality'
import { title_case } from '@/models/utilities/text'
import { view_module } from '@/store/view'
import { Component, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { SimpleTabbedContent, TabbedContent } from '../../../common/tabs/button_group'
import SectionList from '../../../common/text/SectionList'
import StyledText from '../../../common/text/StyledText'

const decorate__creature = (creature: Creature) => {
  const prey = creature?.prey?.length > 0
  if (creature.tag === 'beast') {
    const predator = prey || creature.role === 'predator'
    return decorate_text({
      link: creature,
      tooltip: title_case(creature.genus.name),
      color: predator ? css_colors.primary : undefined
    })
  }
  const predator = prey || creature.traits.some(trait => trait.tag === 'carnivorous')
  return decorate_text({
    link: creature,
    tooltip: primordial__tooltip(creature),
    color: predator ? css_colors.primary : undefined
  })
}

@Component({
  components: {
    SimpleTabbedContent,
    TabbedContent
  }
})
export default class Ecology extends tsx.Component<Record<string, unknown>> {
  private terrain: string[] = []
  private regions: number[] = []
  public mounted() {
    this.reset_terrain()
  }
  public get region() {
    return view_module.codex.nation
  }

  @Watch('region')
  public reset_terrain() {
    this.regions = [this.region.idx]
    this.terrain = Object.keys(this.region.beasts).slice(0, 1)
  }
  public species(index: 'primordials' | 'beasts') {
    const regions = this.region.regions.map(province => window.world.provinces[province].region)
    const all_creatures = Array.from(
      new Set(
        regions
          .map(ridx => {
            const candidates = window.world.regions[ridx][index]
            return Object.values(candidates).flat()
          })
          .flat()
      )
    ).map(i => window.world[index][i])
    const selected = all_creatures
      .filter(creature => {
        const { environment } = creature
        const terrain_match = this.terrain.includes(environment.key)
        return terrain_match && creature.regions.some(r => this.regions.includes(r))
      })
      .sort((a, b) => a.length - b.length)
    return Object.values(scarcity)
      .map(
        rare =>
          [rare, selected.filter(creature => creature.rarity === rare)] as [number, Creature[]]
      )
      .filter(([, v]) => v.length > 0)
      .map(([k, v]) => [
        title_case(rarity__rank(k)),
        <StyledText text={v.map(creature => decorate__creature(creature)).join(', ')}></StyledText>
      ]) as [string, JSX.Element][]
  }
  public render() {
    const tabs = ['fauna', 'flora']
    return (
      <TabbedContent
        selection={tabs}
        content={tab => {
          const tag = tab === 'fauna' ? 'beasts' : 'primordials'
          const regions = this.region.regions.map(
            province => window.world.provinces[province].region
          )
          const all_terrain = Array.from(
            new Set(regions.map(r => Object.keys(window.world.regions[r][tag])).flat())
          )
          const selected_regions = this.regions.map(r => window.world.regions[r])
          const valid_terrain = all_terrain.filter(terr => selected_regions.some(r => r[tag][terr]))
          const sections = this.species(tag)
          return (
            <Container fluid>
              <Row>
                <Column>
                  <AutoComplete
                    v-model={this.regions}
                    items={regions.map(i => {
                      const { idx, name } = window.world.regions[i]
                      return { idx, name }
                    })}
                    item-text='name'
                    item-value='idx'
                    color='primary'
                    chips
                    small-chips
                    deletable-chips
                    multiple></AutoComplete>
                </Column>
                <Column>
                  <AutoComplete
                    v-model={this.terrain}
                    items={valid_terrain}
                    chips
                    small-chips
                    deletable-chips
                    multiple></AutoComplete>
                </Column>
              </Row>
              <Row>
                <Column>
                  <SectionList list={sections}></SectionList>
                </Column>
              </Row>
            </Container>
          )
        }}></TabbedContent>
    )
  }
}
