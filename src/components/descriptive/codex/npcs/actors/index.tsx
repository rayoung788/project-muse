import PrimaryButton from '@/components/common/PrimaryButton'
import { TabProps } from '@/components/common/vuetify/tabs'
import { actor__location } from '@/models/npcs/actors'
import { npc__rest } from '@/models/npcs/actors/actions'
import { actor__expired, actor__is_child } from '@/models/npcs/actors/stats/age'
import { species__by_culture } from '@/models/npcs/species/humanoids/taxonomy'
import { species__size_rank } from '@/models/npcs/species/size'
import { province__hub } from '@/models/regions/provinces'
import { decorate_text } from '@/models/utilities/text/decoration'
import { formatters } from '@/models/utilities/text/formatters'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import StyledText from '../../../../common/text/StyledText'
import CodexPage from '../..'
import NPCAncestry from './ancestry'
import BarterView from './containers/Barter'
import InventoryView from './containers/Inventory'
import Statistics from './stats'

@Component({
  components: {
    CodexPage,
    InventoryView,
    NPCAncestry,
    BarterView,
    Statistics
  }
})
export default class ActorView extends tsx.Component<Record<string, unknown>> {
  private actions() {
    const actor = view_module.codex.actor
    if (actor !== view_module.avatar) return undefined
    return <PrimaryButton class='mr-2' text='Rest' click={() => npc__rest(actor)} />
  }
  public render() {
    const actor = view_module.codex.actor
    if (!actor) return <div>nothing here :)</div>
    const culture = window.world.cultures[actor.culture]
    const expired = actor__expired(actor)
    const is_adult = !actor__is_child({ actor })
    const loc = actor__location(actor)
    const same_loc = view_module.avatar && loc === actor__location(view_module.avatar)
    const working = !expired && is_adult && same_loc
    const tabs: Record<string, TabProps> = {
      Statistics: { element: <Statistics></Statistics> }
    }
    if (actor.relations.length > 0) {
      tabs.Ancestry = { element: <NPCAncestry></NPCAncestry> }
    }
    if (actor === view_module.avatar) {
      tabs.Inventory = { element: <InventoryView></InventoryView> }
    }
    if (actor.barter && working) {
      tabs.Barter = { element: <BarterView></BarterView> }
    }
    const { birth } = actor.location
    const ethnicity = window.world.provinces[birth]
    const birthplace = province__hub(ethnicity)
    const species = species__by_culture(culture)
    return (
      <CodexPage
        title={`${actor.name} ${actor.surname}`}
        subtitle={
          <StyledText
            color={css_colors.subtitle}
            text={`(${actor.idx}) ${species__size_rank(
              species.size
            )} humanoid (${culture.species.toLowerCase()}), ${decorate_text({
              label: formatters.date(actor.birth_date),
              link: birthplace,
              tooltip: birthplace.name,
              color: css_colors.subtitle
            })} - ${decorate_text({
              label: expired ? formatters.date(actor.expires) : 'Present',
              link: loc,
              tooltip: loc?.name ?? 'unknown',
              color: css_colors.subtitle
            })}`}></StyledText>
        }
        tabs={tabs}
        actions={this.actions()}></CodexPage>
    )
  }
}
