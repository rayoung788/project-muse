import { BaseButton, ButtonToggle } from '@/components/common/vuetify/button'
import { Column, Container, Row } from '@/components/common/vuetify/layout'
import { actor__ancestry_nodes, AncestorNode } from '@/models/npcs/actors/history/ancestry'
import { actor__parents } from '@/models/npcs/actors/spawn/relations/parent'
import { actor__age, actor__expired } from '@/models/npcs/actors/stats/age'
import { genders } from '@/models/npcs/actors/stats/appearance/gender'
import { Actor } from '@/models/npcs/actors/types'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'
import {
  cluster,
  event,
  HierarchyPointLink,
  HierarchyPointNode,
  linkRadial,
  pointRadial,
  select,
  stratify,
  zoom
} from 'd3'
import { componentFactory } from 'vue-tsx-support'

const height = 400
const width = 400
const svg_id = 'svg_ancestry'

const enum lineage_types {
  PATERNAL = 'Paternal',
  MATERNAL = 'Maternal'
}

const gender_lineage = (gender: genders) =>
  gender === 'male' ? lineage_types.PATERNAL : lineage_types.MATERNAL

const npc__get_lineages = (npc: Actor) => {
  const parents = actor__parents(npc)
  return parents.map(p => ({
    lineage: p.lineage,
    type: gender_lineage(p.gender)
  }))
}

const ExtendedAncestry = componentFactory.create({
  data() {
    return {
      dx: 0,
      dy: 0,
      scale: 1,
      lineage: lineage_types.PATERNAL
    }
  },
  mounted() {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const zoom_elm: any = zoom()
      .scaleExtent([1, 30])
      .translateExtent([
        [-width, -height],
        [2 * width, 2 * height]
      ])
      .on('zoom', () => {
        const { x, y, k } = event.transform
        this.dx = x
        this.dy = y
        this.scale = k
      })
    const svg = document.getElementById(svg_id) as HTMLElement
    const init = select(svg)
    zoom_elm.scaleTo(init, 1)
    init.call(zoom_elm)
  },
  computed: {
    npc() {
      return view_module.codex.actor
    }
  },
  watch: {
    npc() {
      const culture = window.world.cultures[this.npc.culture]
      this.lineage = gender_lineage(culture.lineage)
    }
  },
  render() {
    const npc = this.npc
    const { lineage } = npc__get_lineages(npc).find(parent => parent.type === this.lineage)
    const ancestry = Object.values(actor__ancestry_nodes(npc, lineage))
    const roots = ancestry.filter(node => node.parentId === null).length
    if (roots > 1) {
      ancestry
        .filter(node => node.parentId === null)
        .forEach(node => {
          node.parentId = -1
        })
      ancestry.push({ id: -1, parentId: null })
    }
    const root = stratify<AncestorNode>()(ancestry)
    const radial = cluster<AncestorNode>().size([2 * Math.PI, Math.min(width, height) / 2])(root)
    const nodes = radial.descendants().map(n => {
      const id = parseInt(n.id)
      const curr = window.world.actors[id]
      const age = curr ? actor__age({ actor: curr, expire_cap: true }) : -1
      return {
        loc: `translate(${pointRadial(n.x, n.y)})`,
        name: curr?.name ? `${curr.name} ${curr.surname}` : 'Unknown Ancestor',
        age,
        expired: curr ? actor__expired(curr) : false,
        id,
        parent: n.parent
      }
    })
    const radial_link = linkRadial<
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      any,
      HierarchyPointLink<AncestorNode>,
      HierarchyPointNode<AncestorNode>
    >()
      .angle(d => d.x)
      .radius(d => d.y)
    const links = radial.links().map(n => radial_link(n))
    const buttons = [lineage_types.PATERNAL, lineage_types.MATERNAL]
    return (
      <Container fluid>
        <Row>
          <Column cols={12}>
            <svg
              id={svg_id}
              viewBox={`0 0 ${width} ${height}`}
              style={{
                'background-color': 'transparent',
                cursor: 'default',
                width: `100%`,
                height: `${height}px`
              }}>
              <g transform={`translate(${this.dx}, ${this.dy}) scale(${this.scale})`}>
                <g transform={`translate(${width * 0.5}, ${width * 0.5})`}>
                  <g
                    id='links'
                    style={{
                      fill: 'none',
                      stroke: '#58180d',
                      'stroke-width': '2px'
                    }}>
                    {links.map((d, i) => (
                      <path key={i} d={d}></path>
                    ))}
                  </g>
                  <g
                    id='nodes'
                    style={{
                      fill: '#fff',
                      cursor: 'pointer'
                    }}>
                    {nodes.map((d, i) => {
                      const { loc, id, parent } = d
                      const radius = id === npc.idx ? 5 : 3
                      const width = id === npc.idx ? 4 : 2
                      return (
                        <g key={i}>
                          <circle
                            transform={loc}
                            r={radius}
                            stroke-width={width}
                            stroke={parent ? 'black' : '#c9ad6a'}
                            onClick={() => {
                              if (window.world.actors[id])
                                view_module.update_codex({ target: window.world.actors[id] })
                            }}></circle>
                        </g>
                      )
                    })}
                  </g>
                  <g>
                    {nodes.map((d, i) => {
                      const { loc, name, id, age, expired } = d
                      const size = id === npc.idx ? 10 : 6
                      return (
                        <g key={i}>
                          <text
                            transform={loc}
                            font-size={size}
                            dy={-size}
                            dx={0}
                            text-anchor='middle'>
                            {name} (<tspan fill={expired ? 'red' : 'black'}>{age}</tspan>)
                          </text>
                        </g>
                      )
                    })}
                  </g>
                </g>
              </g>
            </svg>
          </Column>
        </Row>
        <Row align='center' justify='center'>
          <ButtonToggle
            v-model={this.lineage}
            mandatory
            shaped
            color={css_colors.primary}
            background-color={css_colors.accent}>
            {buttons.map(chart => (
              <BaseButton tile small value={chart}>
                {chart}
              </BaseButton>
            ))}
          </ButtonToggle>
        </Row>
      </Container>
    )
  }
})

export default ExtendedAncestry
