import { Column, Row } from '@/components/common/vuetify/layout'
import { actor__relation } from '@/models/npcs/actors'
import { actor__parents } from '@/models/npcs/actors/spawn/relations/parent'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import ActorTable, { ActorHeaders, ActorRecord, construct_actor_record } from '../ActorTable'

@Component({
  components: {
    ActorTable
  }
})
export default class ImmediateFamily extends tsx.Component<Record<string, unknown>> {
  public render() {
    const { actor } = view_module.codex
    const headers: ActorHeaders = [
      { text: 'Relation', value: 'relation' },
      { text: 'Age', value: 'age' },
      { text: 'Culture', value: 'culture' },
      { text: 'Location', value: 'location' }
    ]
    const { actors } = window.world
    // spouse
    const [spouse] = actor__relation({ actor, type: 'spouse' })
    const spouseRecord: ActorRecord[] = []
    if (spouse) {
      spouseRecord.push({
        ...construct_actor_record(spouse),
        relation: spouse.gender === 'female' ? 'Wife' : 'Husband',
        group: 'Spouse'
      })
    }
    // children
    const childRecords = actor__relation({ actor, type: 'child' }).map(child => ({
      ...construct_actor_record(child),
      relation: child.gender === 'female' ? 'Daughter' : 'Son',
      group: 'Children'
    }))
    // parents
    const parents = actor__parents(actor)
    const parentRecords = parents.map(actor => ({
      ...construct_actor_record(actor),
      relation: actor.gender === 'female' ? 'Mother' : 'Father',
      group: 'Parents'
    }))
    // siblings
    const parent_children = parents
      .map(p => actor__relation({ actor: p, type: 'child' }))
      .flat()
      .map(child => child.idx)
    const siblings = Array.from(new Set(parent_children)).filter(n => n !== actor.idx)
    const siblingRecords = siblings.map(n => {
      const child = actors[n]
      return {
        ...construct_actor_record(child),
        relation: child.gender === 'male' ? 'Brother' : 'Sister',
        group: 'Siblings'
      }
    })
    // grandparents
    const grandparents = parents.map(p => actor__parents(p)).flat()
    const grandRecords = grandparents.map(actor => ({
      ...construct_actor_record(actor),
      relation: actor.gender === 'female' ? 'Grandmother' : 'Grandfather',
      group: 'Grandparents'
    }))
    const data = [childRecords, spouseRecord, siblingRecords, parentRecords, grandRecords].flat()
    return (
      <Row>
        <Column cols={12} v-show={data.length > 0}>
          <ActorTable
            headers={[{ text: 'Name', value: 'name' }, ...headers]}
            actors={data}
            group></ActorTable>
        </Column>
      </Row>
    )
  }
}
