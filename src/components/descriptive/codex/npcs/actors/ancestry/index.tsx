import { Column, Row } from '@/components/common/vuetify/layout'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { SimpleTabbedContent } from '../../../../../common/tabs/button_group'
import ExtendedAncestry from './Extended'
import ImmediateFamily from './Immediate'

@Component({
  components: {
    SimpleTabbedContent,
    ImmediateFamily,
    ExtendedAncestry
  }
})
export default class NPCAncestry extends tsx.Component<Record<string, unknown>> {
  public render() {
    return (
      <Row>
        <Column cols={12}>
          <SimpleTabbedContent
            content={{
              Immediate: <ImmediateFamily></ImmediateFamily>,
              Extended: <ExtendedAncestry></ExtendedAncestry>
            }}></SimpleTabbedContent>
        </Column>
      </Row>
    )
  }
}
