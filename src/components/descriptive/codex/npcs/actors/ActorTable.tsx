import DetailedTableRow, { DataTable, StrictHeaders } from '@/components/common/vuetify/table'
import { actor__location } from '@/models/npcs/actors'
import { actor__age, actor__expired, actor__life_phase } from '@/models/npcs/actors/stats/age'
import { profession__title } from '@/models/npcs/actors/stats/professions'
import { Actor } from '@/models/npcs/actors/types'
import { npc__health, npc__lvl } from '@/models/npcs/stats'
import { title_case } from '@/models/utilities/text'
import { decorate_text } from '@/models/utilities/text/decoration'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import StyledText from '../../../../common/text/StyledText'
import NPCHeathView from './stats/Health'

export interface ActorRecord {
  idx: number
  name: string
  profession: string
  level: number
  age: number
  phase: string
  lineage: string
  location: number
  culture: number
  relation?: string
  group?: string
}

export type ActorHeaders = StrictHeaders<keyof ActorRecord>[]

export const construct_actor_record = (actor: Actor): ActorRecord => {
  const expired = actor__expired(actor)
  return {
    idx: actor.idx,
    name: actor.name,
    profession: profession__title({ actor }),
    lineage: actor.lineage,
    culture: actor.culture,
    location: actor__location(actor)?.idx,
    age: actor__age({ actor }),
    phase: expired ? 'Deceased' : title_case(actor__life_phase({ actor })),
    level: npc__lvl(actor)
  }
}

const items_per_page = 10

const RawActorTable = DataTable<
  ActorRecord,
  {
    'item.age': { item: ActorRecord }
    'item.name': { item: ActorRecord }
    'item.culture': { item: ActorRecord }
    'item.location': { item: ActorRecord }
    'item.relation': { item: ActorRecord }
    'item.level': { item: ActorRecord }
  }
>()
@Component({
  components: {
    RawActorTable,
    NPCHeathView
  }
})
export default class ActorTable extends tsx.Component<{
  actors: ActorRecord[]
  headers: ActorHeaders
  hide_header?: boolean
  group?: boolean
  pagination?: boolean
  full_names?: boolean
}> {
  @Prop() private actors: ActorRecord[]
  @Prop() private headers: ActorHeaders
  @Prop() private group: boolean
  @Prop() private pagination: boolean
  @Prop() private full_names: boolean
  @Prop() private hide_header: boolean
  public render() {
    return (
      <RawActorTable
        headers={this.headers}
        items={this.actors}
        group-by={this.group ? 'group' : []}
        dense
        items-per-page={!this.pagination ? -1 : items_per_page}
        hide-default-footer={!this.pagination || this.actors.length < items_per_page}
        hide-default-header={this.hide_header}
        scopedSlots={{
          'item.name': ({ item }) => {
            const { name, idx } = item
            const actor = window.world.actors[idx]
            return (
              <DetailedTableRow
                title={
                  <StyledText
                    text={decorate_text({
                      label: this.full_names ? `${actor.name} ${actor.surname}` : name,
                      link: actor
                    })}
                  />
                }
                link
                subtitle={`${item.profession}`.toLowerCase()}></DetailedTableRow>
            )
          },
          'item.relation': ({ item }) => {
            return (
              <DetailedTableRow title={item.relation} subtitle={item.lineage}></DetailedTableRow>
            )
          },
          'item.culture': ({ item }) => {
            const culture = window.world.cultures[item.culture]
            return (
              <DetailedTableRow
                title={
                  <StyledText
                    text={decorate_text({
                      link: culture
                    })}
                  />
                }
                link
                subtitle={culture.species}></DetailedTableRow>
            )
          },
          'item.location': ({ item }) => {
            const location = window.world.locations[item.location]
            return (
              <DetailedTableRow
                title={
                  <StyledText
                    text={decorate_text({
                      link: location
                    })}
                  />
                }
                link
                subtitle={location.type}></DetailedTableRow>
            )
          },
          'item.age': ({ item }) => (
            <DetailedTableRow
              title={item.phase}
              subtitle={`${item.age} years old`}></DetailedTableRow>
          ),
          'item.level': ({ item }) => {
            const actor = window.world.actors[item.idx]
            const health = npc__health(actor)
            return (
              <NPCHeathView max={health.max} current={health.current} percent={health.percent} />
            )
          },
          'group.header': (params: { group: string; headers: string[] }) => (
            <td colspan={params.headers.length}>{params.group}</td>
          )
        }}></RawActorTable>
    )
  }
}
