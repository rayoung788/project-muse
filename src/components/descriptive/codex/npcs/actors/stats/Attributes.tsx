import { Column, Container, Row } from '@/components/common/vuetify/layout'
import { css_colors } from '@/styles/colors'
import { Actor } from '@/models/npcs/actors/types'
import { npc__describe_attributes } from '@/models/npcs/actors/stats/attributes'
import { decorate_text } from '@/models/utilities/text/decoration'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import StyledText from '../../../../../common/text/StyledText'

@Component
export default class AttributesView extends tsx.Component<{ attributes: Actor['attributes'] }> {
  @Prop() private attributes: Actor['attributes']
  public render() {
    const attributes = npc__describe_attributes(this.attributes)
    return (
      <Container fluid>
        <Row
          style={{
            color: css_colors.primary,
            'border-top': `2px solid #9C2B1B`
          }}>
          {attributes.map(({ attribute }, i) => (
            <Column key={i} cols={2} class='pa-0 text-center font-weight-bold mt-2'>
              {attribute}
            </Column>
          ))}
        </Row>
        <Row
          class='mt-0'
          style={{
            color: css_colors.primary,
            'border-bottom': `2px solid #9C2B1B`
          }}>
          {attributes.map(({ value, desc }, i) => {
            const negative = value - 10 < 0
            const round = negative ? Math.ceil : Math.floor
            return (
              <Column key={i} cols={2} class='pa-0 pb-1 text-center mt-2'>
                <StyledText
                  text={`${decorate_text({
                    label: value.toString(),
                    tooltip: desc,
                    color: css_colors.primary
                  })} (${negative ? '-' : '+'}${round(Math.abs(value - 10) / 2)})`}></StyledText>
              </Column>
            )
          })}
        </Row>
      </Container>
    )
  }
}
