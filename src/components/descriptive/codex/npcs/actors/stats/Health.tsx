import { decorate_text } from '@/models/utilities/text/decoration'
import { formatters } from '@/models/utilities/text/formatters'
import { scaleLinear } from 'd3'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import StyledText from '../../../../../common/text/StyledText'

const hpColorScale = scaleLinear()
  .domain([0, 0.5, 1])
  .range(['red', 'orange', 'green'] as any)

@Component
export default class NPCHeathView extends tsx.Component<{
  max: number
  current: number
  percent: number
}> {
  @Prop() private max: number
  @Prop() private current: number
  @Prop() private percent: number
  public render() {
    return (
      <StyledText
        text={`${decorate_text({
          label: `${this.current.toFixed(0)}/${this.max.toFixed(0)}`,
          color: this.percent === 0 ? 'gray' : undefined,
          tooltip:
            this.percent === 0
              ? 'defeated'
              : this.percent < 0.3
              ? 'near death'
              : this.percent < 0.6
              ? 'bloodied'
              : 'healthy'
        })} (${decorate_text({
          label: formatters.percent({ value: this.percent, precision: 0 }),
          color: this.percent === 0 ? 'gray' : hpColorScale(this.percent).toString()
        })})`}></StyledText>
    )
  }
}
