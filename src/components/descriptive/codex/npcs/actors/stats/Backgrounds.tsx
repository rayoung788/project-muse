import SectionList from '@/components/common/text/SectionList'
import StyledText from '@/components/common/text/StyledText'
import { Icon } from '@/components/common/vuetify/icon'
import { Divider } from '@/components/common/vuetify/layout'
import DetailedTableRow, { DataTable } from '@/components/common/vuetify/table'
import { actor__relation } from '@/models/npcs/actors'
import { background__find_events } from '@/models/npcs/actors/history/backgrounds'
import { actor__age, actor__life_phase } from '@/models/npcs/actors/stats/age'
import { actor__lifestyle, profession__title } from '@/models/npcs/actors/stats/professions'
import { Actor } from '@/models/npcs/actors/types'
import { describe_coarse_duration } from '@/models/utilities/math/time'
import { decorate_text } from '@/models/utilities/text/decoration'
import { actor__details } from '@/models/utilities/text/entities/actor'
import { formatters, format__date_range } from '@/models/utilities/text/formatters'
import { World } from '@/models/world/types'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

const items_per_page = 6

type Background = Actor['history']['backgrounds'][number] & {
  events: World['actor_events']
}

const BackgroundTable = DataTable<
  Background,
  {
    'item.start': { item: Background }
    'item.loc': { item: Background }
    'item.title': { item: Background }
  }
>()
@Component({
  components: {
    BackgroundTable
  }
})
export default class NPCBackgroundView extends tsx.Component<Record<string, unknown>> {
  private headers = [
    { text: 'Background', value: 'title' },
    { text: 'Location', value: 'loc' },
    { text: 'Time', value: 'start' },
    { text: '', value: 'data-table-expand' }
  ]
  private event_text(params: { actor: Actor; events: World['actor_events'] }) {
    const { actor, events } = params
    return events.map(e => {
      if (e.type === 'union') {
        const [spouse] = actor__relation({ actor, type: 'spouse' })
        return [
          'Union',
          <StyledText
            text={`${actor__details.name({ actor })} married ${actor__details.name({
              actor: spouse,
              link: true
            })} on ${formatters.date(e.time)}.`}></StyledText>
        ]
      }
      const child = window.world.actors[e.actor]
      return [
        'Child',
        <StyledText
          text={`${actor__details.name({ actor: child, link: true })} was born on ${formatters.date(
            e.time
          )}.`}></StyledText>
      ]
    }) as [string, JSX.Element][]
  }
  private skill_text(background: Actor['history']['backgrounds'][number]) {
    return background.skills
      .filter(skills => Object.entries(skills.exp).some(skill => skill[1] > 0))
      .map(skills => [
        format__date_range(skills),
        <StyledText
          text={Object.entries(skills.exp)
            .sort((a, b) => b[1] - a[1])
            .filter(skill => skill[1] > 0)
            .map(([skill, exp]) => decorate_text({ label: skill, tooltip: exp.toFixed(0) }))
            .join(', ')}></StyledText>
      ]) as [string, JSX.Element][]
  }
  public render() {
    const { actor } = view_module.codex
    const backgrounds = actor.history.backgrounds.map(background => {
      const events = background__find_events({
        actor,
        start: background.start,
        end: background.end
      })
        .filter(e => e.actor !== undefined)
        .sort((a, b) => a.time - b.time)
      return { ...background, events }
    })
    return (
      <BackgroundTable
        headers={this.headers}
        items={backgrounds}
        dense
        items-per-page={items_per_page}
        hide-default-footer={backgrounds.length < items_per_page}
        show-expand
        item-key='start'
        scopedSlots={{
          'item.start': ({ item }) => {
            const start = formatters.date(item.start)
            const end_time = Math.min(item.end || window.world.date, window.world.date)
            const end = item.end < window.world.date ? formatters.date(end_time) : 'Present'
            const duration = describe_coarse_duration(end_time - item.start)
            const start_age = actor__age({ actor, ref_date: item.start })
            const start_phase = actor__life_phase({ actor, ref_date: item.start }).toLowerCase()
            const end_age = actor__age({ actor, ref_date: end_time })
            const end_phase = actor__life_phase({ actor, ref_date: end_time }).toLowerCase()
            return (
              <DetailedTableRow
                title={`${start} - ${end}`}
                subtitle={
                  <StyledText
                    color={css_colors.subtitle}
                    text={`${duration} (age ${decorate_text({
                      label: start_age.toString(),
                      tooltip: start_phase
                    })} - ${decorate_text({
                      label: end_age.toString(),
                      tooltip: end_phase
                    })})`}></StyledText>
                }></DetailedTableRow>
            )
          },
          'item.loc': ({ item }) => {
            const link = window.world.locations[item.loc]
            return (
              <DetailedTableRow
                title={<StyledText text={decorate_text({ link })} />}
                subtitle={link.type}
                link></DetailedTableRow>
            )
          },
          'item.title': ({ item }) => {
            return (
              <DetailedTableRow
                title={
                  item.occupation
                    ? profession__title({ actor, time: item.start, ignore_child: true })
                    : 'Childhood'
                }
                subtitle={actor__lifestyle({ actor, time: item.start })}></DetailedTableRow>
            )
          },
          'item.data-table-expand': (params: {
            item: Background
            expand: (v: boolean) => void
            isExpanded: boolean
          }) => {
            const { item, expand, isExpanded } = params
            const canExpand = item.events.length > 0 || item.skills
            return (
              <Icon
                onClick={() => expand(!isExpanded)}
                disabled={!canExpand}
                color={canExpand ? css_colors.primary : undefined}>
                {isExpanded ? 'mdi-chevron-up' : 'mdi-chevron-down'}
              </Icon>
            )
          },
          'expanded-item': ({ item, headers }: { item: Background; headers: string[] }) => {
            const has_events = item.events.length > 0
            return (
              <td class='py-2' colspan={headers.length}>
                {has_events && (
                  <SectionList list={this.event_text({ actor, events: item.events })}></SectionList>
                )}
                {item.skills && has_events && <Divider class='my-2'></Divider>}
                {item.skills && <SectionList list={this.skill_text(item)}></SectionList>}
              </td>
            )
          }
        }}></BackgroundTable>
    )
  }
}
