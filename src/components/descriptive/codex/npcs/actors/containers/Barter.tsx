import ItemsTable, {
  barter_pov,
  ItemHeaders
} from '@/components/descriptive/codex/npcs/actors/containers/ItemsTable'
import { BaseButton, ButtonToggle } from '@/components/common/vuetify/button'
import { Column, Container, Divider, Row } from '@/components/common/vuetify/layout'
import { css_colors } from '@/styles/colors'
import { currency__convert } from '@/models/items/currency'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import CarryCapacity from './CarryCapacity'
import { item__category } from '@/models/items/types'
import { inventory__categories, inventory__filter } from '@/models/npcs/inventory'

@Component({
  components: {
    ItemsTable,
    CarryCapacity
  }
})
export default class BarterView extends tsx.Component<Record<string, unknown>> {
  private category: item__category = 'armor'
  public render() {
    const headers: ItemHeaders = [
      {
        value: 'name',
        text: this.category
      },
      {
        value: 'weight',
        text: 'Weight'
      },
      {
        value: 'value',
        text: 'Value'
      },
      {
        value: 'quantity',
        text: 'Quantity'
      }
    ]
    const merchant = view_module.codex.actor
    const categories = inventory__categories(merchant.inventory)
    const merchant_items = inventory__filter(merchant.inventory, this.category)
    const merchant_currency = currency__convert(merchant.inventory.currency)
    const avatar = view_module.avatar
    const avatar_items = inventory__filter(avatar.inventory, this.category)
    const avatar_currency = currency__convert(avatar.inventory.currency)
    return (
      <Container fluid>
        <Row align='center' justify='center'>
          <ButtonToggle
            v-model={this.category}
            mandatory
            shaped
            color={css_colors.primary}
            background-color={css_colors.accent}>
            {categories.map(cat => (
              <BaseButton tile small value={cat}>
                {cat}
              </BaseButton>
            ))}
          </ButtonToggle>
        </Row>
        <Divider class='mt-5'></Divider>
        <Row class='px-5 py-2'>
          <Column class='text-start'>{`Seller: ${merchant.name}`}</Column>
          <Column class='text-start'>{`Currency: ${merchant_currency}`}</Column>
          <Column class='text-end'>
            <CarryCapacity npc={merchant}></CarryCapacity>
          </Column>
        </Row>
        <Divider></Divider>
        <Row>
          <Column cols={12}>
            <ItemsTable
              headers={[
                ...headers,
                {
                  value: 'action',
                  text: 'Buy'
                }
              ]}
              items={merchant_items}
              barter={barter_pov.MERCHANT}
            />
          </Column>
        </Row>
        <Divider></Divider>
        <Row class='px-5 py-2'>
          <Column class='text-start'>{`Buyer: ${avatar.name}`}</Column>
          <Column class='text-start'>{`Currency: ${avatar_currency}`}</Column>
          <Column class='text-end'>
            <CarryCapacity npc={avatar}></CarryCapacity>
          </Column>
        </Row>
        <Divider></Divider>
        <Row>
          <Column cols={12}>
            <ItemsTable
              headers={[
                ...headers,
                {
                  value: 'action',
                  text: 'Sell'
                }
              ]}
              items={avatar_items}
              barter={barter_pov.BUYER}
            />
          </Column>
        </Row>
      </Container>
    )
  }
}
