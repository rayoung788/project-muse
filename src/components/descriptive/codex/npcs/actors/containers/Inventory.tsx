import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

import ItemsTable from '@/components/descriptive/codex/npcs/actors/containers/ItemsTable'
import PrimaryButton from '@/components/common/PrimaryButton'
import { BaseButton, ButtonToggle } from '@/components/common/vuetify/button'
import { Icon } from '@/components/common/vuetify/icon'
import { Column, Container, Divider, Row } from '@/components/common/vuetify/layout'
import { DataTable } from '@/components/common/vuetify/table'
import { item__lookup } from '@/models/items'
import { item__is_armor } from '@/models/items/armor'
import { currency__convert } from '@/models/items/currency'
import { item__category } from '@/models/items/types'
import {
  actor__equip_best,
  actor__unequip_item,
  equipable__slot
} from '@/models/npcs/actors/equipment'
import { Equipable, equipable_slot } from '@/models/npcs/actors/equipment/types'
import { inventory__categories, inventory__filter } from '@/models/npcs/inventory'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'

import CarryCapacity from './CarryCapacity'

const equipment_header = [
  {
    value: 'slot',
    text: 'Equipment'
  },
  {
    value: 'name',
    text: 'Name'
  },
  {
    value: 'weight',
    text: 'Weight'
  },
  {
    value: 'value',
    text: 'Value'
  },
  {
    value: 'action',
    text: 'Action'
  }
]

interface EquipmentRecord {
  name: string
  slot: equipable_slot
  group: string
  weight: number
  value: number
}

const EquipmentTable = DataTable<
  EquipmentRecord,
  {
    'item.value': { item: EquipmentRecord }
    'item.weight': { item: EquipmentRecord }
    'item.action': { item: EquipmentRecord }
  }
>()
@Component({
  components: {
    EquipmentTable,
    ItemsTable,
    CarryCapacity
  }
})
export default class InventoryView extends tsx.Component<Record<string, unknown>> {
  private category: item__category = 'armor'
  private get equipment() {
    const { actor } = view_module.codex
    const items: EquipmentRecord[] = Object.values<Equipable>(actor.equipment)
      .filter(item => item)
      .map(item => {
        const details = item__lookup[item.tag]
        return {
          name: item.tag,
          slot: equipable__slot(item),
          weight: details.weight,
          value: details.value(item),
          group: item__is_armor(item) ? 'Armor' : 'Weapons'
        }
      })
    return (
      <EquipmentTable
        dense
        headers={equipment_header}
        items={items}
        group-by='group'
        scopedSlots={{
          'item.value': ({ item }: { item: EquipmentRecord }) => currency__convert(item.value),
          'item.weight': ({ item }: { item: EquipmentRecord }) => `${item.weight} lbs`,
          'item.action': ({ item }: { item: EquipmentRecord }) => {
            return (
              <span>
                <Icon
                  small
                  color={css_colors.primary}
                  onClick={() => actor__unequip_item({ actor, slot: item.slot })}>
                  mdi-minus-circle
                </Icon>
              </span>
            )
          },
          'group.header': (params: { group: string; headers: string[] }) => (
            <td colspan={params.headers.length}>{params.group}</td>
          )
        }}
      />
    )
  }
  public render() {
    const npc = view_module.codex.actor
    const items = inventory__filter(npc.inventory, this.category)
    const { currency } = npc.inventory
    const categories = inventory__categories(npc.inventory)
    return (
      <Container fluid>
        <Row align='center' justify='center'>
          <ButtonToggle
            v-model={this.category}
            mandatory
            shaped
            color={css_colors.primary}
            background-color={css_colors.accent}>
            {categories.map(cat => (
              <BaseButton tile small value={cat}>
                {cat}
              </BaseButton>
            ))}
          </ButtonToggle>
        </Row>
        <Divider class='mt-5'></Divider>
        <Row class='px-5 py-2'>
          <Column cols={4} class='text-start pt-4'>{`Currency: ${currency__convert(
            currency
          )}`}</Column>
          <Column cols={4}>
            <PrimaryButton text='Equip Best' click={() => actor__equip_best(npc)} />
          </Column>
          <Column cols={4} class='text-end pt-4'>
            <CarryCapacity npc={npc}></CarryCapacity>
          </Column>
        </Row>
        <Divider></Divider>
        <Row>
          <Column cols={12}>
            <ItemsTable
              headers={[
                {
                  value: 'name',
                  text: `${this.category}`
                },
                {
                  value: 'weight',
                  text: 'Weight'
                },
                {
                  value: 'value',
                  text: 'Value'
                },
                {
                  value: 'quantity',
                  text: 'Quantity'
                },
                {
                  value: 'action',
                  text: 'Action'
                }
              ]}
              items={items}
              equip
              remove
            />
          </Column>
        </Row>
        <Row>
          <Column cols={12}>{this.equipment}</Column>
        </Row>
      </Container>
    )
  }
}
