import { Icon } from '@/components/common/vuetify/icon'
import { DataTable, StrictHeaders } from '@/components/common/vuetify/table'
import { css_colors } from '@/styles/colors'
import { currency__convert } from '@/models/items/currency'
import { Actor } from '@/models/npcs/actors/types'
import { npc__remove_item, npc__add_item } from '@/models/npcs/inventory'
import { province__markets } from '@/models/regions/provinces/networks/trade_goods'
import { Province } from '@/models/regions/provinces/types'
import { formatters } from '@/models/utilities/text/formatters'
import { decorate_text } from '@/models/utilities/text/decoration'
import { view_module } from '@/store/view'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import StyledText from '../../../../../common/text/StyledText'
import { actor__location } from '@/models/npcs/actors'
import { Item } from '@/models/items/types'
import { item__key, item__lookup } from '@/models/items'
import { actor__equip_item, item__is_equipable } from '@/models/npcs/actors/equipment'

export const enum barter_pov {
  MERCHANT = 'merchant',
  BUYER = 'buyer'
}

const local_price_mod = (province: Province, item: Item) => {
  const { markets } = item__lookup[item.tag]
  const trade_goods = province__markets(province)
  return (
    1 +
    (markets.length > 0
      ? markets.reduce((sum, market) => {
          const { rarity, demand } = trade_goods[market]
          return sum + rarity + demand
        }, 0) / markets.length
      : 0)
  )
}

const item_exchange = (params: {
  seller: Actor
  buyer: Actor
  item: Item
  quantity?: number
  province: Province
}) => {
  const { seller, buyer, item, quantity = 1, province } = params
  const exchange = npc__remove_item({
    npc: seller,
    key: item__key(item),
    quantity: quantity
  })
  npc__add_item({ npc: buyer, item: exchange })
  const local_adjust = local_price_mod(province, item)
  const details = item__lookup[exchange.tag]
  const value = Math.floor(details.value(exchange) * exchange.quantity * local_adjust)
  buyer.inventory.currency -= value
  seller.inventory.currency += value
}

const ItemTable = DataTable<
  Item,
  {
    'item.value': { item: Item }
    'item.weight': { item: Item }
    'item.action': { item: Item }
  }
>()

export type ItemHeaders = StrictHeaders<'value' | 'weight' | 'quantity' | 'name' | 'action'>[]

const items_per_page = 10

@Component({
  components: {
    ItemTable
  }
})
export default class ItemsTable extends tsx.Component<{
  items: Item[]
  headers: ItemHeaders
  equip?: boolean
  remove?: boolean
  barter?: barter_pov
}> {
  @Prop() private items: Item[]
  @Prop() private headers: ItemHeaders
  @Prop() private equip?: boolean
  @Prop() private remove?: boolean
  @Prop() private barter?: barter_pov
  public render() {
    const actor = view_module.codex.actor
    const loc = actor__location(actor)
    const province = window.world.provinces[loc.province]
    const avatar = view_module.avatar
    const avatar_coins = avatar.inventory.currency
    const merchant_coins = actor.inventory.currency
    const merchant = this.barter === barter_pov.MERCHANT
    const coins = merchant ? avatar_coins : merchant_coins
    const buyer = merchant ? avatar : actor
    const seller = merchant ? actor : avatar
    return (
      <ItemTable
        dense
        items-per-page={items_per_page}
        hide-default-footer={this.items.length < items_per_page}
        headers={this.headers}
        items={this.items}
        scopedSlots={{
          'item.value': ({ item }) => {
            const value = item__lookup[item.tag].value(item)
            const base = currency__convert(value)
            if (!this.barter) return base
            const adjust = local_price_mod(province, item)
            const local_price = currency__convert(Math.floor(value * adjust))
            return (
              <StyledText
                text={decorate_text({
                  label: local_price,
                  tooltip: `${base} (${formatters.percent({ value: adjust })})`
                })}
              />
            )
          },
          'item.weight': ({ item }) => `${item__lookup[item.tag].weight} lbs`,
          'item.action': ({ item }) => {
            const value = item__lookup[item.tag].value(item)
            return (
              <span>
                {this.remove && (
                  <Icon
                    small
                    color={css_colors.primary}
                    onClick={() =>
                      npc__remove_item({ npc: actor, key: item__key(item), quantity: 1 })
                    }>
                    mdi-close-circle
                  </Icon>
                )}
                {this.equip && item__is_equipable(item) && (
                  <Icon
                    small
                    color='green'
                    class='ml-1'
                    onClick={() => actor__equip_item({ actor, item, force: true })}>
                    mdi-plus-circle
                  </Icon>
                )}
                {this.barter && (
                  <Icon
                    small
                    color={css_colors.primary}
                    disabled={value > coins}
                    onClick={() => item_exchange({ buyer, seller, item, province: province })}>
                    mdi-sack
                  </Icon>
                )}
              </span>
            )
          }
        }}
      />
    )
  }
}
