import { actor__carry_weight } from '@/models/npcs/actors'
import { Actor } from '@/models/npcs/actors/types'
import { formatters } from '@/models/utilities/text/formatters'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

interface CarryCapacityProps {
  npc: Actor
}

@Component
export default class CarryCapacity extends tsx.Component<CarryCapacityProps> {
  @Prop() private npc: Actor
  public render() {
    const npc = this.npc
    const carry = actor__carry_weight(npc)
    return (
      <div>
        {`Capacity: ${carry} / ${npc.carry_capacity} `}(
        <span class={carry > npc.carry_capacity ? 'red--text' : ''}>
          {`${formatters.percent({
            value: carry / npc.carry_capacity
          })}`}
        </span>
        )
      </div>
    )
  }
}
