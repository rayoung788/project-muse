import SectionList from '@/components/common/text/SectionList'
import { culture__regions, culture__religion } from '@/models/npcs/species/humanoids/cultures'
import { decorate_text } from '@/models/utilities/text/decoration'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { Column, Row } from '../../../../common/vuetify/layout'
import CodexPage from '../..'
import StyledText from '../../../../common/text/StyledText'
import LanguageView from './LanguageView'
import CulturalPhysique from './Physique'
@Component({
  components: {
    // custom
    CodexPage,
    LanguageView,
    CulturalPhysique
  }
})
export default class CultureView extends tsx.Component<Record<string, never>> {
  private get general() {
    const culture = view_module.codex.culture
    const { lineage } = culture
    const religion = window.world.religions[culture.religion]
    return (
      <Row>
        <Column cols={7}>
          <SectionList
            list={[
              [
                `Regions`,
                <StyledText
                  text={culture__regions(culture)
                    .map(region => decorate_text({ link: region, tooltip: region.development }))
                    .join(', ')}></StyledText>
              ],
              [
                `Neighbors`,
                <StyledText
                  text={culture.neighbors
                    .map(n => {
                      const neighbor = window.world.cultures[n]
                      return decorate_text({ link: neighbor, tooltip: neighbor.subspecies })
                    })
                    .join(', ')}></StyledText>
              ]
            ]}></SectionList>
        </Column>
        <Column cols={5}>
          <SectionList
            list={[
              [`Lineage`, lineage === 'male' ? 'Patrilineal' : 'Matrilineal'],
              [
                `Religion`,
                <StyledText
                  text={decorate_text({
                    label: culture__religion(culture),
                    link: religion
                  })}></StyledText>
              ]
            ]}></SectionList>
        </Column>
      </Row>
    )
  }
  public render() {
    const culture = view_module.codex.culture
    return (
      <CodexPage
        title={culture.name}
        subtitle={`(${culture.idx}) Culture (${culture.subspecies.toLowerCase()})`}
        content={this.general}
        tabs={{
          Language: { element: <LanguageView></LanguageView> },
          Appearance: { element: <CulturalPhysique></CulturalPhysique> }
        }}></CodexPage>
    )
  }
}
