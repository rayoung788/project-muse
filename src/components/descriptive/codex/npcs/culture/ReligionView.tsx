import SectionList from '@/components/common/text/SectionList'
import { css_colors } from '@/styles/colors'
import { decorate_text } from '@/models/utilities/text/decoration'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { Column, Row } from '../../../../common/vuetify/layout'
import CodexPage from '../..'
import StyledText from '../../../../common/text/StyledText'
import { Religion } from '@/models/npcs/species/humanoids/religions/types'
import { title_case } from '@/models/utilities/text'
import TraitDisplay from '@/components/common/Traits'
import { religion__trait_colors } from '@/models/npcs/species/humanoids/religions/traits'

const organization_text: Record<Religion['organization'], string> = {
  geographic:
    "The faith is divided up into geographic parishes, episcopal sees, towns, administrative regions, or other chunks. Each section has its own hierarchy and leader who answers to the faith's head.",
  doctrinal:
    "There are multiple similar but different doctrinal schools within the faith, or sub-sects that can cooperate with each other. Each school has its own hierarchy, and its members are responsible to their school's heads regardless of where they're located.",
  transmission:
    "The original saints or holy founders each consecrated priests, who consecrated more priests in turn down on to the present day. Every cleric who traces their consecration back to a particular saint is part of that saint's hierarchy.",
  ethnicity:
    'The faith embraces more than one major ethnicity among its believers, and each ethnic group has its own hierarchy and leadership chosen from among it.',
  functional:
    'The faith has several separate hierarchies for different functions within the faith, such as administration, preaching, military affairs, doctrinal purity, or other roles. Priests answer to the hierarchy appropriate to their current assigned duty.'
}

const leadership_text: Record<Religion['leadership'], string> = {
  pontiff:
    'There is a single pontiff with a layer of upper clergy and temple heads beneath them, who have a layer of minor clergy serving them',
  bishops:
    'There are multiple pontiffs, friendly or otherwise, with subordinate clergy obedient to their own pontiff and perhaps cooperative with others.',
  priests:
    'Each holy man or woman is the autonomous leader of their own branch of the sect, with however many followers they can gather.',
  none: 'There is no official clergy; some believers may take up special roles or provide teaching, but they are not qualitatively different from others.',
  secular:
    'The church is entirely part of the secular structure of the land, its clergy no more than officials appointed by the government to their roles.'
}

const clergy_family: Record<Religion['clergy']['family'], string> = {
  none: 'never have families (celibacy is required)',
  rare: 'seldomly have families (celibacy is encouraged)',
  normal: 'typically have normal sized families',
  large: 'typically have large families (children are encouraged)'
}
@Component({
  components: {
    CodexPage
  }
})
export default class ReligionView extends tsx.Component<Record<string, unknown>> {
  public render() {
    const { religion } = view_module.codex
    if (!religion) return <span>nothing here :)</span>
    const { organization, leadership, clergy } = religion
    return (
      <CodexPage
        title={religion.name}
        subtitle={
          <StyledText
            color={css_colors.subtitle}
            text={`[${religion.idx}] Religion (${decorate_text({
              label: religion.type,
              color: css_colors.subtitle
            })})`}></StyledText>
        }
        content={
          <Row>
            <Column cols={12}>
              <SectionList
                list={[
                  [
                    `Cultures`,
                    <StyledText
                      text={religion.cultures
                        .map(idx => {
                          const culture = window.world.cultures[idx]
                          return decorate_text({
                            link: culture,
                            tooltip: culture.subspecies
                          })
                        })
                        .join(', ')}></StyledText>
                  ],
                  [
                    `Organization`,
                    <span>
                      <i>{title_case(organization)}.</i> {organization_text[organization]}
                    </span>
                  ],
                  [
                    `Leadership`,
                    <span>
                      <i>{title_case(leadership)}.</i> {leadership_text[leadership]}
                    </span>
                  ],
                  [
                    `Clergy`,
                    <span>
                      The clergy{' '}
                      {clergy.gender
                        ? `is restricted to ${clergy.gender === 'male' ? 'men' : 'women'}`
                        : 'does not have any gender restrictions'}
                      . Priests {clergy_family[clergy.family]}.
                    </span>
                  ]
                ]}></SectionList>
            </Column>
            {religion.traits.length > 0 && (
              <Column cols={12}>
                <TraitDisplay
                  traits={religion.traits}
                  colors={religion__trait_colors}></TraitDisplay>
              </Column>
            )}
          </Row>
        }></CodexPage>
    )
  }
}
