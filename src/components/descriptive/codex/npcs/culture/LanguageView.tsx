import SectionList from '@/components/common/text/SectionList'
import { BaseButton } from '@/components/common/vuetify/button'
import { Icon } from '@/components/common/vuetify/icon'
import { Column, Row } from '@/components/common/vuetify/layout'
import { phoneme_catalog } from '@/models/npcs/species/humanoids/languages/types'
import { lang__first, lang__last } from '@/models/npcs/species/humanoids/languages/words/actors'
import { range } from '@/models/utilities/math'
import { formatters } from '@/models/utilities/text/formatters'
import { view_module } from '@/store/view'
import { Component, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

const phoneme_mapping: Record<string, string> = {
  [phoneme_catalog.START_CONSONANT]: 'C (start)',
  [phoneme_catalog.MIDDLE_CONSONANT]: 'C (middle)',
  [phoneme_catalog.END_CONSONANT]: 'C (end)',
  [phoneme_catalog.START_VOWEL]: 'V (start)',
  [phoneme_catalog.FRONT_VOWEL]: 'V (front)',
  [phoneme_catalog.MIDDLE_VOWEL]: 'V (middle)',
  [phoneme_catalog.BACK_VOWEL]: 'V (back)',
  [phoneme_catalog.END_VOWEL]: 'V (end)'
}
@Component
export default class LanguageView extends tsx.Component<Record<string, unknown>> {
  private female_names = ''
  private male_names = ''
  private surnames = ''
  public mounted() {
    this.refresh_names()
  }
  private get culture() {
    return view_module.codex.culture
  }
  @Watch('culture')
  private refresh_names() {
    const { language } = this.culture
    const { suffix, patronymic } = language.surnames
    const { male, female } = suffix
    this.female_names = Array.from(
      new Set(range(15).map(() => lang__first(language, 'female')))
    ).join(', ')
    this.male_names = Array.from(new Set(range(15).map(() => lang__first(language, 'male')))).join(
      ', '
    )
    this.surnames = patronymic
      ? `${male.join(', ')} (male) / ${female.join(', ')} (female)`
      : Array.from(new Set(range(30).map((_, i) => lang__last(language, i > 20)))).join(', ')
  }
  public render() {
    const { language, lineage: patrilineal } = this.culture
    return (
      <Row justify='center' align='center'>
        <Column cols={2}>
          <BaseButton icon small class='primary--text' onClick={() => this.refresh_names()}>
            <Icon>mdi-refresh</Icon>
          </BaseButton>
        </Column>
        <Column cols={12}>
          <SectionList
            list={[
              ['Male names', this.male_names],
              ['Female names', this.female_names],
              [
                `Last names${
                  language.surnames.patronymic
                    ? ` (${patrilineal === 'male' ? 'P' : 'M'}atronymic)`
                    : ''
                }`,
                this.surnames
              ]
            ]}></SectionList>
        </Column>
        <Column cols={12}>
          <SectionList
            list={[
              ...Object.entries(language.phonemes).map(([k, v]) => {
                return [
                  phoneme_mapping[k],
                  v
                    .concat()
                    .sort((a, b) => b.w - a.w)
                    .map(
                      ({ v: letter, w: percent }) =>
                        `${letter} (${formatters.percent({ value: percent, precision: 1 })})`
                    )
                    .join(', ')
                ] as [string, string]
              })
            ]}></SectionList>
        </Column>
      </Row>
    )
  }
}
