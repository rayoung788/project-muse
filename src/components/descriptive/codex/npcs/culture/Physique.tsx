import { Column, Row } from '@/components/common/vuetify/layout'
import { DataTable } from '@/components/common/vuetify/table'
import { life_cycle, life_phases } from '@/models/npcs/actors/stats/age/life_phases'
import { genders } from '@/models/npcs/actors/stats/appearance/gender'
import { species__by_culture } from '@/models/npcs/species/humanoids/taxonomy'
import { color__adjacent } from '@/models/utilities/colors'
import { compute_weight, imperial_height } from '@/models/utilities/math'
import { proper_list, proper_sentences, title_case } from '@/models/utilities/text'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import SectionList from '../../../../common/text/SectionList'

const compute_weight_rounded = (...params: Parameters<typeof compute_weight>) =>
  Math.round(compute_weight(...params))

const WeightTable = DataTable<
  { weight: string; short: string; average: string; tall: string },
  Record<string, unknown>
>()

const AgeTable = DataTable<Record<life_phases, string>, Record<string, unknown>>()

@Component({
  components: {
    WeightTable,
    AgeTable
  }
})
export default class CulturalPhysique extends tsx.Component<Record<string, unknown>> {
  private get ages() {
    const { ages } = species__by_culture(view_module.codex.culture)
    const ranges: Record<life_phases, string> = {
      childhood: `0-${ages.childhood}`,
      adolescence: `${ages.childhood + 1}-${ages.adolescence}`,
      'young adult': `${ages.adolescence + 1}-${ages['young adult']}`,
      adult: `${ages['young adult'] + 1}-${ages.adult}`,
      'middle age': `${ages.adult + 1}-${ages['middle age']}`,
      old: `${ages['middle age'] + 1}-${ages.old}`,
      venerable: `${ages.old}+`
    }
    return {
      headers: life_cycle.map(life_phase => ({
        text: life_phase,
        value: life_phase,
        sortable: false,
        align: 'center' as const
      })),
      data: [ranges]
    }
  }
  private compute_weight_by_gender(gender: genders, [short_h, average_h, tall_h]: number[][]) {
    const culture = view_module.codex.culture
    const { boundary, mean } = species__by_culture(culture).bmi
    const underweight = mean - boundary * 1.5
    const average = mean
    const overweight = mean + boundary * 1.5
    const headers = [
      {
        value: 'weight',
        text: `Weight / Height (${title_case(gender)})`,
        sortable: false
      },
      {
        value: 'short',
        text: `Short (${imperial_height(short_h[0])}-${imperial_height(short_h[1])})`,
        sortable: false
      },
      {
        value: 'average',
        text: `Average (${imperial_height(average_h[0])}-${imperial_height(average_h[1])})`,
        sortable: false
      },
      {
        value: 'tall',
        text: `Tall (${imperial_height(tall_h[0])}-${imperial_height(tall_h[1])})`,
        sortable: false
      }
    ]
    const items = [
      {
        weight: `Underweight (${underweight.toFixed(0)})`,
        short: `${compute_weight_rounded(short_h[0], underweight)}-${compute_weight_rounded(
          short_h[1],
          underweight
        )}`,
        average: `${compute_weight_rounded(average_h[0], underweight)}-${compute_weight_rounded(
          average_h[1],
          underweight
        )}`,
        tall: `${compute_weight_rounded(tall_h[0], underweight)}-${compute_weight_rounded(
          tall_h[1],
          underweight
        )}`
      },
      {
        weight: `Normal (${average.toFixed(0)})`,
        short: `${compute_weight_rounded(short_h[0], average)}-${compute_weight_rounded(
          short_h[1],
          average
        )}`,
        average: `${compute_weight_rounded(average_h[0], average)}-${compute_weight_rounded(
          average_h[1],
          average
        )}`,
        tall: `${compute_weight_rounded(tall_h[0], average)}-${compute_weight_rounded(
          tall_h[1],
          average
        )}`
      },
      {
        weight: `Overweight (${overweight.toFixed(0)})`,
        short: `${compute_weight_rounded(short_h[0], overweight)}-${compute_weight_rounded(
          short_h[1],
          overweight
        )}`,
        average: `${compute_weight_rounded(average_h[0], overweight)}-${compute_weight_rounded(
          average_h[1],
          overweight
        )}`,
        tall: `${compute_weight_rounded(tall_h[0], overweight)}-${compute_weight_rounded(
          tall_h[1],
          overweight
        )}`
      }
    ]
    return { headers, items }
  }
  public render() {
    const culture = view_module.codex.culture
    const { male, female, std } = species__by_culture(culture).heights
    const short_male = [male - std * 2, male - std]
    const average_male = [male - std, male + std]
    const tall_male = [male + std, male + std * 2]
    const male_weights = this.compute_weight_by_gender('male', [
      short_male,
      average_male,
      tall_male
    ])
    const short_female = [female - std * 2, female - std]
    const average_female = [female - std, female + std]
    const tall_female = [female + std, female + std * 2]
    const female_weights = this.compute_weight_by_gender('female', [
      short_female,
      average_female,
      tall_female
    ])
    const ages = this.ages
    const { skin, hair, eyes } = culture.appearance
    const appearance = [
      [
        `${title_case(skin.type)}${skin.texture ? ` (${title_case(skin.texture)})` : ''}`,
        proper_sentences(`${proper_list(skin.colors, 'or')}.`)
      ]
    ]
    if (hair)
      appearance.push([
        'Hair',
        proper_sentences(
          `${proper_list(hair.colors, 'or')}. ${proper_list(hair.textures, 'or')} texture.`
        )
      ])
    appearance.push(['Eyes', proper_sentences(`${proper_list(eyes.colors, 'or')}.`)])
    appearance.push([
      `Fashion`,
      proper_sentences(`${proper_list(color__adjacent({ color: culture.fashion.color }), 'and')}.`)
    ])
    return (
      <Row>
        <Column cols={12}>
          <SectionList class='ml-4' list={appearance as [string, string][]}></SectionList>
        </Column>
        <Column cols={12}>
          <WeightTable
            dense
            disable-pagination
            hide-default-footer
            headers={male_weights.headers}
            items={male_weights.items}
          />
        </Column>
        <Column cols={12}>
          <WeightTable
            dense
            disable-pagination
            hide-default-footer
            headers={female_weights.headers}
            items={female_weights.items}
          />
        </Column>
        <Column cols={12}>
          <AgeTable
            dense
            disable-pagination
            hide-default-footer
            headers={ages.headers}
            items={ages.data}
          />
        </Column>
      </Row>
    )
  }
}
