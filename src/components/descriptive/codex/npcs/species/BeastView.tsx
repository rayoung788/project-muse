import { Column, Row } from '@/components/common/vuetify/layout'
import { npc__opposite_gender } from '@/models/npcs/actors/stats/appearance/gender'
import { beast__decorate } from '@/models/npcs/species/beasts'
import { beast__skin_type } from '@/models/npcs/species/beasts/appearance'
import { beast__describe_social } from '@/models/npcs/species/beasts/behavior/social'
import { beast__describe_temperament } from '@/models/npcs/species/beasts/behavior/temperament'
import { beast__describe_territory } from '@/models/npcs/species/beasts/behavior/territory'
import { beast__finalize } from '@/models/npcs/species/beasts/finalize'
import { beast__families } from '@/models/npcs/species/beasts/taxonomy/types'
import { beast__trait_lookup } from '@/models/npcs/species/beasts/traits'
import { BeastTrait } from '@/models/npcs/species/beasts/traits/types'
import { Beast } from '@/models/npcs/species/beasts/types'
import { primordial__decorate } from '@/models/npcs/species/primordials'
import { species__length, species__size_rank, species__weight } from '@/models/npcs/species/size'
import { describe_coarse_duration } from '@/models/utilities/math/time'
import { rarity__rank } from '@/models/utilities/quality'
import { proper_list, title_case } from '@/models/utilities/text'
import { decorate_text } from '@/models/utilities/text/decoration'
import { entity_placeholder, replace_placeholders } from '@/models/utilities/text/placeholders'
import { view_module } from '@/store/view'
import { css_colors } from '@/styles/colors'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import SectionList from '../../../../common/text/SectionList'
import StyledText from '../../../../common/text/StyledText'
import CodexPage from '../..'
import AttributesView from '../actors/stats/Attributes'
import { species__describe_symbiosis } from './symbiosis'

const beast_placeholder = replace_placeholders({ primary: 'This species', secondary: 'It' })

const describe_traits = (params: { traits: Beast['traits']; type?: BeastTrait['type'] }) => {
  const { traits, type } = params
  const filtered = traits.filter(trait => {
    const template = beast__trait_lookup[trait.tag]
    return type ? template.type === type : !template.type
  })
  return filtered.length > 0 ? ` ${filtered.map(trait => trait.text).join(' ')}` : ''
}

@Component({
  components: {
    CodexPage,
    AttributesView
  }
})
export default class BeastView extends tsx.Component<Record<string, unknown>> {
  public get appearance() {
    const species = view_module.codex.beast
    const { gender_variation, appearance, traits } = species
    const { primary, size, primary_color, primary_shades, secondary_color, secondary_shades } =
      gender_variation
    const { skin, mimicry: mimic } = appearance
    const colors = primary_color ?? proper_list(skin.color, 'or')
    const secondary = npc__opposite_gender(primary)
    const appearance_traits = describe_traits({ traits, type: 'appearance' })
    const skin_type = beast__skin_type(species)
    const mimicry = window.world.beasts[mimic?.species]
    const coloration =
      mimic?.role === 'mimic'
        ? ` ${entity_placeholder} ${decorate_text({
            label: 'mimics',
            tooltip: mimic.type
          })} the appearance of the ${decorate_text({
            label: mimicry.name.toLowerCase(),
            link: mimicry,
            tooltip: title_case(mimicry.genus.name)
          })}.`
        : ` They have ${colors} ${skin_type}.${
            primary_shades
              ? ` ${title_case(primary)}s are ${primary_shades} than ${secondary}s.`
              : ''
          }${
            secondary_shades
              ? ` ${title_case(secondary)}s are ${secondary_shades} than ${primary}s.`
              : ''
          }${
            secondary_color
              ? ` ${title_case(
                  secondary
                )}s have ${secondary_color} ${skin_type} in contrast to ${primary}s.`
              : ''
          }`
    const model =
      mimic?.role === 'model'
        ? ` ${entity_placeholder} is ${decorate_text({
            label: 'mimicked',
            tooltip: mimic.type
          })} by the ${decorate_text({
            label: mimicry.name.toLowerCase(),
            link: mimicry,
            tooltip: title_case(mimicry.genus.name)
          })}.`
        : ''
    return (
      <StyledText
        text={beast_placeholder(
          `An adult ${primary} is on average ${species__length(
            species.length
          )} long and weighs ${species__weight(species)}. ${title_case(secondary)}s are ${
            size === 1
              ? `the same size as`
              : `${size > 0.8 ? 'slightly' : 'noticeably'} smaller than`
          } ${primary}s.${coloration}${appearance_traits}${model}`
        )}></StyledText>
    )
  }
  public get life_cycle() {
    const species = view_module.codex.beast
    const { brood_size, traits, family } = species
    const max_life = describe_coarse_duration(species.life_span)
    const maturation = describe_coarse_duration(species.maturation)
    const gestation = describe_coarse_duration(species.gestation)
    const method =
      beast__families[family].reproduction === 'eggs'
        ? `lays clutches containing ${
            typeof brood_size === 'number'
              ? `${brood_size} egg${brood_size === 1 ? '' : 's'}`
              : `${brood_size[0]}-${brood_size[1]} eggs`
          }. The incubation period is ${gestation}`
        : `gives live birth to litters containing ${
            typeof brood_size === 'number'
              ? `${brood_size} ${brood_size === 1 ? 'child' : 'children'}`
              : `${brood_size[0]}-${brood_size[1]} children`
          }. The gestation period is ${gestation}`
    const reproduction_traits = describe_traits({ traits, type: 'reproduction' })
    return `The normal lifespan of this species is ${max_life}. They are considered mature after ${maturation}. This species ${method}.${beast_placeholder(
      reproduction_traits
    )}`
  }
  public get behavior() {
    const species = view_module.codex.beast
    const { activity_period, traits } = species
    const social = beast__describe_social(species)
    const territory = beast__describe_territory(species)
    const territory_traits = describe_traits({ traits, type: 'territory' })
    const habitat_traits = describe_traits({ traits, type: 'habitat' })
    const behavior_traits = describe_traits({ traits, type: 'behavior' })
    const use_traits = describe_traits({ traits, type: 'use' })
    const temperament = beast__describe_temperament(species)
    return (
      <span>
        <i>{title_case(activity_period)}</i>.{' '}
        <StyledText
          text={beast_placeholder(
            `${social} ${territory}${territory_traits} ${temperament}${habitat_traits}${behavior_traits}${use_traits}`
          )}></StyledText>
      </span>
    )
  }
  public get symbiosis() {
    const species = view_module.codex.beast
    const symbiosis_text = species__describe_symbiosis(species)
    return beast_placeholder(symbiosis_text)
  }
  public get diet() {
    const species = view_module.codex.beast
    const { diet, traits } = species
    const combat_traits = describe_traits({ traits, type: 'combat' })
    return (
      <span>
        <i>{title_case(diet.type)}</i>.{' '}
        <StyledText text={beast_placeholder(`${diet.text}${combat_traits}`)}></StyledText>
      </span>
    )
  }
  public render() {
    const species = view_module.codex.beast
    if (!species) return <span>none</span>
    beast__finalize(species)
    const { environment, semi_aquatic } = species
    const data: [string, JSX.Element | string][] = [
      [
        'Environment',
        <StyledText
          text={`${
            semi_aquatic
              ? `${decorate_text({
                  label: `${environment.climate} Coast`,
                  tooltip: environment.terrain
                })}`
              : environment.key
          }`}></StyledText>
      ],
      [
        'Regions',
        <StyledText
          text={species.regions
            .map(r => decorate_text({ link: window.world.regions[r] }))
            .join(', ')}></StyledText>
      ],
      ['Appearance', this.appearance],
      ['Life Cycle', this.life_cycle],
      ['Traits', this.behavior],
      ['Diet', this.diet]
    ]
    const symbiosis = this.symbiosis
    if (symbiosis) data.push(['Symbiosis', <StyledText text={symbiosis}></StyledText>])
    const { predators, prey } = species
    if (prey.length > 0) {
      data.push([
        'Prey',
        <StyledText
          text={prey
            .map(i => {
              const prey = window.world.beasts[i]
              return beast__decorate(prey)
            })
            .join(', ')}></StyledText>
      ])
    }
    if (predators.length > 0) {
      data.push([
        'Predators',
        <StyledText
          text={predators
            .map(({ idx, type }) => {
              return type === 'beast'
                ? beast__decorate(window.world.beasts[idx])
                : primordial__decorate(window.world.primordials[idx])
            })
            .join(', ')}></StyledText>
      ])
    }
    return (
      <CodexPage
        title={species.name}
        subtitle={
          <StyledText
            color={css_colors.subtitle}
            text={`(${species.idx}) ${species__size_rank(species.size)} ${
              species.tag
            } (${rarity__rank(species.rarity)}, ${decorate_text({
              label: species.family,
              tooltip: title_case(species.genus.name),
              color: css_colors.subtitle
            })})`}></StyledText>
        }
        content={
          <Row>
            <Column cols={12} class='pb-0'>
              <SectionList list={data}></SectionList>
            </Column>
          </Row>
        }></CodexPage>
    )
  }
}
