import { species_symbiotic_partner } from '@/models/npcs/species'
import { beast__decorate } from '@/models/npcs/species/beasts'
import { Beast } from '@/models/npcs/species/beasts/types'
import { primordial__decorate } from '@/models/npcs/species/primordials'
import { Primordial } from '@/models/npcs/species/primordials/types'
import { decorate_text } from '@/models/utilities/text/decoration'
import { entity_placeholder } from '@/models/utilities/text/placeholders'

export const species__describe_symbiosis = (species: Beast | Primordial) => {
  const partner = species_symbiotic_partner(species)
  const { symbiosis } = partner ?? species
  const tag = partner?.tag ?? symbiosis?.tag
  return symbiosis
    ? ` ${entity_placeholder} has a ${
        symbiosis.subtype
          ? decorate_text({
              label: symbiosis.type,
              tooltip: symbiosis.subtype
            })
          : symbiosis.type
      }${
        symbiosis.subtype && partner ? ' (host)' : ''
      } relationship with the following species: ${(partner ? [partner.idx] : symbiosis.species)
        .map(idx =>
          tag === 'beast'
            ? beast__decorate(window.world.beasts[idx])
            : primordial__decorate(window.world.primordials[idx])
        )
        .join(', ')}`
    : ''
}
