import { Column, Row } from '@/components/common/vuetify/layout'
import { css_colors } from '@/styles/colors'
import { beast__decorate } from '@/models/npcs/species/beasts'
import { beast__families } from '@/models/npcs/species/beasts/taxonomy/types'
import { primordial__tooltip } from '@/models/npcs/species/primordials'
import { primordial__finalize } from '@/models/npcs/species/primordials/finalize'
import { primordial__trait_lookup } from '@/models/npcs/species/primordials/traits'
import { PrimordialTrait } from '@/models/npcs/species/primordials/traits/types'
import { Primordial } from '@/models/npcs/species/primordials/types'
import { species__length, species__size_rank } from '@/models/npcs/species/size'
import { decorate_text } from '@/models/utilities/text/decoration'
import { proper_list, title_case } from '@/models/utilities/text'
import { entity_placeholder, replace_placeholders } from '@/models/utilities/text/placeholders'
import { view_module } from '@/store/view'
import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import CodexPage from '../..'
import SectionList from '../../../../common/text/SectionList'
import StyledText from '../../../../common/text/StyledText'
import { species__describe_symbiosis } from './symbiosis'
import { rarity__rank } from '@/models/utilities/quality'

const primordial_placeholder = replace_placeholders({ primary: 'This species', secondary: 'It' })

const describe_traits = (params: {
  traits: Primordial['traits']
  type?: PrimordialTrait['type']
}) => {
  const { traits, type } = params
  const filtered = traits.filter(trait => {
    const template = primordial__trait_lookup[trait.tag]
    return type ? template.type === type : !template.type
  })
  return filtered.length > 0 ? ` ${filtered.map(trait => trait.text).join(' ')}` : ''
}

@Component({
  components: {
    CodexPage
  }
})
export default class PrimordialView extends tsx.Component<Record<string, unknown>> {
  private get appearance() {
    const { length: height, appearance, mimicry, genus, traits } = view_module.codex.primordial
    const similar = window.world.primordials[mimicry?.species]
    const mimic =
      mimicry?.role === 'mimic'
        ? `closely resembles the ${decorate_text({
            label: similar.name.toLowerCase(),
            link: similar,
            tooltip: title_case(similar.genus)
          })}.`
        : ''
    const model =
      mimicry?.role === 'model'
        ? ` ${entity_placeholder} is often mistaken for the ${decorate_text({
            label: similar.name.toLowerCase(),
            link: similar,
            tooltip: title_case(similar.genus)
          })}.`
        : ''
    const { color, texture, woody, leaves, thorns, sap, flowers, bioluminescence } = appearance
    const leaf_desc = leaves
      ? ` ${entity_placeholder} has ${leaves.sparsity ? `${leaves.sparsity} ` : ''}${
          leaves.size ? `${leaves.size} ` : ''
        }${leaves.texture ? `${leaves.texture} ` : ''}${
          leaves.variegation
            ? `${decorate_text({ label: 'variegated', tooltip: leaves.variegation })} `
            : ''
        } ${leaves.color} leaves${leaves.veins ? ` with distinct ${leaves.veins} veins` : ''}.`
      : ''
    const thorns_desc = thorns
      ? ` ${entity_placeholder} is covered in ${thorns.length ? `${thorns.length} ` : ''}${
          thorns.density ? `${thorns.density} ` : ''
        }${thorns.sharpness ? `${thorns.sharpness} ` : ''} ${thorns.color} ${thorns.type}.`
      : ''
    const sap_desc = sap ? ` ${entity_placeholder} bleeds a ${sap.texture} ${sap.color} sap.` : ''
    const singular_flower = flowers?.type === 'single'
    const floral_traits = describe_traits({ traits, type: 'flower' })
    const phasing = describe_traits({ traits, type: 'flower coloration' })
    const petals = Boolean(flowers?.exotic || flowers?.texture)
    const flower_desc = flowers
      ? ` ${entity_placeholder} has ${singular_flower ? 'a single ' : `${flowers.type} of`} ${
          flowers.size ? `${flowers.size} ` : ''
        }${flowers.detail ? `${flowers.detail} ` : ''}${flowers.shape}-shaped${
          !phasing ? ` ${proper_list(flowers.color, 'or')}` : ''
        } flower${singular_flower ? '' : 's'}${
          petals ? ` with ${flowers.exotic ?? flowers.texture} petals` : ''
        } that bloom${singular_flower ? 's' : ''} ${
          flowers.season ? `during the ${flowers.season} months` : 'year round'
        }.${phasing}${floral_traits}`
      : ''
    const looks = `has a ${
      bioluminescence ? `${decorate_text({ label: 'luminous', tooltip: bioluminescence })} ` : ''
    }${texture ? `${texture} ` : ''}${color} ${
      woody ? decorate_text({ label: 'exterior', tooltip: 'bark' }) : 'exterior'
    }.${leaf_desc}${thorns_desc}${sap_desc}${flower_desc}${model}`
    return (
      <StyledText
        text={primordial_placeholder(
          `${entity_placeholder} grows on to be on average ${species__length(height)} ${
            genus === 'vine' ? 'long' : 'tall'
          } and ${mimic || looks}`
        )}></StyledText>
    )
  }
  private get reproduction() {
    const { type, seeds, dispersal } = view_module.codex.primordial.reproduction
    const text = `${
      type === 'seeds' && seeds !== 'seeds' ? decorate_text({ label: type, tooltip: seeds }) : type
    }${dispersal ? ` (${dispersal} dispersal)` : ''}`
    return <StyledText text={text}></StyledText>
  }
  private get traits() {
    const { traits, attractors } = view_module.codex.primordial
    const habitat = describe_traits({ traits, type: 'habitat' })
    const behavior = describe_traits({ traits, type: 'behavior' })
    const use = describe_traits({ traits, type: 'use' })
    const attractor = attractors
      ? ` ${entity_placeholder} is known to ${attractors.type} the following ${
          beast__families[attractors.family].plural
        }: ${attractors.species.map(i => beast__decorate(window.world.beasts[i])).join(', ')}.`
      : ''
    const desc = habitat + behavior + attractor + use
    if (desc.length === 0) return false
    return <StyledText text={primordial_placeholder(desc)}></StyledText>
  }
  public get symbiosis() {
    const species = view_module.codex.primordial
    const symbiosis_text = species__describe_symbiosis(species)
    return primordial_placeholder(symbiosis_text)
  }
  public render() {
    const species = view_module.codex.primordial
    if (!species) return <span>none</span>
    primordial__finalize(species)
    const { environment, prey } = species
    const data: [string, JSX.Element | string][] = [
      ['Environment', `${environment.key}`],
      [
        'Regions',
        <StyledText
          text={species.regions
            .map(r => decorate_text({ link: window.world.regions[r] }))
            .join(', ')}></StyledText>
      ],
      ['Appearance', this.appearance]
    ]
    const traits = this.traits
    if (traits) data.push(['Traits', traits])
    data.push(['Reproduction', this.reproduction])
    const symbiosis = this.symbiosis
    if (symbiosis) data.push(['Symbiosis', <StyledText text={symbiosis}></StyledText>])
    if (prey?.length > 0) {
      data.push([
        'Prey',
        <StyledText
          text={prey
            .map(i => {
              const prey = window.world.beasts[i]
              return beast__decorate(prey)
            })
            .join(', ')}></StyledText>
      ])
    }
    return (
      <CodexPage
        title={species.name}
        subtitle={
          <StyledText
            color={css_colors.subtitle}
            text={`(${species.idx}) ${species__size_rank(species.size)} ${
              species.tag
            } (${rarity__rank(species.rarity)}, ${decorate_text({
              label: species.family,
              tooltip: primordial__tooltip(species),
              color: css_colors.subtitle
            })})`}></StyledText>
        }
        content={
          <Row>
            <Column cols={12} class='pb-0'>
              <SectionList list={data}></SectionList>
            </Column>
          </Row>
        }></CodexPage>
    )
  }
}
