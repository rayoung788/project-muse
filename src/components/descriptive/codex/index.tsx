import PrimaryButton from '@/components/common/PrimaryButton'
import {
  Card,
  CardActions,
  CardSubtitle,
  CardText,
  CardTitle
} from '@/components/common/vuetify/cards'
import { Divider, Spacer } from '@/components/common/vuetify/layout'
import { Lazy } from '@/components/common/vuetify/lazy'
import { TabProps } from '@/components/common/vuetify/tabs'
import { css_colors } from '@/styles/colors'
import { actor__location } from '@/models/npcs/actors'
import { view_module } from '@/store/view'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { GPS } from '../../common/GPS'
import { css } from '@emotion/css'
import { fonts } from '@/styles/fonts'
import { zoom_to_current } from '@/store/zoom'
import { style__subtitle } from '@/styles'
import { TransparentTabs } from '../../common/tabs/transparent'

export const style__codex_title = css`
  color: ${css_colors.primary};
  font-family: ${fonts.titles};
  font-weight: 800;
  font-size: 2.5rem;
`

interface CodexPageProps {
  title: string | JSX.Element
  subtitle: string | JSX.Element
  content?: JSX.Element
  actions?: JSX.Element
  tabs?: Record<string, TabProps>
}
interface CodexPageEvents {
  onTabChange: string
}
@Component({
  components: {
    GPS
  }
})
export default class CodexPage extends tsx.Component<CodexPageProps, CodexPageEvents> {
  @Prop() private title: string | JSX.Element
  @Prop() private subtitle: string | JSX.Element
  @Prop() private content?: JSX.Element
  @Prop() private actions?: JSX.Element
  @Prop() private tabs?: Record<string, TabProps>
  public render() {
    const { codex } = view_module
    const { current } = codex
    const avatar = view_module.avatar === view_module.codex.actor && current === 'actor'
    const { zoom, settlement_idx } = zoom_to_current()
    const enabled_gps = ['location', 'nation'].includes(current) || avatar
    Object.keys(this.tabs ?? {}).forEach(key => {
      this.tabs[key].element = <CardText>{this.tabs[key].element}</CardText>
    })
    return (
      <Lazy>
        <Card flat class='transparent ma-3'>
          <CardTitle class='mb-2'>
            <h3 class={style__codex_title}>{this.title}</h3>
            <Spacer></Spacer>
            <PrimaryButton
              class='mr-3'
              text='Back'
              disabled={codex.history.length < 1}
              click={() => {
                const [past] = codex.history.slice(-1)
                if (past) view_module.restore_history()
              }}
            />
            <PrimaryButton
              class='mr-2'
              text='PC'
              v-show={view_module.avatar && !avatar}
              click={() => view_module.update_codex({ target: view_module.avatar })}
            />
            <PrimaryButton
              class='mr-2'
              text='Reset'
              v-show={view_module.avatar && avatar}
              click={() => view_module.release_avatar()}
            />
            <GPS
              zoom={zoom}
              settlement_idx={avatar ? actor__location(view_module.avatar)?.idx : settlement_idx}
              disabled={!enabled_gps}></GPS>
          </CardTitle>
          <CardSubtitle>
            <h4 class={style__subtitle}>
              <i>{this.subtitle}</i>
            </h4>
          </CardSubtitle>
          <CardText>{this.content ?? <span></span>}</CardText>
          {this.tabs && (
            <CardText>
              <TransparentTabs tabs={this.tabs}></TransparentTabs>
            </CardText>
          )}
          {this.actions && <Divider></Divider>}
          {this.actions && <CardActions class='pt-4'>{this.actions}</CardActions>}
        </Card>
      </Lazy>
    )
  }
}
