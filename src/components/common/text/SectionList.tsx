import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

@Component
export default class SectionList extends tsx.Component<{
  list: [string, JSX.Element | JSX.Element[] | string][]
}> {
  @Prop() private list: [string, JSX.Element | JSX.Element[] | string][]
  public render() {
    return (
      <div>
        {this.list.map(([k, v]) => (
          <div>
            <span class='font-weight-bold'>{k}: </span>
            {v}
          </div>
        ))}
      </div>
    )
  }
}
