import { Component, Prop } from 'vue-property-decorator'
import { view_module } from '@/store/view'
import * as tsx from 'vue-tsx-support'
import TooltipText from '@/components/common/text/TooltipText'
import { TaggedEntity, codex_categories } from '@/models/utilities/codex/entities'
import { css } from '@emotion/css'
import { css_colors } from '@/styles/colors'

const style__links = css`
  a {
    color: ${css_colors.black};
    border-bottom: 1px solid ${css_colors.black};
    &:hover {
      color: ${css_colors.primary} !important;
      border-bottom: 1px dotted ${css_colors.primary} !important;
    }
  }
`

const style__italics = css`
  font-style: italic;
`

interface LinkedTextProps {
  text: string
  color?: string
}
@Component({
  components: {
    TooltipText
  }
})
export default class StyledText extends tsx.Component<LinkedTextProps> {
  @Prop(String) private text: string
  @Prop(String) private color: string
  public render() {
    const base_color = this.color ?? 'black'
    return (
      <span class={style__links}>
        {this.text.split(/@(.+?)@/g).map((text, j) => {
          if (text.match(/.+|.+|.+/)) {
            const [label, i, cat, tooltip, color, italics] = text.split('|')
            const tag = cat as TaggedEntity['tag']
            const idx = parseInt(i)
            const onClick = codex_categories.includes(tag)
              ? () => view_module.update_codex({ target: { tag, idx } })
              : false
            const text_color = color !== '' ? color : base_color
            const style = {
              color: text_color,
              'border-bottom': onClick ? `1px solid ${text_color}` : undefined
            }
            const link = onClick ? (
              <a style={style} onClick={onClick}>
                {label}
              </a>
            ) : (
              <span style={style}>{label}</span>
            )
            return (
              <span class={italics === 'true' ? style__italics : undefined} key={j}>
                {tooltip ? (
                  <TooltipText
                    text={link}
                    tooltip={<span>{tooltip}</span>}
                    underline_color={text_color}
                    link={Boolean(onClick)}></TooltipText>
                ) : (
                  link
                )}
              </span>
            )
          }
          return (
            <span key={j} style={{ color: base_color }}>
              {text}
            </span>
          )
        })}
      </span>
    )
  }
}
