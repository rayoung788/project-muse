import { BaseTooltip } from '../vuetify/tooltip'
import { componentFactory } from 'vue-tsx-support'
import { PropType } from 'vue'
import { css } from '@emotion/css'
import { css_colors } from '@/styles/colors'

const style__underlined_tooltip = (color?: string) => css`
  cursor: pointer;
  border-bottom: 1px dotted ${color ?? css_colors.black};
  text-decoration: none;
`

const TooltipText = componentFactory.create({
  components: {
    BaseTooltip
  },
  props: {
    text: Object as PropType<JSX.Element>,
    tooltip: Object as PropType<JSX.Element>,
    underline_color: String,
    link: Boolean
  } as const,
  render() {
    return (
      <BaseTooltip
        top
        scopedSlots={{
          activator: (params: { on: Record<string, (e: Event) => void> }) => {
            return (
              <span
                class={this.link ? '' : style__underlined_tooltip(this.underline_color)}
                {...{ on: params.on }}>
                {this.text}
              </span>
            )
          }
        }}>
        {this.tooltip}
      </BaseTooltip>
    )
  }
})

export default TooltipText
