import { Bar, mixins } from 'vue-chartjs'
import { ofType } from 'vue-tsx-support'

const { reactiveProp } = mixins

const BarChart = ofType<{ chartData: any; options?: any; height?: number }>().convert({
  extends: Bar,
  mixins: [reactiveProp],
  props: ['options', 'chartData', 'height'],
  mounted() {
    this.renderChart(this.chartData, this.options)
  }
} as any)

export default BarChart
