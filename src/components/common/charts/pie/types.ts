export interface PieData {
  label: string
  value: number
  color: string
}

export interface NestedPieData extends PieData {
  children: NestedPieData[] // child nodes
}

export type CustomPieChartTooltip = (
  tooltipItems: { index: number },
  data: { labels: string[]; datasets: { data: number[] }[] }
) => string
