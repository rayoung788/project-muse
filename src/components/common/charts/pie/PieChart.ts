import { formatters } from '@/models/utilities/text/formatters'
import { Pie, Doughnut, mixins } from 'vue-chartjs'
import { ofType } from 'vue-tsx-support'
import { PieData } from './types'

const { reactiveProp } = mixins

export const pie_chart__construct = (chart: PieData[]) => {
  const labels: string[] = []
  const data: number[] = []
  const colors: string[] = []
  // sort culture entries by percent and format pie chart data & labels
  chart.forEach(({ label, value, color }) => {
    labels.push(label)
    data.push(value)
    colors.push(color)
  })
  return {
    labels,
    datasets: [
      {
        data,
        backgroundColor: colors
      }
    ]
  }
}

export const pie_chart__percent_tooltips = (
  tooltipItems: { index: number },
  data: { labels: string[]; datasets: { data: number[] }[] }
) => {
  const label = data.labels[tooltipItems.index]
  const value = data.datasets[0].data[tooltipItems.index]
  return `${label}: ${formatters.percent({ value, precision: 2 })}`
}

export const pie_chart__basic_tooltips = (
  tooltipItems: { index: number },
  data: { labels: string[]; datasets: { data: number[] }[] }
) => {
  const label = data.labels[tooltipItems.index]
  const value = data.datasets[0].data[tooltipItems.index]
  return `${label}: ${value}`
}

export const PieChart = ofType<{ chartData: any; options: any; height?: number }>().convert({
  extends: Pie,
  mixins: [reactiveProp],
  props: ['options', 'chartData'],
  mounted() {
    this.renderChart(this.chartData, this.options)
  }
} as any)

export const DoughnutChart = ofType<{ chartData: any; options: any; height?: number }>().convert({
  extends: Doughnut,
  mixins: [reactiveProp],
  props: ['options', 'chartData'],
  mounted() {
    this.renderChart(this.chartData, this.options)
  }
} as any)
