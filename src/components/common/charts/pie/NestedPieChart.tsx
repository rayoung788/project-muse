import PrimaryButton from '@/components/common/PrimaryButton'
import { BaseButton, ButtonToggle } from '@/components/common/vuetify/button'
import { Row, Column, Container } from '@/components/common/vuetify/layout'
import { Component, Prop, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { DoughnutChart, PieChart, pie_chart__construct } from './PieChart'
import { CustomPieChartTooltip, NestedPieData } from './types'

type custom_title = (node: NestedPieData) => string

type chart_types = 'pie' | 'doughnut'

@Component({
  components: {
    DoughnutChart,
    PrimaryButton,
    BaseButton,
    ButtonToggle,
    Row,
    Column,
    Container
  }
})
export default class NestedPieChart extends tsx.Component<{
  tooltips: CustomPieChartTooltip
  chart_data: NestedPieData
  title: custom_title
  type: chart_types
}> {
  @Prop() private tooltips: CustomPieChartTooltip
  @Prop() private chart_data: NestedPieData
  @Prop() private title: custom_title
  @Prop() private type: chart_types
  private context: string[] = []
  private get current() {
    let current = this.chart_data
    const context = [...this.context]
    while (context.length > 0) {
      const child = context.shift()
      current = current.children.find(({ label }) => child === label)
    }
    return current
  }
  @Watch('chartData')
  public updateContext() {
    this.context = []
  }
  public render() {
    const current = this.current
    const nodes = current.children
    const Chart = this.type === 'pie' ? PieChart : DoughnutChart
    return (
      <Container>
        <Row v-show={current.label} class='text-center'>
          <Column>{this.title(current)}</Column>
        </Row>
        <Row>
          <Column>
            <Chart
              height={200}
              class='chart'
              chartData={pie_chart__construct(nodes)}
              options={{
                tooltips: {
                  callbacks: {
                    label: this.tooltips
                  }
                },
                onClick: (_: Event, data: { _model: { label: string } }[]) => {
                  const [d] = data
                  const name = d?._model?.label?.replace(/ \(.*/, '')
                  const child = this.current.children.find(({ label }) => label === name)
                  if (child?.children?.length > 0) {
                    this.context = [...this.context, name]
                  }
                }
              }}
            />
          </Column>
        </Row>
        <Row class='text-center'>
          <Column>
            <PrimaryButton
              text='Back'
              disabled={this.context.length < 1}
              click={() => {
                this.context.pop()
                this.context = [...this.context]
              }}></PrimaryButton>
          </Column>
        </Row>
      </Container>
    )
  }
}
