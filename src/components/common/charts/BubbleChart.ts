import { Bubble, mixins } from 'vue-chartjs'
import { ofType } from 'vue-tsx-support'

const { reactiveProp } = mixins

const BubbleChart = ofType<{
  chartData: any
  options?: any
  height?: number
}>().convert({
  extends: Bubble,
  mixins: [reactiveProp],
  props: ['options', 'chartData', 'height'],
  mounted() {
    this.renderChart(this.chartData, this.options)
  }
} as any)

export default BubbleChart
