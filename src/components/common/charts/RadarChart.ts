import { mixins, Radar } from 'vue-chartjs'
import { ofType } from 'vue-tsx-support'

const { reactiveProp } = mixins

const RadarChart = ofType<{ chartData: any; options: any }>().convert({
  extends: Radar,
  mixins: [reactiveProp],
  props: ['options', 'chartData', 'height'],
  mounted() {
    this.renderChart(this.chartData, this.options)
  }
} as any)

export default RadarChart
