import { mixins, Scatter } from 'vue-chartjs'
import { ofType } from 'vue-tsx-support'

const { reactiveProp } = mixins

const LineChart = ofType<{ chartData: any; options: any; height: number }>().convert({
  extends: Scatter,
  mixins: [reactiveProp],
  props: ['options', 'chartData', 'height'],
  mounted() {
    this.renderChart(this.chartData, this.options)
  }
} as any)

export default LineChart
