import { Component, Prop, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { title_case } from '@/models/utilities/text'
import SectionList from './text/SectionList'
import StyledText from './text/StyledText'
import { Chip } from './vuetify/chip'
import { Row, Column } from './vuetify/layout'
import { Dice } from '@/models/utilities/math/dice'

interface TraitProps {
  traits: { tag: string; text: string }[]
  colors: Record<string, string>
}
@Component
export default class TraitDisplay extends tsx.Component<TraitProps> {
  @Prop() private traits: TraitProps['traits']
  @Prop() private colors: TraitProps['colors']
  private selected_trait = ''
  private dice = new Dice('traits')
  public mounted() {
    this.on_traits_change()
  }
  @Watch('traits')
  public on_traits_change() {
    this.selected_trait = this.traits.length > 0 ? this.dice.choice(this.traits).tag : ''
  }
  public render() {
    const selected_trait = this.traits.find(trait => trait.tag === this.selected_trait)
    return (
      <Row wrap>
        {this.traits.length > 0 && (
          <Column cols={12}>
            <Row justify='center'>
              {this.traits.map(trait => (
                <Chip
                  class='mr-1'
                  dark
                  label
                  onClick={() => (this.selected_trait = trait.tag)}
                  color={this.colors[trait.tag]}
                  outlined={trait.tag !== this.selected_trait}>
                  {title_case(trait.tag)}
                </Chip>
              ))}
            </Row>
          </Column>
        )}
        {selected_trait && (
          <Column cols={12}>
            <SectionList
              list={[
                [
                  title_case(selected_trait.tag),
                  <StyledText text={selected_trait.text}></StyledText>
                ]
              ]}></SectionList>
          </Column>
        )}
      </Row>
    )
  }
}
