import { view_module } from '@/store/view'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { BaseButton } from './vuetify/button'
import { Icon } from './vuetify/icon'
import { css_colors } from '../../styles/colors'

interface GPSProps {
  settlement_idx: number
  zoom?: number
  disabled?: boolean
}

@Component({
  components: {
    BaseButton,
    Icon
  }
})
export class GPS extends tsx.Component<GPSProps> {
  @Prop({ type: Number, required: true }) private settlement_idx: number
  @Prop({ type: Number, default: 40 }) private zoom: number
  @Prop({ type: Boolean, default: false }) private disabled: boolean
  private zoom_to() {
    const { x, y } = window.world.locations[this.settlement_idx]
    view_module.set_gps({ x, y, zoom: this.zoom })
  }
  public render() {
    return (
      <BaseButton
        disabled={this.disabled}
        small
        icon
        onClick={() => this.zoom_to()}
        color={css_colors.primary}>
        <Icon>mdi-crosshairs-gps</Icon>
      </BaseButton>
    )
  }
}
