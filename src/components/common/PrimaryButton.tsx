import { Component, Prop } from 'vue-property-decorator'
import { BaseButton } from './vuetify/button'
import * as tsx from 'vue-tsx-support'
import { css_colors } from '../../styles/colors'

const blank = () => {}

interface PrimaryButtonProps {
  text: string | JSX.Element
  click?: () => void
  show?: boolean
  disabled?: boolean
  loading?: boolean
}

@Component({
  components: {
    BaseButton
  }
})
export default class PrimaryButton extends tsx.Component<PrimaryButtonProps> {
  @Prop() private text: string | JSX.Element
  @Prop() private click?: () => void
  @Prop() private show?: boolean
  @Prop() private disabled?: boolean
  @Prop() private loading?: boolean
  public render() {
    return (
      <BaseButton
        color={css_colors.primary}
        small
        tile
        dark={!this.disabled}
        v-show={this.show ?? true}
        disabled={this.disabled ?? false}
        loading={this.loading ?? false}
        onClick={this.click ?? blank}>
        {this.text}
      </BaseButton>
    )
  }
}
