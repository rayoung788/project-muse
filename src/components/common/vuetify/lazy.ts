import { ofType } from 'vue-tsx-support'
import { VLazy } from 'vuetify/lib'

export const Lazy = ofType().convert(VLazy)
