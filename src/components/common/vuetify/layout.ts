import { ofType } from 'vue-tsx-support'
import { VCol, VRow, VSpacer, VDivider, VContainer, VMain, VFooter } from 'vuetify/lib'

export const Spacer = ofType().convert(VSpacer)
export const Divider = ofType().convert(VDivider)

export const Column = ofType<{
  cols?: number
  offset?: number
}>().convert(VCol)

export const Row = ofType<{
  align?: string
  justify?: string
  shaped?: boolean
  wrap?: boolean
  'background-color'?: string
}>().convert(VRow)

export const Container = ofType<{ fluid?: boolean }>().convert(VContainer)

export const Content = ofType().convert(VMain)
export const Footer = ofType<{ app?: boolean; fixed?: boolean }>().convert(VFooter)
