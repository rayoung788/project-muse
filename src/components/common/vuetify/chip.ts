import { ofType } from 'vue-tsx-support'
import { VChip } from 'vuetify/lib'

export const Chip = ofType<{
  color?: string
  outlined?: boolean
  label?: boolean
  dark?: boolean
  onClick?: () => void
}>().convert(VChip)
