import { ofType } from 'vue-tsx-support'
import { VAlert } from 'vuetify/lib'

export const Alert = ofType<{
  color: string
  text?: boolean
  outlined?: boolean
  dense?: boolean
}>().convert(VAlert)
