import { VDataTable } from 'vuetify/lib'
import { Component, Prop } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { Container, Row } from './layout'
import { css_colors } from '@/styles/colors'
import { css } from '@emotion/css'

export interface TableHeaders {
  text: string
  value: string
  align?: 'start' | 'center' | 'end'
  sortable?: boolean
  filterable?: boolean
  divider?: boolean
  class?: string | string[]
  cellClass?: string | string[]
  width?: string | number
}

interface TableProps<Data> {
  dense?: boolean
  'disable-pagination'?: boolean
  'hide-default-footer'?: boolean
  'hide-default-header'?: boolean
  'footer-props'?: any
  'group-by'?: string | string[]
  'show-expand'?: boolean
  expanded?: Data[]
  'single-expand'?: boolean
  'items-per-page'?: number
  'item-key'?: string
  'item-class'?: (item: Data) => string
  page?: number
  headers: TableHeaders[]
  items: Data[]
}

export interface StrictHeaders<T extends string> extends TableHeaders {
  value: T
}

export const DataTable = <Data, Slots, Events = {}>() =>
  tsx.ofType<TableProps<Data>, Events, Slots>().convert(VDataTable)

export const style__subtitle = css`
  font-size: 0.65rem;
  color: ${css_colors.subtitle};
`

@Component
export default class DetailedTableRow extends tsx.Component<{
  title: string | JSX.Element
  subtitle: string | JSX.Element
  link?: boolean
}> {
  @Prop() private title: string | JSX.Element
  @Prop() private subtitle: string | JSX.Element
  @Prop() private link?: boolean
  public render() {
    return (
      <Container>
        <Row>{this.title}</Row>
        <Row class={`pt-${this.link ? 3 : 2}`}>
          <i class={style__subtitle}>{this.subtitle}</i>
        </Row>
      </Container>
    )
  }
}
