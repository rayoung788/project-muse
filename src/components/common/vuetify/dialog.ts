import { ofType } from 'vue-tsx-support'
import { VDialog } from 'vuetify/lib'

export const BaseDialog = ofType<{ width?: number }>().convert(VDialog)
