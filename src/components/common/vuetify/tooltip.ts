import { ofType } from 'vue-tsx-support'
import { VTooltip } from 'vuetify/lib'

export const BaseTooltip = ofType<
  { top?: boolean },
  {},
  {
    activator: { on: Record<string, (e: Event) => void> }
  }
>().convert(VTooltip)
