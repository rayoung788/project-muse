import { ofType } from 'vue-tsx-support'
import { VAutocomplete, VRadio, VRadioGroup, VSelect, VSlider, VTextField } from 'vuetify/lib'

export const AutoComplete = ofType<{
  multiple?: boolean
  chips?: boolean
  'small-chips'?: boolean
  'deletable-chips'?: boolean
  color?: string
  spellcheck?: boolean
  dense?: boolean
  items: { name: string; idx: number }[] | string[] | number[]
  'item-text'?: 'name'
  'item-value'?: 'idx'
  'persistent-hint'?: boolean
  hint?: string
  label?: string
  dark?: boolean
  onClick?: () => void
}>().convert(VAutocomplete)

export const Select = ofType<{
  items: string[]
  label?: string
  onClick?: () => void
  outlined?: boolean
  filled?: boolean
  dense?: boolean
  color?: string
}>().convert(VSelect)

export const TextField = ofType<{
  spellcheck?: boolean
  disabled?: boolean
  label: string
  type?: string
  suffix?: string
  onClick?: () => void
}>().convert(VTextField)

export const RadioButton = ofType<{
  color?: string
  label?: string
  value?: string
  onClick?: () => void
}>().convert(VRadio)

export const RadioGroup = ofType<{
  row?: boolean
  column?: boolean
}>().convert(VRadioGroup)

export const Slider = ofType<{
  label: string
  'tick-labels': string[]
  min: number
  max: number
  step: number
}>().convert(VSlider)
