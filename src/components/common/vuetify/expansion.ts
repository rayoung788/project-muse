import { ofType } from 'vue-tsx-support'
import {
  VExpansionPanel,
  VExpansionPanelContent,
  VExpansionPanelHeader,
  VExpansionPanels
} from 'vuetify/lib'

export const ExpansionPanels =
  ofType<{ accordion?: boolean; flat?: boolean }>().convert(VExpansionPanels)
export const ExpansionPanel = ofType().convert(VExpansionPanel)
export const ExpansionPanelHeader = ofType().convert(VExpansionPanelHeader)
export const ExpansionPanelContent = ofType().convert(VExpansionPanelContent)
