import { ofType } from 'vue-tsx-support'
import { VApp, VAppBar, VToolbar, VToolbarItems, VToolbarTitle } from 'vuetify/lib'

export const App = ofType().convert(VApp)
export const AppBar = ofType<{
  app?: boolean
  light?: boolean
  dark?: boolean
}>().convert(VAppBar)

export const Toolbar = ofType().convert(VToolbar)
export const ToolItems = ofType().convert(VToolbarItems)
export const ToolbarTitle = ofType().convert(VToolbarTitle)
