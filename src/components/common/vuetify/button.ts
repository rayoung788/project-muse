import { ofType } from 'vue-tsx-support'
import { VBtn, VBtnToggle, VSwitch } from 'vuetify/lib'

export const BaseButton = ofType<{
  color?: string
  small?: boolean
  large?: boolean
  icon?: boolean
  text?: boolean
  tile?: boolean
  disabled?: boolean
  loading?: boolean
  block?: boolean
  dark?: boolean
  onClick?: () => void
  value?: number | string
}>().convert(VBtn)

export const ButtonToggle = ofType<{
  color: string
  mandatory?: boolean
  shaped?: boolean
  dense?: boolean
  'background-color'?: string
}>().convert(VBtnToggle)

export const ButtonSwitch = ofType<{
  color: string
  label: string
}>().convert(VSwitch)
