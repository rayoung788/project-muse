import { ofType } from 'vue-tsx-support'
import { VTab, VTabs, VTabsSlider, VTabsItems, VTabItem } from 'vuetify/lib'

export const Tabs = ofType<{
  'background-color'?: string
  color?: string
  centered?: boolean
  vertical?: boolean
}>().convert(VTabs)
export const TabsSlider = ofType<{ color?: string }>().convert(VTabsSlider)
export const Tab = ofType<{ ripple?: boolean; disabled?: boolean }>().convert(VTab)
export const TabItems = ofType().convert(VTabsItems)
export const TabItem = ofType<{ disabled?: boolean }>().convert(VTabItem)

export interface TabProps {
  element: JSX.Element
  title?: JSX.Element
  disabled?: boolean
}
