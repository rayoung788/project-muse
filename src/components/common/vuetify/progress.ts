import { ofType } from 'vue-tsx-support'
import { VProgressCircular } from 'vuetify/lib'

export const ProgressCircular = ofType<{
  indeterminate?: boolean
  size?: number
  width?: number
  value?: number
  color?: string
}>().convert(VProgressCircular)
