import { ofType } from 'vue-tsx-support'
import { VBadge } from 'vuetify/lib'

export const Badge = ofType<{
  color: string
  content?: string
  light?: boolean
  dark?: boolean
  overlap?: boolean
  dot?: boolean
  icon?: string
}>().convert(VBadge)
