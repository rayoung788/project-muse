import { ofType } from 'vue-tsx-support'
import { VSnackbar } from 'vuetify/lib'

export const Snackbar = ofType<{
  timeout?: number
  color?: string
  text?: boolean
}>().convert(VSnackbar)
