import { ofType } from 'vue-tsx-support'
import { VCard, VCardActions, VCardSubtitle, VCardText, VCardTitle } from 'vuetify/lib'

export const Card = ofType<{ flat?: boolean }>().convert(VCard)
export const CardText = ofType().convert(VCardText)
export const CardTitle = ofType().convert(VCardTitle)
export const CardSubtitle = ofType().convert(VCardSubtitle)
export const CardActions = ofType().convert(VCardActions)
