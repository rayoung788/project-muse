import { ofType } from 'vue-tsx-support'
import { VIcon } from 'vuetify/lib'

export const Icon = ofType<{
  small?: boolean
  color?: string
  disabled?: boolean
  size?: number
  dark?: boolean
  onClick?: () => void
}>().convert(VIcon)
