import { Column, Container, Row } from '@/components/common/vuetify/layout'
import { Component, Prop, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { BaseButton, ButtonToggle } from '@/components/common/vuetify/button'
import { css_colors } from '@/styles/colors'

type ColorSelector = (selected: string) => string

@Component
export class TabbedContent extends tsx.Component<{
  bottom?: boolean
  default_idx?: number
  selection: string[]
  content: <T extends string>(selected: T) => JSX.Element
  color?: ColorSelector
  selected?: string
}> {
  @Prop() private bottom?: boolean
  @Prop() private selection: string[]
  @Prop() private default_idx?: number
  @Prop() private content: <T extends string>(selected: T) => JSX.Element
  @Prop() private color: ColorSelector
  @Prop({ default: '' }) public selected: string
  private selected_content = ''
  public mounted() {
    this.reset_selected()
  }
  public render() {
    const color = this.color?.(this.selected_content) || 'primary'
    const toggle = (
      <Row align='center' justify='center'>
        <ButtonToggle
          v-model={this.selected_content}
          mandatory
          shaped
          color={color}
          background-color={css_colors.accent}>
          {this.selection.map(chart => (
            <BaseButton tile small value={chart}>
              {chart}
            </BaseButton>
          ))}
        </ButtonToggle>
      </Row>
    )
    return (
      this.selected_content !== '' && (
        <Container fluid>
          {!this.bottom && toggle}
          <Row class='mt-2'>
            <Column cols={12}>{this.content(this.selected_content)}</Column>
          </Row>
          {this.bottom && toggle}
        </Container>
      )
    )
  }
  public reset_selected() {
    this.selected_content = this.selection[this.default_idx ?? 0] ?? ''
  }
  @Watch('selection')
  public watch_selection(val: string[], old: string[]) {
    if (val.toString() !== old.toString()) this.reset_selected()
  }
  @Watch('selected')
  public watch_selected(val: string) {
    this.selected_content = val
  }
  @Watch('selected_content')
  public watch_selected_content(val: string) {
    this.$emit('update:selected', val)
  }
}

@Component({
  components: {
    TabbedContent
  }
})
export class SimpleTabbedContent extends tsx.Component<{
  content: Record<string, JSX.Element>
  color?: ColorSelector
}> {
  @Prop() private content: Record<string, JSX.Element>
  @Prop() private color: ColorSelector
  public render() {
    const content = Object.keys(this.content)
    return (
      <TabbedContent
        selection={content}
        content={(selected: string) => this.content[selected]}
        color={this.color}></TabbedContent>
    )
  }
}
