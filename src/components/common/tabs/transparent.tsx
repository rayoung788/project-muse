import { Component, Prop, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { css_colors } from '@/styles/colors'
import { Tabs, TabsSlider, Tab, TabItems, TabItem, TabProps } from '../vuetify/tabs'

interface TransparentTabProps {
  tabs: Record<string, TabProps>
  styles?: string
}
interface TransparentTabEvents {
  onTabChange: string
}

@Component
export class TransparentTabs extends tsx.Component<TransparentTabProps, TransparentTabEvents> {
  @Prop() private tabs: Record<string, TabProps>
  @Prop() private styles?: string
  private active = 0
  public render() {
    const tabs = Object.keys(this.tabs)
    const items = Object.values(this.tabs)
    return (
      <div>
        <Tabs
          background-color='transparent'
          color={css_colors.primary}
          centered
          v-model={this.active}>
          <TabsSlider color={css_colors.secondary}></TabsSlider>
          {tabs.map((tab, i) => {
            const { title } = items[i]
            return (
              <Tab disabled={items[i].disabled} key={i} ripple>
                {title ?? tab}
              </Tab>
            )
          })}
        </Tabs>
        <TabItems class='transparent my-3' v-model={this.active}>
          {items.map(({ element, disabled }, i) => (
            <TabItem disabled={disabled} key={i} class={this.styles ?? ''}>
              {element}
            </TabItem>
          ))}
        </TabItems>
      </div>
    )
  }
  @Watch('active')
  public style_change() {
    this.$emit('tabChange', Object.keys(this.tabs)[this.active])
  }
}
