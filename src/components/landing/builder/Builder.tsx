import { Component, Prop, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import { css_colors } from '../../../styles/colors'

import * as d3 from 'd3'
import { circle__draw, ContinentTemplate, isle_heights } from '../shapes'
import { builder_actions } from './actions'
import { style__main_content } from '@/styles'

const canvas_id = 'builder_canvas'
export const builder__id = 'builder_canvas_container'

@Component
export default class MapBuilder extends tsx.Component<{
  template: ContinentTemplate
  setTemplate?: (template: ContinentTemplate) => void
  action: typeof builder_actions[number]
}> {
  @Prop() private template: ContinentTemplate
  @Prop() private action: typeof builder_actions[number]
  @Prop() private setTemplate: (template: ContinentTemplate) => void
  private redraw = false
  private dragging = false
  private x = 0
  private y = 0
  private get template_copy(): ContinentTemplate {
    return JSON.parse(JSON.stringify(this.template))
  }
  private panZoom() {
    const canvas = document.getElementById(canvas_id) as HTMLCanvasElement
    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
    const init = d3.select(ctx.canvas)
    init.on('mousemove', this.mouse_over)
    init.on('mousedown', this.mouse_click)
    init.on('mouseup', this.mouse_release)
    init.on('dblclick', this.double_click)
  }
  private mouse_over() {
    const [x, y] = d3.mouse(document.querySelector(`#${canvas_id}`))
    const canvas = document.getElementById(canvas_id) as HTMLCanvasElement
    const dx = -(this.x - x) / canvas.width
    const dy = -(this.y - y) / canvas.height
    this.x = x
    this.y = y
    if (this.dragging) {
      const template = this.template_copy
      const selected = template.isles[template.selected]
      selected.x += dx
      selected.y += dy
      this.setTemplate(template)
      this.redraw = !this.redraw
    }
  }
  private closest(params: { isles: ContinentTemplate['isles']; px: number; py: number }) {
    const { isles, px, py } = params
    return isles.reduce(
      (closest: { d: number; i: number }, { x, y }, i) => {
        const dist = Math.hypot(x - px, y - py)
        return dist < closest.d ? { d: dist, i } : closest
      },
      {
        d: Infinity,
        i: -1
      }
    ).i
  }
  private get mouse_pos() {
    const canvas = document.getElementById(canvas_id) as HTMLCanvasElement
    return {
      px: this.x / canvas.width,
      py: this.y / canvas.height
    }
  }
  private mouse_click() {
    const { px, py } = this.mouse_pos
    const template = this.template_copy
    const closest = this.closest({ isles: template.isles, px, py })
    if (this.action !== 'none') {
      this.setTemplate({ ...template, selected: closest })
      this.dragging = true
    }
    this.redraw = !this.redraw
  }
  private double_click() {
    const { px, py } = this.mouse_pos
    const template = this.template_copy
    const closest = this.closest({ isles: template.isles, px, py })
    if (this.action === 'add') {
      const selected = template.isles.length
      this.setTemplate({
        isles: [...template.isles, { x: px, y: py, r: 0.2, w: isle_heights.low }],
        selected
      })
    } else if (this.action === 'remove') {
      const isles = template.isles.filter((_, i) => i !== closest)
      this.setTemplate({ isles, selected: this.closest({ isles, px, py }) })
    }
    this.redraw = !this.redraw
  }
  private mouse_release() {
    this.dragging = false
  }
  private resize() {
    const container = document.getElementById(builder__id)
    const canvas = document.getElementById(canvas_id) as HTMLCanvasElement
    canvas.width = container.clientWidth
    canvas.height = container.clientWidth
  }
  public paint_canvas() {
    const canvas = document.getElementById(canvas_id) as HTMLCanvasElement
    this.template.isles.forEach((circle, i) => {
      circle__draw({ canvas, circle, selected: this.template.selected === i })
    })
  }
  public mounted() {
    this.resize()
    this.paint_canvas()
    this.panZoom()
  }
  public render() {
    return (
      <canvas
        class={style__main_content}
        id={canvas_id}
        onWheel={event => {
          const dir = event.deltaY > 0 ? 1 : -1
          const template = this.template_copy
          const selected = template.isles[template.selected]
          if (!selected) return
          selected.r *= 1 + dir * 0.2
          this.setTemplate?.(template)
          this.redraw = !this.redraw
        }}
        style={{
          'background-color': css_colors.background.map,
          height: `100%`,
          width: `100%`
        }}></canvas>
    )
  }
  @Watch('redraw')
  public redraw_canvas() {
    this.resize()
    this.paint_canvas()
  }
  @Watch('template')
  public change_template() {
    this.redraw_canvas()
  }
}
