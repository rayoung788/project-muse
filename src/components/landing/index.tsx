import { Dice, generate_id } from '@/models/utilities/math/dice'
import { profile, profile__switch } from '@/models/utilities/performance'
import { world__spawn } from '@/models/world/spawn'
import { CivilizationShaper } from '@/models/world/spawn/shapers/civilization'
import { ContinentShaper } from '@/models/world/spawn/shapers/continents'
import { DisplayShaper } from '@/models/world/spawn/shapers/display'
import { InfrastructureShaper } from '@/models/world/spawn/shapers/infrastructure'
import { LoreShaper } from '@/models/world/spawn/shapers/lore'
import { RegionalShaper } from '@/models/world/spawn/shapers/regions'
import { view_module } from '@/store/view'
import { Chart } from 'chart.js'
import 'chartjs-plugin-zoom'
import { Component, Watch } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'
import PrimaryButton from '../common/PrimaryButton'
import { BaseButton, ButtonSwitch, ButtonToggle } from '../common/vuetify/button'
import { Slider, TextField } from '../common/vuetify/input'
import { Column, Container, Row } from '../common/vuetify/layout'
import { ProgressCircular } from '../common/vuetify/progress'
import { css_colors } from '../../styles/colors'
import { builder_actions } from './builder/actions'
import MapBuilder, { builder__id } from './builder/Builder'
import { ContinentTemplate, isle_heights, shapes } from './shapes'
import { fonts } from '@/styles/fonts'

@Component({
  components: {
    MapBuilder
  }
})
export default class Landing extends tsx.Component<Record<string, unknown>> {
  private catchup = 500
  private progress = 0
  private loading = false
  private show_builder = false
  private state = 'Generation takes 3-4 seconds.'
  private tiers = 15
  private res = 8
  private seed: string = generate_id()
  private history = 100
  private template: ContinentTemplate = { isles: [], selected: 0 }
  private origin_mold: ContinentTemplate = { isles: [], selected: 0 }
  private action: typeof builder_actions[number] = 'add'
  public mounted() {
    Chart.defaults.global.defaultFontFamily = fonts.content
    this.seed_template()
  }
  private setTemplate(mold: ContinentTemplate) {
    this.template = { ...mold }
  }
  public render() {
    const min = 1
    const max = this.tiers
    return (
      <div>
        <Row justify='center'>
          {this.show_builder && (
            <Column cols={6} id={builder__id}>
              <MapBuilder
                action={this.action}
                template={this.template}
                setTemplate={this.setTemplate}></MapBuilder>
            </Column>
          )}
          <Column class='mt-3' cols={6}>
            <Container fluid>
              <Row class='text-center' justify='center'>
                <Column cols={7} class='mt-5'>
                  <TextField v-model={this.seed} label='seed:' />
                </Column>
                <Column cols={3} class='mt-5'>
                  <TextField v-model={this.history} type='number' label='years:' />
                </Column>
              </Row>
              {!this.loading && (
                <Row class='text-center' justify='center'>
                  <Column cols={10} class='pt-0'>
                    <BaseButton
                      tile
                      large
                      block
                      color={css_colors.primary}
                      dark
                      onClick={this.shape}>
                      Generate World
                    </BaseButton>
                  </Column>
                </Row>
              )}
              {this.loading && (
                <Row class='text-center' justify='center'>
                  <Column cols={3} class='mt-5'>
                    <ProgressCircular
                      size={100}
                      width={5}
                      color={css_colors.primary}
                      value={this.progress}>
                      {this.progress}%
                    </ProgressCircular>
                  </Column>
                </Row>
              )}
              {this.loading && (
                <Row class='text-center' justify='center'>
                  <Column cols={9}>{this.state}</Column>
                </Row>
              )}
              {!this.loading && (
                <Row class='text-center' justify='center'>
                  <Column cols={10}>
                    <Slider
                      label='Cells:'
                      v-model={this.res}
                      tick-labels={[...Array(this.tiers).keys()].map(i => `${i + 1}x`)}
                      min={min}
                      max={max}
                      step={1}
                    />
                  </Column>
                </Row>
              )}
              {!this.loading && (
                <Row class='text-center' justify='center'>
                  <ButtonSwitch
                    color={css_colors.primary}
                    label='Map Builder'
                    v-model={this.show_builder}></ButtonSwitch>
                </Row>
              )}
            </Container>
            {!this.loading && this.show_builder && (
              <Container fluid>
                <Row class='text-center mt-3' justify='center'>
                  <Column cols={5}>
                    <ButtonToggle
                      v-model={this.action}
                      mandatory
                      shaped
                      color={css_colors.primary}
                      background-color={css_colors.accent}>
                      {builder_actions
                        .filter(a => a !== 'none')
                        .map(action => (
                          <BaseButton tile small value={action}>
                            {action}
                          </BaseButton>
                        ))}
                    </ButtonToggle>
                  </Column>
                </Row>
                <Row class='text-center mt-3' justify='center'>
                  <Column cols={10}>
                    <PrimaryButton
                      class='mr-1'
                      text='Height'
                      disabled={!this.template.isles[this.template.selected]}
                      click={() => {
                        const selected = this.template.isles[this.template.selected]
                        const { high, low } = isle_heights
                        selected.w = selected.w === low ? high : low
                        this.setTemplate(this.template)
                      }}></PrimaryButton>
                    <PrimaryButton
                      class='mr-1'
                      text='Clear'
                      click={() => {
                        this.setTemplate({ isles: [], selected: 0 })
                      }}></PrimaryButton>
                    <PrimaryButton
                      class='mr-1'
                      text='Reset'
                      click={() => {
                        this.setTemplate(this.origin_mold)
                      }}></PrimaryButton>
                    <PrimaryButton
                      class='mr-1'
                      text='Random'
                      click={() => this.random_template()}></PrimaryButton>
                    <PrimaryButton
                      text='Export'
                      click={() => {
                        navigator.clipboard.writeText(JSON.stringify(this.template.isles))
                      }}></PrimaryButton>
                  </Column>
                </Row>
              </Container>
            )}
          </Column>
        </Row>
      </div>
    )
  }
  private shape() {
    // initiate generation
    console.log(`Seed: ${this.seed}`)
    this.loading = true
    console.time('World Generation')
    this.progress += 10
    this.state = 'Shaping the continents...'
    window.world = world__spawn({ seed: this.seed, res: this.res, template: this.template.isles })
    setTimeout(() => this.continents(), this.catchup + 10)
  }
  private continents() {
    profile({ label: 'Continents', f: () => new ContinentShaper().build() })
    this.progress += 15
    this.state = '...Partitioning regions'
    setTimeout(() => this.regions(), this.catchup)
  }
  private regions() {
    profile({ label: 'Regions', f: () => new RegionalShaper().build() })
    this.progress += 5
    this.state = 'Spreading civilization...'
    setTimeout(() => this.civilizations(), this.catchup)
  }
  private civilizations() {
    profile({ label: 'Civilization', f: () => new CivilizationShaper().build() })
    this.progress += 10
    this.state = '...Building infrastructure'
    setTimeout(() => this.infrastructure(), this.catchup)
  }
  private infrastructure() {
    profile({ label: 'Roads', f: () => new InfrastructureShaper().build() })
    this.progress += 15
    this.state = 'Lorecrafting...'
    setTimeout(() => this.lore(), this.catchup)
  }
  private lore() {
    profile({ label: 'Lore', f: () => new LoreShaper(this.history).build() })
    this.progress += 15
    this.state = '...Preparing the canvas'
    setTimeout(() => this.display(), this.catchup)
  }
  private display() {
    profile({ label: 'Canvas', f: () => new DisplayShaper().build() })
    this.progress += 20
    this.state = 'Finalizing world...'
    setTimeout(() => this.finalize(), this.catchup)
  }
  private finalize() {
    profile__switch(window.profiles.current)
    view_module.shaper(window.world.id)
    // reset states after generation is complete
    this.loading = false
    this.progress = 0
    this.state = 'Generation takes 3-4 seconds.'
    console.timeEnd('World Generation')
  }
  public random_template() {
    this.template = {
      isles: [...window.dice.choice(shapes)],
      selected: 0
    }
    this.origin_mold = { ...this.template }
  }
  @Watch('seed')
  public seed_template() {
    window.dice = new Dice(this.seed)
    this.random_template()
  }
}
