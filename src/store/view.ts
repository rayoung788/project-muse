import { world__tick } from '@/models/history/events'
import { actor__relation } from '@/models/npcs/actors'
import { Actor } from '@/models/npcs/actors/types'
import { region__nation } from '@/models/regions'
import { Loc } from '@/models/regions/locations/types'
import { province__hub } from '@/models/regions/provinces'
import {
  Codex,
  codex__restore_history,
  codex__spawn,
  codex__update,
  codex__zoom_location,
  UpdateCodex
} from '@/models/utilities/codex'
import { Dice } from '@/models/utilities/math/dice'
import { hour_ms } from '@/models/utilities/math/time'
import { DisplayShaper } from '@/models/world/spawn/shapers/display'
import store from '@/store'
import { Action, getModule, Module, Mutation, VuexModule } from 'vuex-module-decorators'

interface ZoomParams {
  x: number
  y: number
  zoom: number
}

interface Notifications {
  active: boolean
  text: string
}

@Module({ dynamic: true, store, name: 'view' })
class View extends VuexModule {
  public world_id = ''
  public codex: Codex = { ...codex__spawn }
  public avatar: Actor = null
  public redraw = false
  public power_shift = false
  public clock = new Date()
  public gps = { x: 0, y: 0, zoom: 0 }
  public notifications: Notifications = { active: false, text: '' }

  @Mutation
  public update_codex(params: { target: UpdateCodex['target']; disable_zoom?: boolean }) {
    const { target, disable_zoom } = params
    codex__update({ target, codex: this.codex })
    const zoom = codex__zoom_location(target)
    if (!disable_zoom && zoom) this.gps = zoom
  }
  @Mutation
  public shaper(id: string) {
    this.world_id = id
    // always zoom to the same region on every load
    const dice = new Dice(id)
    const region = dice.choice(window.world.regions)
    const nation = region__nation(region)
    // set starting codex values
    this.codex.location = province__hub(window.world.provinces[region.capital])
    this.codex.culture = window.world.cultures[nation.culture.native]
    this.codex.war = window.world.wars[0]
    this.codex.rebellion = window.world.rebellions[0]
    codex__update({ codex: this.codex, target: nation })
    this.clock = new Date(window.world.date)
  }
  @Mutation
  public restore_history() {
    codex__restore_history(this.codex)
  }
  @Mutation
  public set_avatar(avatar: Actor) {
    this.avatar = avatar
  }
  @Mutation
  public unset_avatar() {
    this.avatar = null
  }
  @Mutation
  public travel(dest: Loc) {
    this.avatar.location.curr = dest.idx
    actor__relation({ actor: this.avatar, type: 'party' }).forEach(actor => {
      actor.location.curr = dest.idx
    })
  }
  @Mutation
  public tick(duration: number) {
    this.clock = new Date(window.world.date + duration)
    world__tick(this.clock.getTime())
    const redraw = window.world.regions.some(r => r.borders_changed)
    this.power_shift = !this.power_shift
    if (redraw) {
      this.notifications = { active: true, text: 'borders have changed' }
      DisplayShaper.draw_borders()
      this.redraw = !this.redraw
    }
  }
  @Mutation
  public set_gps({ x, y, zoom }: ZoomParams) {
    this.gps = { x, y, zoom }
  }
  @Mutation
  public trigger_redraw() {
    this.redraw = !this.redraw
  }
  @Mutation
  public spawn_notification(text: string) {
    this.notifications = { active: true, text }
  }
  @Mutation
  public close_notification() {
    this.notifications = { active: false, text: '' }
  }

  @Action
  public update_avatar(actor: Actor) {
    this.context.commit('set_avatar', actor)
    this.context.commit('update_codex', { target: actor })
  }
  @Action
  public update_avatar_location(params: { dest: Loc; hours: number }) {
    const { dest, hours } = params
    this.context.commit('travel', dest)
    this.context.commit('tick', hours * hour_ms)
    this.context.commit('update_codex', { target: dest })
  }
  @Action
  public release_avatar() {
    this.context.commit('unset_avatar')
  }
}

export const view_module = getModule(View)
