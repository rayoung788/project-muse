import { location__zoom } from '@/models/utilities/codex'
import { view_module } from './view'

export const zoom_to_current = () => {
  const { codex } = view_module
  const { current, location, nation } = codex
  const avatar = view_module.avatar === view_module.codex.actor && current === 'actor'
  const loc_zoom = current === 'location' || avatar
  const zoom = loc_zoom ? location__zoom(location) : 10
  const settlement_idx = loc_zoom ? location.idx : nation.capital
  const { x, y } = window.world.locations[settlement_idx]
  return { x, y, zoom, settlement_idx }
}
