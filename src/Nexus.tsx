import './Nexus.css'

import { Component } from 'vue-property-decorator'
import * as tsx from 'vue-tsx-support'

import Codex from '@/components/descriptive'
import Landing from '@/components/landing'
import WorldViews from '@/components/journey'
import { view_module } from '@/store/view'
import { css } from '@emotion/css'

import PrimaryButton from './components/common/PrimaryButton'
import SectionList from './components/common/text/SectionList'
import StyledText from './components/common/text/StyledText'
import TraitDisplay from './components/common/Traits'
import { BaseButton, ButtonSwitch, ButtonToggle } from './components/common/vuetify/button'
import {
  Card,
  CardActions,
  CardSubtitle,
  CardText,
  CardTitle
} from './components/common/vuetify/cards'
import { Chip } from './components/common/vuetify/chip'
import {
  ExpansionPanel,
  ExpansionPanelContent,
  ExpansionPanelHeader,
  ExpansionPanels
} from './components/common/vuetify/expansion'
import { Icon } from './components/common/vuetify/icon'
import {
  AutoComplete,
  RadioButton,
  RadioGroup,
  Select,
  Slider,
  TextField
} from './components/common/vuetify/input'
import {
  Column,
  Container,
  Content,
  Divider,
  Footer,
  Row,
  Spacer
} from './components/common/vuetify/layout'
import { Lazy } from './components/common/vuetify/lazy'
import { ProgressCircular } from './components/common/vuetify/progress'
import { Snackbar } from './components/common/vuetify/snackbar'
import { Tab, TabItem, TabItems, Tabs, TabsSlider } from './components/common/vuetify/tabs'
import { App, AppBar, ToolbarTitle } from './components/common/vuetify/toolbar'
import PerformanceChart from './components/Performance'
import { location__conditions } from './models/regions/locations/environment/conditions'
import { formatters } from './models/utilities/text/formatters'
import { world__load, world__save } from './models/world/spawn/serialization'
import { css_colors } from './styles/colors'
import DetailedTableRow from './components/common/vuetify/table'
import { TransparentTabs } from './components/common/tabs/transparent'

const style__upload_button = css`
  cursor: pointer;
  position: relative;
  overflow: hidden;
  input[type='file'] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    cursor: inherit;
    display: block;
  }
`

@Component({
  components: {
    // vuetify
    App,
    AppBar,
    AutoComplete,
    BaseButton,
    ButtonSwitch,
    ButtonToggle,
    Card,
    CardActions,
    CardSubtitle,
    CardText,
    CardTitle,
    Chip,
    Column,
    Container,
    Content,
    Divider,
    DetailedTableRow,
    ExpansionPanel,
    ExpansionPanelContent,
    ExpansionPanelHeader,
    ExpansionPanels,
    Footer,
    Icon,
    Lazy,
    ProgressCircular,
    RadioButton,
    RadioGroup,
    Row,
    Select,
    Slider,
    Snackbar,
    Spacer,
    Tab,
    TabItem,
    TabItems,
    Tabs,
    TabsSlider,
    TextField,
    ToolbarTitle,
    // common
    PrimaryButton,
    SectionList,
    StyledText,
    TraitDisplay,
    TransparentTabs,
    // custom
    Codex,
    Landing,
    PerformanceChart,
    WorldViews
  }
})
export default class Nexus extends tsx.Component<Record<string, unknown>> {
  private loading = false
  private saving = false
  private stats = false
  private get id() {
    return view_module.world_id
  }

  private load(e: Event) {
    this.loading = true
    const target = e.target as HTMLInputElement
    const files = target.files
    if (!files.length) return
    const file = files[0]
    const fr = new FileReader()
    fr.onload = () => {
      world__load(fr.result.toString())
      this.loading = false
    }
    fr.readAsText(file)
  }

  private save() {
    this.saving = true
    setTimeout(() => {
      world__save()
      this.saving = false
    }, 100)
  }

  public render() {
    let time = Date.now()
    let context = ''
    if (this.id) {
      const city = view_module.codex.location
      const { conditions, summary, local_time } = location__conditions(city)
      time = local_time
      context = `(${conditions.season.toLowerCase()}, ${summary})`
    }
    return (
      <App>
        <AppBar
          app
          style={{
            backgroundColor: '#f7f2e1'
          }}>
          <div class='ma-4'>
            <div style={{ 'font-size': '25px', 'line-height': 1.2, color: css_colors.primary }}>
              Nexus
            </div>
            <div style={{ 'font-size': '12px', 'line-height': 0.2, color: 'gray' }}>
              {this.id ? ` ${this.id}` : '∞'}
            </div>
          </div>
          <Spacer />
          <div class='ma-4'>
            <div style={{ 'font-size': '20px', 'line-height': 1.2, color: css_colors.primary }}>
              {formatters.date(time)}
            </div>
            <div style={{ 'font-size': '12px', 'line-height': 0.5, color: 'gray' }}>
              {formatters.time(time)} {context}
            </div>
          </div>
          <PrimaryButton
            class='mx-2'
            text='Save'
            click={this.save}
            loading={this.saving}
            disabled={!this.id || this.loading}
          />
          <PrimaryButton
            text={
              <span>
                LOAD
                <input type='file' onChange={this.load} />
              </span>
            }
            class={style__upload_button}
            loading={this.loading}
            disabled={this.saving}
          />
          <PrimaryButton
            text={<span>{this.stats ? 'WORLD' : 'STATS'}</span>}
            class='mx-2'
            click={() => (this.stats = !this.stats)}
            disabled={this.saving || !this.id}
          />
        </AppBar>
        <Content>
          <Container fluid class='paper'>
            {this.id && !this.stats ? (
              <Row justify='center'>
                <Column cols={6}>
                  <WorldViews />
                </Column>
                <Column cols={5}>
                  <Codex />
                </Column>
              </Row>
            ) : this.id && this.stats ? (
              <PerformanceChart></PerformanceChart>
            ) : (
              <Landing />
            )}
          </Container>
          <Snackbar
            v-model={view_module.notifications.active}
            text
            color={css_colors.primary}
            timeout={1000}
            scopedSlots={{
              action: () => {
                return (
                  <Icon color={css_colors.primary} onClick={() => view_module.close_notification()}>
                    mdi-close
                  </Icon>
                )
              }
            }}>
            <span>{view_module.notifications.text}</span>
          </Snackbar>
        </Content>
        <Footer app fixed style={{ backgroundColor: '#f7f2e1', color: css_colors.primary }}>
          <span>&copy; 2017</span>
        </Footer>
      </App>
    )
  }
}
