import { Item } from '@/models/items/types'

export interface Inventory {
  items: Record<string, Item>
  currency: number
}
