export type primordial__habitat_trait_tags =
  | 'subterranean'
  | 'desert'
  | 'cliffs'
  | 'volcanic'
  | 'tropism'
  | 'decay'
  | 'purifier'
