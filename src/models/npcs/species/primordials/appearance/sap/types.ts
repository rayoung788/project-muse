import { all_colors } from '@/models/utilities/colors'

export interface Sap {
  color: all_colors
  texture?: 'oily' | 'sticky' | 'watery'
}
