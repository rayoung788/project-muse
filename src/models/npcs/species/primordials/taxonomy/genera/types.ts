import { weighted_distribution } from '@/models/utilities/math'
import { Primordial } from '../../types'

export interface PrimordialGenus {
  appearance: (species: Primordial) => Primordial['appearance']
  size: (species: Primordial) => weighted_distribution<Primordial['size']>
  reproduction: (species: Primordial) => Primordial['reproduction']
}
