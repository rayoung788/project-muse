import { PrimordialGenus } from '../types'
import { lichens } from './lichens'

export const molds: PrimordialGenus = {
  ...lichens
}
