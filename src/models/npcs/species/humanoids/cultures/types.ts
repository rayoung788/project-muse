import { genders } from '@/models/npcs/actors/stats/appearance/gender'
import { Climate } from '@/models/world/climate/types'
import { TaggedEntity } from '@/models/utilities/codex/entities'
import { Language } from '../languages/types'
import { hue } from '@/models/utilities/colors'
import {
  HumanoidAppearance,
  humanoid__species
} from '@/models/npcs/species/humanoids/taxonomy/types'
import { directions } from '@/models/utilities/math/points'

type CulturalTerms = 'tribes' | 'diplomat' | 'royal guards' | 'district' | 'palace'

export interface Culture extends TaggedEntity {
  tag: 'culture'
  // neighboring cultures
  neighbors: number[]
  // origin region
  origin: number
  // cultural homelands (dialects)
  regions: number[]
  // species stats
  species: humanoid__species
  subspecies: string
  zone: Climate['zone']
  side: directions
  // cultural traits
  civilized?: boolean
  language?: Language
  lineage: genders
  religion?: number
  appearance?: HumanoidAppearance
  terms: Record<CulturalTerms, string>
  fashion: { color: hue }
  currency: string
  // display colors (for maps)
  display: string
}
