export type beast__reproduction_trait_tags =
  | 'parthenogenic'
  | 'hermaphrodites'
  | 'lek'
  | 'brood_parasitism'
  | 'cooperative'
