export type beast__use_trait_tags =
  | 'cultural'
  | 'livestock'
  | 'transportation'
  | 'companions'
  | 'leather'
  | 'fur'
  | 'textiles'
  | 'honey'
