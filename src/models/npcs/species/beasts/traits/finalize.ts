import { appearance_trait__finalize } from './appearance/finalize'
import { behavior_trait__finalize } from './behavior/finalize'
import { reproduction_trait__finalize } from './reproduction/finalize'
import { BeastTraitFinalize, beast_traits } from './types'

export const beast__trait_finalize: Partial<
  Record<beast_traits, (params: BeastTraitFinalize) => void>
> = {
  ...appearance_trait__finalize,
  ...behavior_trait__finalize,
  ...reproduction_trait__finalize
}
