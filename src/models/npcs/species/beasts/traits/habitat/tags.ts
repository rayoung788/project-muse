export type beast__habitat_trait_tags =
  | 'terrestrial'
  | 'arboreal'
  | 'semi_arboreal'
  | 'burrowing'
  | 'urban'
  | 'decline'
  | 'pest'
