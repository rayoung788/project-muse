import { Armor } from '@/models/items/armor/types'
import { Weapon } from '@/models/items/weapons/types'

export type equipable_slot = 'armor' | 'mainhand' | 'offhand'

export type Equipable = Armor | Weapon

export type Equipment = {
  mainhand: null | Equipable
  offhand: null | Equipable
  armor: null | Equipable
}
