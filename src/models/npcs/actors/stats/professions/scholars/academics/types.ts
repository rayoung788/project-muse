export type scholar_professions =
  | 'historian'
  | 'botanist'
  | 'astronomer'
  | 'philosopher'
  | 'linguist'
  | 'archivist'
