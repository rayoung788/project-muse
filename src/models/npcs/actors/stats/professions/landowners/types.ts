export type aristocrat_professions =
  | 'gentry (major)'
  | 'gentry (minor)'
  | 'noble (major)'
  | 'noble (minor)'
