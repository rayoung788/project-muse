export type performer_professions =
  | 'actor'
  | 'musician'
  | 'singer'
  | 'composer'
  | 'dancer'
  | 'stagehand'
