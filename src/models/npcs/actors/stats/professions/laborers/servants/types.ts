export type servant_professions =
  | 'servant'
  | 'porter'
  | 'gardner'
  | 'midwife'
  | 'cook'
  | 'waiter'
  | 'barkeep'
