// import { spawn__npc } from '@/models/npcs'
// import { species__sizes } from '@/models/npcs/species/size'
// import { npc__cr_to_lvl } from '@/models/npcs/stats'
// import { describe_terrain } from '@/models/regions/locations/geography/environment'
// import { WorldLocation } from '@/models/regions/locations/types'
// import { percentage_scale, weighted_distribution } from '@/models/utilities/math'
// import { EncounterBuilder } from '../types'

// const spirit__tier = (level: number): [string, species__sizes] => {
//   return level < 3
//     ? ['sprite', 'small']
//     : level < 5
//     ? ['remnant', 'medium']
//     : level < 7
//     ? ['revenant', 'medium']
//     : level < 9
//     ? ['lord', 'large']
//     : level < 11
//     ? ['giant', 'huge']
//     : ['ascendent', 'gargantuan']
// }

// const families = ['Elemental', 'Fiend', 'Celestial', 'Fey', 'Shadow'] as const

// const genera: Record<
//   typeof families[number],
//   (loc: WorldLocation) => weighted_distribution<string>
// > = {
//   Elemental: loc => {
//     const { climate, terrain } = describe_terrain(loc)
//     return [
//       { v: 'fire', w: climate === 'Cold' ? 0 : 1 },
//       { v: 'lava', w: climate === 'Cold' ? 0 : 1 },
//       { v: 'magma', w: 1 },
//       { v: 'ash', w: 1 },
//       { v: 'water', w: terrain === 'Desert' ? 0 : 1 },
//       { v: 'mist', w: 1 },
//       { v: 'steam', w: terrain === 'Desert' ? 0 : 1 },
//       { v: 'ice', w: climate === 'Cold' ? 1 : 0 },
//       { v: 'earth', w: 1 },
//       { v: 'mud', w: terrain === 'Forest' ? 1 : 0 },
//       { v: 'sand', w: terrain === 'Desert' ? 1 : 0 },
//       { v: 'dust', w: 1 },
//       { v: 'air', w: 1 },
//       { v: 'wind', w: 1 },
//       { v: 'cloud', w: 1 },
//       { v: 'storm', w: 1 }
//     ]
//   },
//   Fiend: () => [
//     { v: 'greed', w: 1 },
//     { v: 'envy', w: 1 },
//     { v: 'desire', w: 1 },
//     { v: 'lust', w: 1 },
//     { v: 'gluttony', w: 1 },
//     { v: 'pride', w: 1 },
//     { v: 'wrath', w: 1 },
//     { v: 'sloth', w: 1 },
//     { v: 'fear', w: 1 },
//     { v: 'despair', w: 1 },
//     { v: 'misery', w: 1 },
//     { v: 'anguish', w: 1 },
//     { v: 'spite', w: 1 }
//   ],
//   Celestial: () => [
//     { v: 'austerity', w: 1 },
//     { v: 'mercy', w: 1 },
//     { v: 'harmony', w: 1 },
//     { v: 'humility', w: 1 },
//     { v: 'justice', w: 1 },
//     { v: 'valor', w: 1 },
//     { v: 'wisdom', w: 1 },
//     { v: 'knowledge', w: 1 },
//     { v: 'hope', w: 1 },
//     { v: 'honor', w: 1 },
//     { v: 'law', w: 1 }
//   ],
//   Fey: loc => {
//     const { terrain } = describe_terrain(loc)
//     return [
//       { v: 'seasons', w: 1 },
//       { v: 'harvest', w: 1 },
//       { v: 'crafting', w: 1 },
//       { v: 'cultivation', w: 1 },
//       { v: 'dreams', w: 1 },
//       { v: 'omens', w: 1 },
//       { v: 'forest', w: terrain === 'Forest' ? 1 : 0 },
//       { v: 'plains', w: terrain === 'Plains' ? 1 : 0 },
//       { v: 'arctic', w: terrain === 'Arctic' ? 1 : 0 },
//       { v: 'desert', w: terrain === 'Desert' ? 1 : 0 },
//       { v: 'sky', w: 0.2 },
//       { v: 'peak', w: terrain === 'Mountains' ? 1 : 0 }
//     ]
//   },
//   Shadow: () => [
//     { v: 'gloom', w: 1 },
//     { v: 'blight', w: 1 },
//     { v: 'decay', w: 1 },
//     { v: 'death', w: 1 },
//     { v: 'void', w: 1 },
//     { v: 'abyssal', w: 1 },
//     { v: 'ruin', w: 1 },
//     { v: 'chaos', w: 1 },
//     { v: 'aberrant', w: 1 },
//     { v: 'cryptic', w: 1 }
//   ]
// }

// export const spirit__encounter: EncounterBuilder = {
//   title: 'Spirit Encounter',
//   finalize: ({ cr, loc }) => {
//     const plan = percentage_scale(Array<number>(window.dice.randint(1, 5)).fill(1))
//     const family = window.dice.choice([...families])
//     let genus: string = window.dice.weighted_choice(genera[family](loc))
//     if (window.dice.random > 0.7) {
//       const mutation = window.dice.choice(families.filter(fam => fam !== family))
//       genus = `${genus}, ${window.dice.weighted_choice(genera[mutation](loc))}`
//     }
//     return plan.map(w => {
//       const level = npc__cr_to_lvl(w * cr)
//       const [tier, size] = spirit__tier(level)
//       return spawn__npc({
//         name: 'spirit',
//         species: { type: 'spirit', size, family, genus },
//         level,
//         tier
//       })
//     })
//   }
// }
