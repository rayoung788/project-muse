// import { spawn__npc } from '@/models/npcs'
// import { beast__spawn } from '@/models/npcs/species/beasts'
// import { species__size, species__sizes } from '@/models/npcs/species/size'
// import { npc__cr_to_lvl } from '@/models/npcs/stats'
// import { describe_terrain } from '@/models/regions/locations/geography/environment'
// import { percentage_scale } from '@/models/utilities/math'
// import { EncounterBuilder } from '../types'

// const beast__tier = (params: { size: species__sizes; level: number }) => {
//   const { size, level } = params
//   const start = (species__size[size] - species__size.small) * 2 - 1
//   return level < start + 1
//     ? 'ravenous'
//     : level < start + 3
//     ? 'frenzied'
//     : level < start + 5
//     ? 'enchanted'
//     : 'mutated'
// }

// export const beast__encounter: EncounterBuilder = {
//   title: 'Beast Encounter',
//   finalize: ({ cr, loc }) => {
//     const level = npc__cr_to_lvl(cr)
//     // determine creature size
//     const sizes: species__sizes[] = ['medium']
//     if (level >= 3) sizes.push('large')
//     if (level >= 5) sizes.push('huge')
//     const chosen_size = window.dice.choice(sizes)
//     // find prospect candidates
//     const region = window.world.regions[loc.region]
//     const environment = describe_terrain(loc)
//     const candidates =
//       region.beasts[environment.key]
//         ?.map(i => window.world.beasts[i])
//         ?.filter(({ role, size }) => role === 'predator' && size === species__size[chosen_size]) ??
//       []
//     const chosen =
//       candidates.length > 0 && window.dice.random > 0.3
//         ? window.dice.choice(candidates)
//         : window.world.beasts[
//             beast__spawn({
//               region,
//               environment,
//               role: 'predator',
//               size: species__size[chosen_size]
//             })
//           ]
//     const plan = percentage_scale(
//       Array<number>(chosen_size === 'medium' ? window.dice.randint(3, 5) : 1).fill(1)
//     )
//     return plan.map(w => {
//       const level = npc__cr_to_lvl(w * cr)
//       return spawn__npc({
//         name: chosen.name,
//         species: { type: 'beast', idx: chosen.idx },
//         level,
//         tier: beast__tier({ size: chosen_size, level })
//       })
//     })
//   }
// }
