import { Region } from '@/models/regions/types'
import { War } from '../types'

export interface WarBackgroundArgs {
  invader: Region
  defender: Region
}

export interface WarBackground {
  tag: War['background']['type']
  spawn: (args: WarBackgroundArgs) => number
  text: (args: WarBackgroundArgs) => string
}
