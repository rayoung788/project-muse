export interface Battle {
  name: string
  count: number
}

export interface NextBattle {
  province: number
  aggressor: number
}
