import { Region } from '@/models/regions/types'
import { Rebellion } from '../types'

export interface RebellionBackgroundArgs {
  nation: Region
  rebels: Region
}

export interface RebellionBackground {
  tag: Rebellion['background']['type']
  spawn: (args: RebellionBackgroundArgs) => number
  text: (args: RebellionBackgroundArgs) => string
  goal: (params: { rebellion: Rebellion }) => void
}
