import { location_icon } from '@/components/journey/maps/world/icons/locations/types'
import { ExteriorCell } from '@/models/world/cells/types'
import { Province } from '../../../provinces/types'
import { LocationTrait } from '../traits/types'
import { Loc } from '../../types'

export interface LocationTemplate {
  type: Loc['type']
  population?: [number, number]
  coastal?: number
  icon: ((loc: Loc) => location_icon) | location_icon
  group: 'settlement' | 'wilderness'
  spawn?: ((province: Province) => number) | number
  hostile?: boolean
  finalize?: (loc: Loc) => void
  restrictions?: (cell: ExteriorCell) => boolean
  traits?: (loc: Loc) => LocationTrait['tag'][]
}
