export interface TraitEnriched<tags extends string> {
  traits: { tag: tags; text: string }[]
}

export interface Trait<tags extends string, args extends { entity: TraitEnriched<tags> }> {
  tag: tags
  text: (params: args) => string
  spawn: (params: args) => number
  conflicts?: tags[]
}
