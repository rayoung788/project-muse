import { ItemDetails, item__basic_details } from '../types'
import { Material, material__tag } from './types'

// const alcohol__type_by_rarity = {
//   [scarcity.abundant]: 'Poor',
//   [scarcity.common]: 'Basic',
//   [scarcity.uncommon]: 'Distilled',
//   [scarcity.rare]: 'Pure',
//   [scarcity.exceptional]: 'Pristine'
// }
abstract class MaterialDetails extends ItemDetails {
  abstract value(item: Material): number
}

const _material: Omit<MaterialDetails, 'tag' | 'weight' | 'markets'> = {
  category: 'materials',
  ...item__basic_details
}
export const materials: Record<material__tag, MaterialDetails> = {
  herbs: {
    tag: 'herbs',
    base_price: 50,
    weight: 0.1,
    markets: ['reagents (alchemical)'],
    ..._material
  },
  reagent: {
    tag: 'reagent',
    base_price: 80,
    weight: 0.1,
    markets: ['reagents (alchemical)'],
    ..._material
  },
  gemstone: {
    tag: 'gemstone',
    base_price: 1000,
    weight: 0.1,
    markets: ['metals (gemstones)'],
    ..._material
  }
}
