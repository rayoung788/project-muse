import { Item } from '../types'
import type { armor__enchant } from './armor/types'
import type { weapon__enchant } from './weapons/types'

export interface EnchantmentInstance {
  tag: armor__enchant | weapon__enchant
}

export interface EnchantableItem extends Item {
  enchantment?: EnchantmentInstance
  culture: number
}

export interface EnchantmentDescriptionParams<Instance extends EnchantmentInstance> {
  item: EnchantableItem
  instance: Instance
}

export type BasicEnchantmentDescription = EnchantmentDescriptionParams<EnchantmentInstance>

export abstract class Enchantment {
  tag: EnchantmentInstance['tag']
  public abstract description(params: BasicEnchantmentDescription): string
  public abstract spawn(instance: EnchantmentInstance): EnchantmentInstance
  public abstract key(instance: EnchantmentInstance): string
}

export const enchantment__basic_details = {
  key: (instance: EnchantmentInstance) => instance.tag,
  spawn: (instance: EnchantmentInstance) => instance
}

export const enchantment__item_tag = (item: BasicEnchantmentDescription['item']) =>
  `this ${item.tag}`
