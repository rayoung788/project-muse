import { route_types } from '../../world/travel/types'

export const enum vehicle_types {
  ROWBOAT = 'Rowboat',
  GALLEON = 'Galleon'
}

interface Vehicle {
  mph: number
  traveling_hours: number
  type: vehicle_types
  travel_type: route_types
}

export const vehicles: Record<vehicle_types, Vehicle> = {
  [vehicle_types.ROWBOAT]: {
    mph: 1.5,
    traveling_hours: 10,
    type: vehicle_types.ROWBOAT,
    travel_type: 'sea'
  },
  [vehicle_types.GALLEON]: {
    mph: 4,
    traveling_hours: 24,
    type: vehicle_types.GALLEON,
    travel_type: 'sea'
  }
}
