import { market_groups } from '../economy'
import { ItemDetails, item__basic_details } from '../types'
import { MiscItem, misc_item__tag } from './types'

// const saddles_by_rarity = {
//   [scarcity.abundant]: 'Torn',
//   [scarcity.common]: 'Riding',
//   [scarcity.uncommon]: 'Military',
//   [scarcity.rare]: 'Enchanted',
//   [scarcity.exceptional]: 'Exotic'
// }
abstract class MiscItemDetails extends ItemDetails {
  abstract value(item: MiscItem): number
}

const _misc: Omit<MiscItemDetails, 'tag' | 'weight' | 'markets'> = {
  category: 'misc',
  ...item__basic_details
}
export const misc_items: Record<misc_item__tag, MiscItemDetails> = {
  saddle: {
    tag: 'saddle',
    base_price: 500,
    weight: 15,
    markets: market_groups.leather,
    ..._misc
  },
  waterskin: {
    tag: 'waterskin',
    base_price: 20,
    weight: 5,
    markets: market_groups.leather,
    ..._misc
  },
  book: {
    tag: 'book',
    base_price: 700,
    weight: 5,
    markets: market_groups.texts,
    ..._misc
  },
  rope: {
    tag: 'book',
    base_price: 100,
    weight: 5,
    markets: market_groups.textiles,
    ..._misc
  }
}
