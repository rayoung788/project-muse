import { ContinentTemplate } from '@/components/landing/shapes'
import { Delaunay, Voronoi } from 'd3-delaunay'
import PriorityQueue from 'js-priority-queue'
import { Rebellion } from '../history/events/rebellion/types'
import { War } from '../history/events/war/types'
import { LogRecord, WorldEvent } from '../history/types'
import { ActorEvent } from '../npcs/actors/history/events/types'
import { Actor } from '../npcs/actors/types'
import { Beast } from '../npcs/species/beasts/types'
import { Culture } from '../npcs/species/humanoids/cultures/types'
import { Religion } from '../npcs/species/humanoids/religions/types'
import { Humanoid, humanoid__species } from '../npcs/species/humanoids/taxonomy/types'
import { Primordial } from '../npcs/species/primordials/types'
import { Loc } from '../regions/locations/types'
import { Province } from '../regions/provinces/types'
import { Region } from '../regions/types'
import { Thread } from '../threads/types'
import { Dimensions } from '../utilities/dimensions'
import { ExteriorCell } from './cells/types'
import { Display } from './spawn/shapers/display/types'
import { route_types } from './travel/types'

export const sea_level_cutoff = 0.06
export const mountains_cutoff = 0.5

export interface CoastalEdge {
  water: number
  land: number
  edge: [number, number][]
}

interface WorldDimensions extends Dimensions {
  // voronoi cell resolution
  cells: number
  // actual width / height (miles)
  rw: number
  rh: number
  // noise resolution
  res: number
  // cell dimensions
  cell_area: number
  cell_length: number
}

interface EventCounts {
  wars: number
  rebellions: number
  regions: number
}

export interface World {
  id: string
  diagram?: Voronoi<Delaunay.Point>
  cells: ExteriorCell[]
  // geography
  landmarks: Record<
    number,
    {
      name: string
      type: 'ocean' | 'lake' | 'continent' | 'island'
      monsoon?: boolean
      water: boolean
      size: number
    }
  >
  mountains: string[]
  coasts: CoastalEdge[]
  routes: Record<
    route_types,
    { path: number[]; length: number; src: number; dst: number; imperial?: boolean }[]
  >
  display: Display
  dim: WorldDimensions
  template: ContinentTemplate['isles']
  // entities
  regions: Region[]
  provinces: Province[]
  locations: Loc[]
  cultures: Culture[]
  humanoids: Record<humanoid__species, Humanoid>
  beasts: Beast[]
  primordials: Primordial[]
  actors: Actor[]
  actor_events: ActorEvent[]
  religions: Religion[]
  threads: Thread[]
  unique_names: Record<string, boolean>
  // planet info
  date: number
  first_new_moon: number // first new moon on record
  lunar_cycle: number // synodic month (days)
  rotation: number // daily period (hours)
  tilt: number // axial tilt (degrees)
  seasons: {
    winter: number[]
    spring: number[]
    summer: number[]
    fall: number[]
  }
  geo_bounds: {
    lat: number[]
    long: number[]
  }
  // history
  past: LogRecord[]
  future: PriorityQueue<WorldEvent>
  wars: War[]
  rebellions: Rebellion[]
  history_recording: boolean // used to halt detailed npc generation
  statistics: { current: EventCounts; past: (EventCounts & { time: number })[] }
}
