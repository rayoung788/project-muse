import { Dice } from '@/models/utilities/math/dice'
import { MemCache } from './models/utilities/performance/memoization'
import { Profiles } from './models/utilities/performance'
import { World } from './models/world/types'

declare global {
  interface Window {
    dice: Dice
    world: World
    time: number
    profiles: Profiles
    memcache: MemCache
  }
}
