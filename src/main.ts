import Vue from 'vue'

import Nexus from './Nexus'
import vuetify from './styles/plugins/vuetify'
import store from './store'
import 'vue-tsx-support/enable-check'

Vue.config.productionTip = false

new Vue({
  vuetify,
  store,
  render: h => h(Nexus)
}).$mount('#app')
