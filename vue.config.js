module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/project-muse/' : '/',
  css: {
    loaderOptions: {
      sass: {
        data: `@import "~@/main.scss"`
      }
    }
  },
  chainWebpack: config => {
    config.optimization.minimize(false)
    config.module
      .rule(/\.(j|t)sx$/)
      .test(/\.(j|t)sx$/)
      .use('vue-jsx-hot-loader')
      .before('babel-loader')
      .loader('vue-jsx-hot-loader')
  }
}
